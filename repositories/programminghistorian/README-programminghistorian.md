data and scripts processing programminghistorian  
https://programminghistorian.org/  
https://gitlab.gwdg.de/sshoc/data-ingestion/issues/5

# CSV export

The csv export is created by a php script, the source code will be shared.

There are two files: _ProgrammingHistorian_CSVExport.csv_ holds all information on lessons. _ProgrammingHistorian_CSVExport_Persons.csv_ contains information on the persons that are mentioned in the lessons. As there is no specific identifier in PH, the connection is done by the name.

When there are more than one value in a row, e.g. contributors, then they are separated with | (pipe).

When opening in LibreOffice Calc: Set , (comma) as general separator and " as separator for text. Also activate the option that apostrophe formats text.
The keys for the rows are sorted alphabetically.

# Tool extraction

output of test-wise extraction of tool mentions in programminghistorian
