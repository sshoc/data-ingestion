#!/bin/sh
set -e
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mkdir -p ~/.creds
chmod 700 ~/.ssh
cat /app/sshconfig >> ~/.ssh/config
cd /app
pip install --upgrade pip setuptools wheel
GITLAB_TOKEN=$(echo $GITLAB)
git clone https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git
cd data-ingestion
cd /app/data-ingestion
# TODO: Remove checkout
git checkout 96-ab-testing

cd /app/data-ingestion/repositories/extraction/
cp /app/prodigy.json /app/data-ingestion/repositories/extraction/prodigy.json

pip install pydantic==1.7.2
pip install 'dvc[gs]'
pip install psycopg2==2.8.6

dvc pull publication_retrieval/candidates/sentences_to_analyse_prod.jsonl
dvc pull ner_ml/model/tools_model_with_corrections_pretrained

PRODIGY_LOGGING=verbose prodigy ner.correct correct_analyze ner_ml/model/tools_model_with_corrections_pretrained publication_retrieval/candidates/sentences_to_analyse.jsonl  --label Tool