#!/bin/sh
set -e
set -x
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mkdir -p ~/.creds
chmod 700 ~/.ssh

cd /app
pip install --upgrade pip setuptools wheel
GITLAB_TOKEN=$(echo $GITLAB)
git clone https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git

# TODO: remove checkout
cd data-ingestion
cd /app/data-ingestion
git checkout 96-ab-testing

##### COPY Database Setting #####
cd /app/data-ingestion/repositories/extraction/
cp /app/prodigy.json /app/data-ingestion/repositories/extraction/prodigy.json

##### Install dependencies ####
pip install pydantic==1.7.2
pip install psycopg2==2.8.6
pip install 'dvc[gs]'
cd /app/data-ingestion || exit
pip install -U -r /app/data-ingestion/repositories/extraction/ner_ml/requirements.txt

#### Pull NER models to compare ####
cd /app/data-ingestion/repositories/extraction/ || exit