# SSHOC Tool Extraction

## Workflow
![Image](sshoc_tool_extractoion_process.png "SSHOC Tool Extraction Process")

## Components

### SSHOC-NER-TRAINER
Trains the Tool NER model.

* Input: Train/Testset are loaded from prodigy database
* Output: 
  * new NER model
  * Training result: ner-train-result.txt

The newly trained model replaces the old model stored in git/dvc and is tagged with <code>new_model_$(date +%y%m%d)</code>

### SSHOC-TOOL-INGESTER
Extracts tool names from publications with the NER model.  
Only new publications are processed by checking the <code>processed-at</code> property.  

The ingestion pipeline works as follows
1. Retrieve publications
2. Download PDFs
3. Convert to XMLs
4. Extract tool candidates
5. Add relations

### SSHOC-EVALUATOR
Evaluates tool names that were suggested by the NER model, but couldn't be validated by SSHOC Marketplace or Wikidata.  
This component starts up a prodigy instance with <code>ner.correct</code> recipe.  
Human annotators should correct the accept/reject/ignore the annotations suggested by the model. The evaluation results will be stored in the prodigy database.

### SSHOC-DATA-EXPORTER
Loads the newly evaluated annotations, merge them with the old dataset, and splits them into train and testset.  
Train and testset are exported as jsonl files to <code>git/dvc</code> and also to prodigy database.

1. Train model 

<code> ner_model </code> can be trained using the sshoc_ner_train docker image.

if(basemodel exists): Compare against base model
periodically, once a month (automated)
Extract Entity and Ingest
Run periodically once a month after Step 1.
manually Evaluate suggestion
Add manually evaluate data to training/test set
Back to 1.


# DATASETS
Input datasets are shared version controlled with DVC.
You can pull the data with the following command.

```
dvc pull
```

Command to list dvc tracked file in a specific path
```
 dvc list -R --dvc-only https://gitlab.gwdg.de/sshoc/data-ingestion.git repositories/extraction/data
```

The command pulls following data
* DH_PUB dataset in TEI Format from years 2015-2020
* PlosOne dataset
* Extracted sentences from both datasets joined in one file, corpus_full.jsonl

## DH_PUBLICATIONS
Once pulled, DH_PUBLICATIOS dataset can be found in the data/dh_pub directory.


# PRE-PROCESSING
Following pre-processing steps are done to collect sentences for NER annotation/training

## Convert TEI to sentences in JSONL
### Activate conda environment
```
conda env create -f environment.yml
conda activate sshoc_prodigy
```
```
dvc pull
dvc repro
```

87945 sentences were extracted.

the stages of the dvc pipeline were created by following commands
```
dvc run -n preprocess_dh_pub -d preprocess/tei_to_sentences.py -o data/dh_pub_output python preprocess/tei_to_sentences.py  data/dh_pub/xml data/dh_pub_output/
dvc run -n to_jsonl -d preprocess/corpus_to_jsonl.py -d data/dh_pub_output -o data/sentences python preprocess/corpus_to_jsonl.py data/dh_pub_output data/sentences/dh_pub_sentences.jsonl
```
