import jsonlines
import sys

"""
python text_to_pattern.py INPUT OUTPUT LABEL

example : python text_to_pattern.py data/patterns/wikidata_software_names.csv data/patterns/wikidata_software_pattens.jsonl Tool 

INPUT : contains one phrase per line
OUTPUT : Prodigy patterns for the specified label and phrases
LABEL : the annotation label for patterns file
"""

def text_to_pattern():
    data = [] 
    with open(INPUT) as f:
        for line in f:
            line = line.strip('\n')
            data.append({"label": LABEL, "pattern": line})
        with jsonlines.open(OUTPUT, 'w') as writer:
            writer.write_all(data)

INPUT = sys.argv[1]
OUTPUT = sys.argv[2]
LABEL = sys.argv[3]
text_to_pattern()
