from bs4 import BeautifulSoup
import os
import logging
import sys
import spacy
from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

nlp = spacy.load('en_core_web_md')

logger = logging.getLogger('preprocess')
logger.setLevel(logging.INFO)

logger.info(len(sys.argv))
for arg in sys.argv:
    logger.info(arg)

input_dir = sys.argv[1]
output_dir = sys.argv[2]

all_directories = [input_dir+'/2015',
                   input_dir+'/2016',
                   input_dir+'/2017',
                   input_dir+'/2018',
                   input_dir+'/2019',
                   input_dir+'/2020']




def write_to_file(output_dir, filename, paragraphs):
    output_file = get_output_file(output_dir, filename.name.replace('.xml', '.txt'))
    non_english_file = get_output_file('../'+output_dir+'/non_english', '.txt')
    error_file = get_output_file('../'+output_dir+'/error', '.txt')

    en_sentences = []
    non_en_sentences = []
    erroneos_sentences = []

    for p in paragraphs:
        sentences = extract_sentences(p)
        check_language(sentences, en_sentences, non_en_sentences, erroneos_sentences)

    write(en_sentences, output_file)
    write(non_en_sentences, non_english_file, mode='a+')
    write(erroneos_sentences, error_file, mode='a+')


def write(list_data, file, mode='w+'):
    with open(file, mode) as fd:
        for data in list_data:
            fd.write(data + '\n')


def check_language(sentences, en, non_en, erroneos):

    for sent in sentences:
        try:
            en.append(sent) if detect(sent) == 'en' else non_en.append(sent)
        except LangDetectException:
            erroneos.append(sent)


def extract_sentences(p):
    doc = nlp(p.getText(strip=True))
    sentences = [sent.string.strip() for sent in doc.sents]
    return sentences


def get_output_file(output_dir, filename):
    out_filename = output_dir + filename
    if not os.path.exists(os.path.dirname(out_filename)):
        try:
            os.makedirs(os.path.dirname(out_filename))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    return out_filename


def retrieve_paragraphs(soup):
    pg = []

    for t in soup.findAll('text'):
        pg = pg + (t.findAll('p'))

    return pg


for directory in all_directories:
    for filename in os.scandir(directory):
        with open(filename.path, 'r') as tei:
            soup = BeautifulSoup(tei, 'lxml')
            paragraphs = retrieve_paragraphs(soup)
            write_to_file(output_dir, filename, paragraphs)
