import errno
import glob2, os, json, jsonlines, sys

"""
python corpus_to_jsonl.py corpusfolderpath OUTPUT 

example : python corpus_to_jsonl.py data/SENTS-sample  data/corpus_sample.jsonl

corpusfolderpath : contains multiple sentences per text file
OUTPUT : JSONL file with one sentence per line (suitable as large prodigy source file) 
"""


def get_output_file(output_dir, filename):
    out_filename = output_dir + filename
    if not os.path.exists(os.path.dirname(out_filename)):
        try:
            os.makedirs(os.path.dirname(out_filename))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    return out_filename


def corpus_to_jsonl(filename):
    with open(filename) as f:
        for line in f:
            line = line.strip('\n')
            print(line)
            data.append({"text": line})


data = []
corpusfolderpath = sys.argv[1]
OUTPUT = get_output_file('', sys.argv[2])

for filename in glob2.glob(corpusfolderpath + '/*.txt'):
    corpus_to_jsonl(filename)

with jsonlines.open(OUTPUT, 'w') as writer:
    writer.write_all(data)
