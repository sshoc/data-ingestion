import logging
import os
import zipfile
from pathlib import Path

import time

import spacy
import spacy.cli

from itertools import filterfalse
from datetime import datetime, timezone
from model.sshoc_tool import SshocItem, SshocPublication
from openapi_client import *
from requests.exceptions import HTTPError, ConnectionError, TooManyRedirects
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter, SshocVocabulary
from bs4 import BeautifulSoup

from spacy.tokens import Doc
from util.config_helper import load_openaire_config

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class Publication():
    def __init__(self, publication: SshocPublication, filename: str, link: str):
        self._publication: SshocPublication = publication
        self._filename: str = filename
        self._link: str = link
        self._tool_candidates: [ToolCandidate] = []

    @property
    def filename(self):
        return self._filename

    @property
    def link(self):
        return self._link

    @property
    def tool_candidates(self):
        return self._tool_candidates

    @tool_candidates.setter
    def tool_candidates(self, tool_candidates):
        self._tool_candidates = tool_candidates

    @property
    def publication(self):
        return self._publication

    @publication.setter
    def publication(self, publication):
        self._publication = publication


def exists_processed_at(item: PublicationCore) -> bool:
    if hasattr(item, 'properties'):
        for prop in item.properties:
            if prop is not None and prop.type.code == 'processed-at' and prop.value is not None:
                return True
    return False


def check_accessible_at(item: SshocItem) -> bool:
    if hasattr(item, 'accessible_at'):
        try:
            for link in item.accessible_at:
                if link is not None:
                    return True
                # if ".pdf" in link in link:
                #    logging.info(link)
                #    return True
        except Exception as e:
            logging.error('Error occurred while checking pdf link: ' + item.label)
    return False


class ToolCandidate(object):
    def __init__(self, persistent_id, label):
        self._persistent_id = persistent_id
        self._label = label

    @property
    def persistent_id(self):
        return self._persistent_id

    @property
    def label(self):
        return self._label

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.persistent_id == other.persistent_id and
                self.label == other.label
        )

    def __hash__(self):
        return hash(self._persistent_id)


class WikidataAdapter(object):
    def __init__(self, rest_client: RestClient):
        self.logger = logging.getLogger(__name__)

        self._rest_client = RestClient()

    def lookup_wikidata_with_toolname(self, tool_name):
        found = []
        query_string = """
            SELECT DISTINCT ?item   ?itemLabel_en WHERE {
            ?item ?label "%s"@en;
            wdt:P31 wd:Q7397 .
            ?item rdfs:label ?itemLabel_en . FILTER(lang(?itemLabel_en)='en')
            }""" % (tool_name)

        params = {'query': query_string}
        headers = {'Accept': 'application/sparql-results+json',
                   'User-Agent': 'sshoc-mp/1.0 (https://sshoc-marketplace.acdh-dev.oeaw.ac.at) generic-library/1.0'}
        try:
            res = self._rest_client.get('https://query.wikidata.org/sparql', params=params, headers=headers).json()
            matches = res["results"]["bindings"]
            if len(matches) >= 1:
                for row in matches:
                    tool = ToolCore(label=tool_name, description=row["item"]["value"])
                    found.append(tool)
        except HTTPError:
            self.logger.warning('Failed to query wikidata for tool: ' + tool_name + ' with query ' + query_string)

        return found


class SshocToolRetriever(object):
    def __init__(self, sshoc_adapter: SshocAdapter, pdf_save_path='pdf', xml_dir="output"):
        self.logger = logging.getLogger(__name__)
        self._sshoc_adapter = sshoc_adapter
        self._rest_client = RestClient()
        self._pdf_dir = pdf_save_path
        self._xml_dir = xml_dir

    def get_sshoc_publications_with_valid_link(self, skip_existing=True, only_pdf=False, page=None, page_limit=None):
        pubs = []
        if page is not None:
            retrieved = self._sshoc_adapter.get_items_of_page(self._sshoc_adapter.PUBLICATIONS_ENDPOINT,
                                                              SshocVocabulary.PUBLICATION, page)
            pubs.extend(retrieved)
        else:
            pubs.extend(self._sshoc_adapter.get_publications(page_limit=page_limit))

        self.logger.info('total # of pubs:' + str(len(pubs)))
        pubs_with_accessible_at = list(filter(check_accessible_at, pubs))
        self.logger.info('# of pubs with valid link' + str(len(pubs_with_accessible_at)))

        if skip_existing:
            return list(filterfalse(exists_processed_at, pubs_with_accessible_at))
        else:
            return pubs_with_accessible_at

    def download_publications(self, publications: [SshocPublication], pdf_path='pdf') -> [Publication]:
        result = []
        xml_count = 0

        self.logger.info("Start downloading total of " + str(len(publications)) + " publications")

        for pub in publications:
            for idx, link in enumerate(pub.accessible_at):
                try:
                    if link.endswith('.xml'):
                        xml_count += 1
                    response = self._rest_client.get(link)
                    filename = self.__write_publication(idx, pub, response, link)
                    if link.endswith('.xml'):
                        self.logger.info(link)
                    result.append(Publication(pub, filename, link))
                except KeyError:
                    with open('logs/pub_download_keyerror.log', 'a+') as ke_log:
                        ke_log.write(link + '\n')
                    self.logger.debug('KeyError while downloading publication source:' + link)
                except HTTPError:
                    self.logger.debug('HTTPError while downloading publication source:' + link)
                    with open('logs/pub_download_httperror.log', 'a+') as fne_log:
                        fne_log.write(link + '\n')
                except ConnectionError:
                    with open('logs/pub_download_connectionerror.log', 'a+') as fne_log:
                        fne_log.write(link + '\n')
                    self.logger.debug('ConnectionError while downloading to publication source:' + link)
                except TooManyRedirects:
                    with open('logs/pub_download_connectionerror.log', 'a+') as tmr_log:
                        tmr_log.write(link + '\n')
                    self.logger.debug('TooManyRedirects: ' + link)

        self.logger.info("Total number of XML Links: " + str(xml_count))
        self.logger.info("PDF download completed..")

        pdf_dir = Path(pdf_path)

        with zipfile.ZipFile("publications_pdf.zip", "w", zipfile.ZIP_DEFLATED) as zip_file:
            for entry in pdf_dir.rglob("*"):
                zip_file.write(entry, entry.relative_to(pdf_dir))

        return result

    def __write_publication(self, idx, pub, response, link):
        filename = pub.label + '_' + str(idx)
        os.makedirs(os.path.dirname(self._pdf_dir + '/' + filename), exist_ok=True)

        extension = None
        if link.endswith('pdf'):
            extension = '.pdf'
        elif link.endswith('xml'):
            extension = '.xml'
        else:
            extension = '.html'

        try:
            file_path = self._pdf_dir + '/' + filename + extension if extension.endswith(
                '.pdf') else self._xml_dir + '/' + filename + extension
            with open(file_path, 'wb+') as f:
                f.write(response.content)

        except FileNotFoundError:
            with open('logs/write_publication_file_not_found_error.log', 'a+') as fne_log:
                fne_log.write(self._xml_dir + filename + extension + '\n')
        return filename


class SshocToolCandidateExtractor(object):
    _sshoc_adapter: SshocAdapter

    def __init__(self, sshoc_adapter: SshocAdapter, model_version: float = None, pdf_save_path='pdf', xml_dir="output"):
        self.logger = logging.getLogger(__name__)
        self.model_version = model_version
        self.config = load_openaire_config(os.environ.get('CONFIG_FILE'))
        self._sshoc_adapter = sshoc_adapter
        self._pdf_dir = pdf_save_path
        self._xml_dir = xml_dir
        self._nlp = spacy.load("en_core_web_sm")
        self._nlp.remove_pipe("ner")
        self.logger.info(self._nlp.pipeline)
        version_postfix = '' if self.model_version is None else '_' + str(self.model_version)
        self._pretrained_model = spacy.load('../ner_ml/model/tools_model_with_corrections_pretrained' + version_postfix)
        self._rest_client = RestClient()
        self._wiki_adapter = WikidataAdapter(RestClient())

    def extract_tool_candidate_names(self, publication_object: object):
        self.logger.info("Extracting tool candidate names from XML")

        sentences: [str] = self.__convert_xml_to_sentences(publication_object)
        tool_name_to_sentences: {str: [str]} = self.__extract_toolnames(sentences)

        return tool_name_to_sentences, sentences

    def retrieve_tool_candidates_from_marketplace(self, tool_candidate_names: [str]) -> [ToolCandidate]:
        existing = []
        non_existing = []

        for tool_name in tool_candidate_names:
            self.logger.info('tool name: ' + tool_name)
            try:
                search_result = self._sshoc_adapter.search_item(tool_name, category=SshocVocabulary.TOOLS_OR_SERVICE[
                    SshocVocabulary.VocabularyType.ENTITY_TYPE.value], order="score")

                search_result = list(filter(lambda x: x.label.lower() == tool_name.lower(), search_result))
                if len(search_result) == 0:
                    non_existing.append(tool_name)
                elif len(search_result) >= 1:
                    tool = search_result[0]
                    existing.append(ToolCandidate(tool.persistent_id, tool.label))
            except HTTPError:
                # TODO: write in file
                self.logger.error('HTTP Error while searching tool in MP ' + tool_name)

        # TODO What if > 1 exact matches?

        return set(existing), non_existing

    def retrieve_tool_candidates_from_wikidata(self, non_existing: [str]):
        # TODO Move this check to sshoc adapter
        if self._sshoc_adapter.auth_token is None:
            self._sshoc_adapter.sign_in(self.config['sshoc']['username'], self.config['sshoc']['password'])
        existing = []
        wiki_non_existing = []

        for tool_name in non_existing:
            found = self._wiki_adapter.lookup_wikidata_with_toolname(tool_name)

            if len(found) == 0:
                wiki_non_existing.append(tool_name)
            else:
                for tool_found in found:
                    tool_created = self._sshoc_adapter.create_item(tool_found, SshocVocabulary.TOOLS_OR_SERVICE)
                    existing.append(ToolCandidate(tool_created.persistent_id, tool_created.label))

        return set(existing), wiki_non_existing

    def process_publication_tool_relation(self, publication_holder: Publication):
        if self._sshoc_adapter.authToken is None:
            self._sshoc_adapter.sign_in(self.config['AUTH']['username'], self.config['AUTH']['password'])
        if len(publication_holder.tool_candidates) == 0:
            self.logger.info("No tool candidates to add for publication: " + publication_holder.publication.label)
            return [], None

        added_relations = []
        for candidate in publication_holder.tool_candidates:
            try:
                relation = self.__add_publication_tool_relation(publication_holder.publication.persistent_id,
                                                                candidate.persistent_id)
                added_relations.append(relation)
            except HTTPError:
                self.logger.warning(
                    "Ingesting relation failed: " + publication_holder.publication.persistent_id + candidate.persistent_id)

        retrieved_pub = self._sshoc_adapter.get_publication(publication_holder.publication.persistent_id)
        if retrieved_pub.persistent_id == publication_holder.publication.persistent_id:
            publication_holder.publication = retrieved_pub
        publication: PublicationCore = self.__prepare_publication_payload(publication_holder.publication)

        updated_publication: SshocItem = self._sshoc_adapter.update_publication(
            publication_holder.publication.persistent_id, publication)

        return added_relations, updated_publication

    def __prepare_publication_payload(self, publication: SshocPublication) -> PublicationCore:
        now = datetime.now(timezone.utc)
        now_iso = now.strftime("%Y-%m-%dT%H:%M:%S%z")

        property_type_id = PropertyTypeId(code='processed-at')
        _property = PropertyCore(type=property_type_id, value=now_iso)

        model_version_property_id = PropertyTypeId(code='model-version')
        _model_version_property = PropertyCore(type=model_version_property_id, value=self.model_version)

        publication.properties.append(_property)
        publication.properties.append(_model_version_property)

        return publication

    def __extract_toolnames(self, sentences) -> {str: [str]}:
        tool_names = []
        sent_of_tools = {}
        for doc in self._pretrained_model.pipe(sentences, disable=["tagger", "parser"]):
            for ent in doc.ents:
                # spacy.displacy.render(doc, style='ent', jupyter=True)
                tool_names.append(ent.text)
                if ent.text not in sent_of_tools:
                    sent_of_tools[ent.text] = [doc.text]
                else:
                    sent_of_tools[ent.text].append(doc.text)

        return sent_of_tools

    def __convert_xml_to_sentences(self, publication_object: Publication) -> [str]:
        tic = time.perf_counter()
        sentences = []
        try:
            extension = self.__check_extension(publication_object.filename)
            with open(self._xml_dir + publication_object.filename + extension, 'r') as tei:
                soup_tic = time.perf_counter()
                parser = 'html' if extension == '.html' else 'lxml'
                soup = BeautifulSoup(tei, parser)
                soup_toc = time.perf_counter()

                self.logger.debug(f"Beautiful Soup Converted in {soup_toc - soup_tic:0.4f} seconds")

                texts: list[str] = self.__extract_paragraph_texts(soup)
                docs: list[Doc] = list(self._nlp.pipe(texts))
                sentences: list[str] = self.__extract_sentences(docs)

            toc = time.perf_counter()
            self.logger.debug(f"Converted in {toc - tic:0.4f} seconds")
        except UnicodeDecodeError:
            with open('logs/xml_to_sentence_unicode_decode_error.log', 'a+') as unicode_error_log:
                unicode_error_log.write(self._xml_dir + publication_object.filename + '\n')
        except FileNotFoundError:
            with open('logs/file_not_exists.log', 'a+') as fne_log:
                fne_log.write(self._xml_dir + publication_object.filename + '.tei.xml' + '\n')

        return sentences

    def __check_extension(self, filename):
        if os.path.exists(self._xml_dir + filename + '.tei.xml'):
            return '.tei.xml'
        elif os.path.exists(self._xml_dir + filename + '.xml'):
            return '.xml'
        elif os.path.exists(self._xml_dir + filename + '.html'):
            return '.html'
        else:
            raise FileNotFoundError

    @staticmethod
    def __extract_sentences(docs):
        sentences = []
        for doc in docs:
            sentences = sentences + [sent.text.strip() for sent in doc.sents]
        return sentences

    @staticmethod
    def __extract_paragraph_texts(soup) -> list[str]:
        texts = []
        for p in soup.findAll('p'):
            texts.append(p.getText(strip=True))
        return texts

    # TODO: Redesign return types with inheritance
    def __add_publication_tool_relation(self, publication_persistent_id, tool_persistent_id) -> object:
        try:
            return self._sshoc_adapter.add_item_relation(publication_persistent_id, "mentions", tool_persistent_id)
        except HTTPError:
            raise HTTPError
