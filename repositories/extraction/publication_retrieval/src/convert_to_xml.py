import argparse
import logging
import sys
from pathlib import Path

from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter

sys.path.append(str(Path('.').absolute().parent.parent.parent))

from repositories.extraction.publication_retrieval.src.format_converter import FormatConverter

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser(description='Converts downloaded pdfs to xml with Grobid')
parser.add_argument('--testrun', action='store_true',
                    help='Run in test mode.')

args = parser.parse_args()

if __name__ == '__main__':
    testrun = args.testrun

    rest_client = RestClient()
    sshoc_adapter = SshocAdapter(rest_client)

    pdf_path = 'pdf_test/' if testrun else 'pdf/'
    xml_path = 'output_test/' if testrun else 'output/'
    tool_candidate_extractor = FormatConverter(pdf_save_path=pdf_path, xml_dir=xml_path)
    tool_candidate_extractor.convert_pdf_to_xml()
