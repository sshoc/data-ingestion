import logging
import pickle

from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter, SshocVocabulary

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

page = None

pubs = []

rest_client = RestClient()
sshoc_adapter = SshocAdapter(rest_client)

if page is not None:
    retrieved = sshoc_adapter.get_items_of_page(sshoc_adapter.PUBLICATIONS_ENDPOINT,
                                                      SshocVocabulary.PUBLICATION, page)
    pubs.extend(retrieved)
else:
    pubs.extend(sshoc_adapter.get_publications())

with open('all_publications.pkl', 'wb+') as file:
    pickle.dump(pubs, file)


