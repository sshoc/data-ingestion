import configparser
import logging
import sys

sys.path.insert(0, './grobid_client')
from repositories.extraction.publication_retrieval.grobid_client import grobid_client

class FormatConverter(object):
    GROBID_CONFIG = "grobid_client/config.json"

    def __init__(self, pdf_save_path='pdf/', xml_dir="output/", _grobid_client=None):
        self.logger = logging.getLogger(__name__)
        self.config = configparser.ConfigParser()
        self.config.read('config.INI')
        self._pdf_dir = pdf_save_path
        self._xml_dir = xml_dir
        self._grobid_client = _grobid_client
        if _grobid_client is None:
            self._grobid_client = grobid_client.GrobidClient(config_path=self.GROBID_CONFIG)

    def convert_pdf_to_xml(self):
        self.logger.info("Converting pdf documents to xml")

        self._grobid_client.process("processFulltextDocument", self._pdf_dir, output=self._xml_dir)