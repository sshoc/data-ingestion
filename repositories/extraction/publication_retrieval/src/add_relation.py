import pickle
import sys
from pathlib import Path

import jsons
import yaml
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter
sys.path.append(str(Path('.').absolute().parent.parent.parent))
from repositories.extraction.publication_retrieval.src.publication_tool_extractor import SshocToolCandidateExtractor

with open("params.yaml", 'r') as fd:
    params = yaml.safe_load(fd)

with open("../ner_ml/metadata.yaml", 'r') as md:
    model_metadata = yaml.safe_load(md)

ingest = params['ingest']
model_version = model_metadata['model']['version']

if __name__ == '__main__':
    if ingest:
        updated = []
        with open("publications/pub2pdf_with_candidates.pkl", 'rb') as pickle_file:
            publication2pdf = pickle.load(pickle_file)
            rest_client = RestClient()
            sshoc_adapter = SshocAdapter(rest_client)
            tool_candidate_extractor = SshocToolCandidateExtractor(sshoc_adapter, model_version)

            for pub in publication2pdf:
                print('adding relation')
                added, updated_pub = tool_candidate_extractor.process_publication_tool_relation(pub)
                for rel in added:
                    print(rel)
                print('updated pub')
                print(updated_pub)

                if len(added) > 0 and updated is not None:
                    updated.append({'pub': pub,'relation': added})

        if len(updated) > 0:
            with open("relations/added_relations.pkl", 'wb+') as added_file:
                pickle.dump(updated, added_file)
            with open("metrics/added_relations_stats.json", 'w') as stats_file:
                stat = {
                    "total_number_of_added_relations": len(updated)
                }
                stats_file.write(jsons.dumps(stat))
            with open("metrics/added_relations.json", 'w') as added_rels_file:
                added_rels = {
                    "rels": updated
                }
                added_rels_file.write(jsons.dumps(added_rels))
