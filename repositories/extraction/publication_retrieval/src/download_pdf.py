import argparse
import pickle
import sys

import jsons
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter

sys.path.insert(0, 'src')

from publication_tool_extractor import Publication, SshocToolRetriever

parser = argparse.ArgumentParser(description='Download pdf publications')
parser.add_argument('--testrun', action='store_true',
                    help='Run in test mode.')

args = parser.parse_args()

if __name__ == '__main__':
    testrun = args.testrun
    filename_postfix = 'test' if testrun else 'prod'

    with open('publications/publications_' + filename_postfix + ".pkl", 'rb') as pickle_file:
        pubs = pickle.load(pickle_file)

    rest_client = RestClient()
    sshoc_adapter = SshocAdapter(rest_client)
    pdf_path = 'pdf_test' if testrun else 'pdf'
    xml_path = 'output_test' if testrun else 'output'

    tool_retriever = SshocToolRetriever(sshoc_adapter, pdf_save_path=pdf_path, xml_dir=xml_path)
    publication2pdf: [Publication] = tool_retriever.download_publications(pubs, pdf_path)

    with open('publications/publications2pdf_' + filename_postfix +  '.pkl', 'wb+') as file:
        pickle.dump(publication2pdf, file)

    with open('publications/downloaded_pdf_' + filename_postfix + '.json', 'w') as file:
        pubObj = {
            "pubList": publication2pdf
        }
        jsonObj = jsons.dumps(pubObj)
        file.write(jsonObj)

    with open('metrics/download_stats_' + filename_postfix + '.json', 'w') as file:
        stat = {
            "number_of_downloaded_pubs": len(publication2pdf)
        }
        file.write((jsons.dumps(stat)))
