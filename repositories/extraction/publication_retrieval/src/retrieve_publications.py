import argparse
import pickle
import sys

import jsons as jsons
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter
from pathlib import Path

from publication_tool_extractor import SshocToolRetriever

parser = argparse.ArgumentParser(description='Retrieve publications from marketplace')
parser.add_argument('--testrun', action='store_true',
                    help='Run in test mode.')
parser.add_argument('--page-limit', type=int, help='limit number of pages to retrieve from MP')

args = parser.parse_args()

if __name__ == '__main__':
    testrun = args.testrun
    skip_existing = not testrun
    page_limit = args.page_limit

    rest_client = RestClient()
    sshoc_adapter = SshocAdapter(rest_client)
    tool_candidate_extractor = SshocToolRetriever(sshoc_adapter)
    # Retrieve publications with paging
    # omitting page parameter will retrieve all publications
    publications = tool_candidate_extractor.get_sshoc_publications_with_valid_link(skip_existing=skip_existing,
                                                                                   page_limit=page_limit)

    Path("publications").mkdir(parents=True, exist_ok=True)

    filename_postfix = 'test' if testrun else 'prod'
    with open('publications/publications_' + filename_postfix + '.pkl', 'wb+') as file:
        pickle.dump(publications, file)

    with open('publications/publications_' + filename_postfix + '.json', 'w') as file:
        pubObj = {
            "pubList": publications
        }
        jsonObj = jsons.dumps(pubObj)
        file.write(jsonObj)

    with open('metrics/retrieval_stats_' + filename_postfix + '.json', 'w') as file:
        stat = {
            "number_of_retreived_pubs": len(publications)
        }
        file.write(jsons.dumps(stat))
