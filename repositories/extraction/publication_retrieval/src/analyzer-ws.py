import configparser
import logging
import os
import time

import spacy
import spacy.cli
import sys
import csv

from itertools import filterfalse
from datetime import datetime, timezone
from model.sshoc_tool import SshocItem, SshocPublication
from openapi_client import *
from requests.exceptions import HTTPError
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter, SshocVocabulary
from bs4 import BeautifulSoup

# sys.path.insert(0, './')
from spacy.tokens import Doc

sys.path.insert(0, './grobid_client')

# sys.path.insert(0, '../grobid_client')
# sys.path.insert(0, '../')

# print(sys.path)
# print(os.getcwd())
from repositories.extraction.publication_retrieval.grobid_client import *

from repositories.extraction.publication_retrieval.grobid_client import grobid_client

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class PublicationPdf():
    def __init__(self, publication: SshocPublication, filename: str, link: str):
        self._publication: SshocPublication = publication
        self._filename: str = filename
        self._link: str = link
        self._tool_candidates: [ToolCandidate] = []

    @property
    def filename(self):
        return self._filename

    @property
    def link(self):
        return self._link

    @property
    def tool_candidates(self):
        return self._tool_candidates

    @tool_candidates.setter
    def tool_candidates(self, tool_candidates):
        self._tool_candidates = tool_candidates

    @property
    def publication(self):
        return self._publication

    @publication.setter
    def publication(self, publication):
        self._publication = publication


def exists_processed_at(item: PublicationCore) -> bool:
    if hasattr(item, 'properties'):
        for prop in item.properties:
            if prop.type.code == 'processed-at' and prop.value is not None:
                return True
    return False


def check_accessible_at(item: SshocItem) -> bool:
    if hasattr(item, 'accessible_at'):
        try:
            for link in item.accessible_at:
                if ".pdf" in link:
                    return True
        except Exception as e:
            logging.error('Error occurred while checking pdf link: ' + item.label)
    return False


class ToolCandidate(object):
    def __init__(self, persistent_id, label):
        self._persistent_id = persistent_id
        self._label = label

    @property
    def persistent_id(self):
        return self._persistent_id

    @property
    def label(self):
        return self._label

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.persistent_id == other.persistent_id and
                self.label == other.label
        )

    def __hash__(self):
        return hash(self._persistent_id)


class WikidataAdapter(object):
    def __init__(self, rest_client: RestClient):
        self.logger = logging.getLogger(__name__)

        self._rest_client = RestClient()

    def lookup_wikidata_with_toolname(self, tool_name):
        found = []
        query_string = """
            SELECT DISTINCT ?item   ?itemLabel_en WHERE {
            ?item ?label "%s"@en;
            wdt:P31 wd:Q7397 .
            ?item rdfs:label ?itemLabel_en . FILTER(lang(?itemLabel_en)='en')
            }""" % (tool_name)

        params = {'query': query_string}
        headers = {'Accept': 'application/sparql-results+json',
                   'User-Agent': 'sshoc-mp/1.0 (https://sshoc-marketplace.acdh-dev.oeaw.ac.at) generic-library/1.0'}
        try:
            res = self._rest_client.get('https://query.wikidata.org/sparql', params=params, headers=headers).json()
            matches = res["results"]["bindings"]
            if len(matches) >= 1:
                for row in matches:
                    tool = ToolCore(label=tool_name, description=row["item"]["value"])
                    found.append(tool)
        except HTTPError:
            self.logger.warning('Failed to query wikidata for tool: ' + tool_name + ' with query ' + query_string)

        return found


class SshocToolCandidateExtractor(object):
    _sshoc_adapter: SshocAdapter
    GROBID_CONFIG = "grobid_client/config.json"

    def __init__(self, sshoc_adapter: SshocAdapter, pdf_save_path='pdf/', xml_dir="output/", _grobid_client=None):
        self.logger = logging.getLogger(__name__)
        self.config = configparser.ConfigParser()
        self.config.read('config.INI')
        self._sshoc_adapter = sshoc_adapter
        self._pdf_dir = pdf_save_path
        self._xml_dir = xml_dir
        self._nlp = spacy.load("en_core_web_sm")
        self._nlp.remove_pipe("ner")
        self.logger.info(self._nlp.pipeline)
        self._pretrained_model = spacy.load("../models/tools_model_with_corrections_pretrained")
        self._grobid_client = _grobid_client
        if _grobid_client is None:
            self._grobid_client = grobid_client.GrobidClient(config_path=self.GROBID_CONFIG)
        self._rest_client = RestClient()
        self._wiki_adapter = WikidataAdapter(RestClient())

    def get_sshoc_publications_with_pdf_link(self, page=None):
        pubs = []
        if page is not None:
            retrieved = self._sshoc_adapter.get_items_of_page(self._sshoc_adapter.PUBLICATIONS_ENDPOINT,
                                                              SshocVocabulary.PUBLICATION, page)
            pubs.extend(retrieved)
        else:
            pubs.extend(self._sshoc_adapter.get_publications())

        self.logger.info('total # of pubs:' + str(len(pubs)))
        pubs_with_accessible_at = list(filter(check_accessible_at, pubs))
        return pubs_with_accessible_at

    def download_pdfs(self, publications: [SshocPublication]) -> [PublicationPdf]:
        result = []

        for pub in publications:
            for idx, link in enumerate(pub.accessible_at):
                try:
                    response = self._rest_client.get(link)
                    filename = self.__write_pdf(idx, pub, response)
                    result.append(PublicationPdf(pub, filename, link))
                except HTTPError:
                    self.logger.debug('Error while downloading pdf:' + link)

        self.logger.info("PDF download completed..")
        return result

    def convert_pdf_to_xml(self):
        self.logger.info("Converting pdf documents to xml")

        self._grobid_client.process("processFulltextDocument", self._pdf_dir, output=self._xml_dir)

    def extract_tool_candidate_names(self, publication_object: PublicationPdf) -> {str}:
        self.logger.info("Extracting tool candidate names from XML")

        sentences: [str] = self.__convert_xml_to_sentences(publication_object)
        tool_names, sents_and_tools = self.__extract_toolnames(sentences)

        return set(tool_names), sents_and_tools

    def retrieve_tool_candidates_from_marketplace(self, tool_candidate_names: [str]) -> [ToolCandidate]:
        existing = []
        non_existing = []

        for tool_name in tool_candidate_names:
            self.logger.info('tool name: ' + tool_name)
            search_result = self._sshoc_adapter.search_item(tool_name, category=SshocVocabulary.TOOLS_OR_SERVICE[
                SshocVocabulary.VocabularyType.ENTITY_TYPE.value], order="score")

            search_result = list(filter(lambda x: x.label.lower == tool_name.lower, search_result))
            if len(search_result) == 0:
                non_existing.append(tool_name)
            elif len(search_result) >= 1:
                tool = search_result[0]
                existing.append(ToolCandidate(tool.persistent_id, tool.label))

        # TODO What if > 1 exact matches?

        return set(existing), non_existing

    def retrieve_tool_candidates_from_wikidata(self, non_existing: [str]):
        # TODO Move this check to sshoc adapter
        if self._sshoc_adapter.authToken is None:
            self._sshoc_adapter.sign_in(self.config['AUTH']['username'], self.config['AUTH']['password'])
        existing = []
        wiki_non_existing = []

        for tool_name in non_existing:
            found = self._wiki_adapter.lookup_wikidata_with_toolname(tool_name)

            if len(found) == 0:
                wiki_non_existing.append(tool_name)
            else:
                for tool_found in found:
                    tool_created = self._sshoc_adapter.create_item(tool_found, SshocVocabulary.TOOLS_OR_SERVICE)
                    existing.append(ToolCandidate(tool_created.persistent_id, tool_created.label))

        return set(existing), wiki_non_existing

    def process_publication_tool_relation(self, publication_holder: PublicationPdf):
        if self._sshoc_adapter.authToken is None:
            self._sshoc_adapter.sign_in(self.config['AUTH']['username'], self.config['AUTH']['password'])
        if len(publication_holder.tool_candidates) == 0:
            self.logger.info("No tool candidates to add for publication: " + publication_holder.publication.label)
            return [], None

        added_relations = []
        for candidate in publication_holder.tool_candidates:
            try:
                relation = self.__add_publication_tool_relation(publication_holder.publication.persistent_id,
                                                                candidate.persistent_id)
                added_relations.append(relation)
            except HTTPError:
                self.logger.warning(
                    "Ingesting relation failed: " + publication_holder.publication.persistent_id + candidate.persistent_id)

        retrieved_pub = self._sshoc_adapter.get_publication(publication_holder.publication.persistent_id)
        if retrieved_pub.persistent_id == publication_holder.publication.persistent_id:
            publication_holder.publication = retrieved_pub
        publication: PublicationCore = self.__prepare_publication_payload(publication_holder.publication)

        updated_publication: SshocItem = self._sshoc_adapter.update_publication(
            publication_holder.publication.persistent_id, publication)

        return added_relations, updated_publication

    @staticmethod
    def __prepare_publication_payload(publication: SshocPublication) -> PublicationCore:
        now = datetime.now(timezone.utc)
        now_iso = now.strftime("%Y-%m-%dT%H:%M:%S%z")

        property_type_id = PropertyTypeId(code='processed-at')
        _property = PropertyCore(type=property_type_id, value=now_iso)

        publication.properties.append(_property)

        return publication

    def __write_pdf(self, idx, pub, response):
        filename = pub.label + '_' + str(idx)
        os.makedirs(os.path.dirname(self._pdf_dir + filename), exist_ok=True)

        with open(self._pdf_dir + filename + '.pdf', 'wb+') as f:
            f.write(response.content)

        return filename

    def __extract_toolnames(self, sentences) -> [str]:
        tool_names = []
        sent_of_tools = {}
        for doc in self._pretrained_model.pipe(sentences, disable=["tagger", "parser"]):
            for ent in doc.ents:
                # spacy.displacy.render(doc, style='ent', jupyter=True)
                tool_names.append(ent.text)
                sent_of_tools[ent.text] = ent.sent
        return tool_names, sent_of_tools

    def __convert_xml_to_sentences(self, publication_object: PublicationPdf) -> [str]:
        tic = time.perf_counter()

        with open(self._xml_dir + publication_object.filename + '.tei.xml', 'r') as tei:
            soup_tic = time.perf_counter()
            soup = BeautifulSoup(tei, 'lxml')
            soup_toc = time.perf_counter()

            self.logger.debug(f"Beautiful Soup Converted in {soup_toc - soup_tic:0.4f} seconds")

            texts: list[str] = self.__extract_paragraph_texts(soup)
            docs: list[Doc] = list(self._nlp.pipe(texts))
            sentences: list[str] = self.__extract_sentences(docs)

        toc = time.perf_counter()
        self.logger.debug(f"Converted in {toc - tic:0.4f} seconds")

        return sentences

    @staticmethod
    def __extract_sentences(docs) -> list[str]:
        sentences = []
        for doc in docs:
            sentences = sentences + [sent.text.strip() for sent in doc.sents]
        return sentences

    @staticmethod
    def __extract_paragraph_texts(soup) -> list[str]:
        texts = []
        for p in soup.findAll('p'):
            texts.append(p.getText(strip=True))
        return texts

    # TODO: Redesign return types with inheritance
    def __add_publication_tool_relation(self, publication_persistent_id, tool_persistent_id) -> object:
        try:
            return self._sshoc_adapter.add_item_relation(publication_persistent_id, "mentions", tool_persistent_id)
        except HTTPError:
            raise HTTPError


def main():
    print('running:' + os.getcwd())
    rest_client = RestClient()
    sshoc_adapter = SshocAdapter(rest_client)
    tool_candidate_extractor = SshocToolCandidateExtractor(sshoc_adapter)
    # Demonstration of tool extraction happy path
    # Retrieve publications with paging
    # omitting page parameter will retrieve all publications
    publications = tool_candidate_extractor.get_sshoc_publications_with_pdf_link()

    publication2pdf: [PublicationPdf] = tool_candidate_extractor.download_pdfs(publications)

    tool_candidate_extractor.convert_pdf_to_xml()

    analyse = []

    for pub in publication2pdf:
        none_existing = []
        candidate_names, can_sents_and_tools = tool_candidate_extractor.extract_tool_candidate_names(pub)
        print('Tool Candidates:')
        print(candidate_names)
        pub.tool_candidates, none = tool_candidate_extractor.retrieve_tool_candidates_from_marketplace(
            candidate_names)
        wiki_candidates, none = tool_candidate_extractor.retrieve_tool_candidates_from_wikidata(none)
        pub.tool_candidates = pub.tool_candidates | wiki_candidates
        none_existing.extend(none)

        for tool in none:
            analyse.append([pub.link, tool, can_sents_and_tools[tool]])

    with open('analyse.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(analyse)
    print(analyse)


if __name__ == "__main__":
    main()
