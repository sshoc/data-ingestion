from model.sshoc_tool import SshocPublication
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter

from repositories.extraction.publication_retrieval.src.publication_tool_extractor import SshocToolCandidateExtractor

if __name__ == '__main__':
    rest_client = RestClient()
    sshoc_adapter = SshocAdapter(rest_client)
    tool_candidate_extractor = SshocToolCandidateExtractor(sshoc_adapter)
    # Retrieve publications with paging
    # omitting page parameter will retrieve all publications
    publications = tool_candidate_extractor.get_sshoc_publications_with_valid_link(skip_existing=False)

    pub_with_relations_count = 0
    relations_count = 0
    for pub in publications:
        relations_count += len(pub.related_items)
        if len(pub.related_items) > 0:
            for rel in pub.related_items:
                if 'category' in rel and rel['category'] == 'tool-or-service':
                    pub_with_relations_count += 1

    print('# Publications ' + str(len(publications)))
    print('# Publications with relations ' + str(pub_with_relations_count))
    print('# Relations ' + str(relations_count))

