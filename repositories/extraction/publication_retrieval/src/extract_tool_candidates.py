import argparse
import csv
import json
import pickle
import random

import sys
import jsons
import logging

from pathlib import Path
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter

sys.path.append(str(Path('.').absolute().parent.parent.parent))

from repositories.extraction.publication_retrieval.src.publication_tool_extractor import SshocToolCandidateExtractor, \
    Publication

parser = argparse.ArgumentParser(description='Extract tool names using NER model')
parser.add_argument('model_version')
parser.add_argument('--testrun', action='store_true',
                    help='Run in test mode.')

args = parser.parse_args()

if __name__ == '__main__':
    testrun = args.testrun
    model_version = args.model_version

    mode_postfix = 'test' if testrun else model_version + 'prod'
    filename_postfix = model_version + '_' + mode_postfix

    logging.info(filename_postfix)

    pdf_path = 'pdf_test/' if testrun else 'pdf/'
    xml_path = 'output_test/' if testrun else 'output/'

    with open('publications/publications2pdf_' + mode_postfix + '.pkl', 'rb') as pickle_file:
        publication2pdf: [Publication] = pickle.load(pickle_file)

        rest_client = RestClient()
        sshoc_adapter = SshocAdapter(rest_client)
        tool_candidate_extractor = SshocToolCandidateExtractor(sshoc_adapter, model_version=model_version, pdf_save_path=pdf_path, xml_dir=xml_path)

        count_sentences = 0

        pubs_with_tool_candidates = []
        pubs_without_tool_candidates = []
        all_tool_names = []
        all_extracted_tools = []
        none_existing = []
        analyse = []
        mp_found_tools = []
        wikidata_found_tools = []

        for pub in publication2pdf:
            candidates_to_sentences, sentences = tool_candidate_extractor.extract_tool_candidate_names(pub)
            count_sentences += len(sentences)
            logging.debug('Tool Candidates:')
            logging.debug(candidates_to_sentences.keys())
            all_tool_names.extend(candidates_to_sentences.keys())

            pub.tool_candidates, none = tool_candidate_extractor.retrieve_tool_candidates_from_marketplace(
                candidates_to_sentences.keys())
            wiki_candidates, none = tool_candidate_extractor.retrieve_tool_candidates_from_wikidata(none)

            mp_found_tools += pub.tool_candidates
            wikidata_found_tools += wiki_candidates
            pub.tool_candidates = pub.tool_candidates | wiki_candidates

            if len(pub.tool_candidates) > 0:
                pubs_with_tool_candidates.append(pub.publication.persistent_id)
            else:
                pubs_without_tool_candidates.append(pub.publication.persistent_id)

            none_existing.extend(none)

            for tool in pub.tool_candidates:
                try:
                    all_extracted_tools.append(
                        [pub.link, tool.label, 'marketplace', candidates_to_sentences[tool.label]])
                except KeyError:
                    with open('logs/sentences_key_not_exists_' + filename_postfix + '.log', 'a+') as fne_log:
                        fne_log.write(tool.label + 'no sentences' + '\n')
            for tool in wiki_candidates:
                try:
                    all_extracted_tools.append([pub.link, tool.label, 'wikidata', candidates_to_sentences[tool.label]])
                except KeyError:
                    with open('logs/sentences_key_not_exists_wiki_' + filename_postfix + '.log', 'a+') as fne_log:
                        fne_log.write(tool.label + 'no sentences' + '\n')

            for tool in none:
                analyse.append([pub.link, tool, candidates_to_sentences[tool]])
                all_extracted_tools.append([pub.link, tool, 'none', candidates_to_sentences[tool]])

        with open('candidates/analyse_' + filename_postfix + '.csv', 'w+', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(analyse)
        logging.debug(analyse)

        with open('candidates/extracted_toolnames_' + filename_postfix + '.csv', 'w+', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(all_extracted_tools)
        logging.debug(all_extracted_tools)

        with open('publications/pub2pdf_with_candidates_' + filename_postfix + '.pkl', 'wb+') as pub2pdf:
            pickle.dump(publication2pdf, pub2pdf)

        with open('candidates/none_existing_' + filename_postfix + '.pkl', 'wb+') as none_existing_file:
            pickle.dump(none_existing, none_existing_file)

        with open('candidates/analyse_' + filename_postfix + '.pkl', 'wb+') as analyse_file:
            pickle.dump(analyse, analyse_file)

        with open('candidates/sentences_to_analyse_' + filename_postfix + '.jsonl', 'w+') as analyse_sentences:
            random.shuffle(analyse)
            for entry in analyse:
                for sent in entry[2]:
                    sent = sent.strip('\n')
                    print(sent)
                    sent_dict = {"text": sent}
                    analyse_sentences.write(json.dumps(sent_dict))
                    analyse_sentences.write('\n')

        with open('candidates/all_sentences_with_tool_candidates_' + filename_postfix + '.txt', "w+") as sentences_file:
            for entry in all_extracted_tools:
                for sent in entry[3]:
                    sentences_file.write(sent)

        logging.info('Total Number of sentences: ' + str(count_sentences))

        with open('metrics/extraction_stats_' + filename_postfix + '.json', "w+") as stats_file:

            stat = {
                "total_number_of_pubs": len(publication2pdf),
                "number_of_extracted_toolnames": len(all_tool_names),
                "number_of_distinct_extracted_toolnames": len(set(all_tool_names)),
                "number_of_pubs_without_tool_mentions": len(set(pubs_without_tool_candidates)),
                "number_of_pubs_with_tool_mentions": len(set(pubs_with_tool_candidates)),
                "total_number_of_sentences": count_sentences,
                "tools to number of sentences ratio": len(all_tool_names)/count_sentences,
                "pubs with mention to total pubs ratio": len(set(pubs_with_tool_candidates))/len(publication2pdf),
                "tools found on marketplace": len(set(mp_found_tools)),
                "tools found on wikidata": len(set(wikidata_found_tools)),
            }
            stats_file.write(jsons.dumps(stat))
