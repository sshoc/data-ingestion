# PREREQUISITES`
1. Setup virtual environment with python version 3.9.x

   
2. Set environment variable for google cloud storage
   
   ```export GOOGLE_APPLICATION_CREDENTIALS=[PATH_TO_CREDENTIAL FILE]```


3. Run ```dvc pull```
   

4. Run ```python -m pip install --upgrade build```
   

5. Run ```python -m build openaire```
   

6. Run ```pip install -r repositories/extraction/publication_retrieval/requirements.txt```
   

7. Add repository root to PYTHONPATH
   
   ```export PYTHONPATH=$PYTHONPATH:/{REPOSITORY_ROOT}```


8. Add config.INI with the content below to ```repositories/extraction/publication_retrieval```


```
[SSHOC]
API_BASE_URI=http://localhost:8080/api/

[AUTH]
username=XXXXXX
password=XXXXXX
```

9. For local testing, SSHOC BACKEND Service must be running on the local machine.

   - Start backend: ```docker-compose up```
   - Stop backend: ```docker-compose stop```  
   - Reset DB : ```docker-compose down --volumes```
   

10. grobid server must be running locally


```
docker run -t --rm --init -p 9000:8070 -p 9001:8071 lfoppiano/grobid:0.6.2
```

11. Run unittests from repositories/extraction/publication_retrieval directory

```python -m unittest```

# Tool Extraction Process

Each step of the pipeline is specified as a DVC pipeline stage.
The specification can be viewed in the dvc.yaml file.

1. Sign in
   - Input: Username, password
   - Outout: Token 
2. Get list of publications
   - Input: -
   - Output: [Publications] 
3. Filter publications with pdf link ('accessibleAt')
   - Input: [Publications]
   - Output: [{Publications - [PDF_LINK]}]
4. Download pdf with the link.
   - Input: [{Publication - [PDF_LINK]}]
   - Output: [{Publication - [PDF_LINK] - [FilePath]}]
5. Extract tool candidates from downloaded pdfs
   - Input: {Publication - [PDF_LINK]}
   - Output: [str]
5a. Convert to xml
   - Input: {Publication - [PDF_LINK] - [PdfFilePath]}  
   - Output: [{Publication - [PDF_LINK] - [PdfFilePath]}
5.b. Extract tool names
    - Input: [{Publication - [PDF_LINK] - [PdfFilePath]}
    - Output: [str]
6. Search for existing tool entity in the MP
   - Use item-search
     - item-search might return multiple results
       - we go for exact string match
     - if no match,
       - add it to the non-existing list
   - Input : toolname(string)
   - Output: tool_id(string)
7. Search for non-existing tool entity in the Wikidata
   - Use Wikidata SPARQL endpoint
      - if exists
         - create a tool in th MP with Wikidata information
      - if no match
         - keep them in the non-existing list (used for the model)
   - Input : non-existing list [str]
   - Output: tool_id(string)

-----------------------------
This could be one operation for consistency
8. Add publication - tool relation: "mentions"/"is-mentioned-in"
   - Input: [PublicationPdf]
   - Output:
9. Update publication with 'processed_at'
--------------
