#!/bin/sh
set -e
set -x
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mkdir -p ~/.creds
chmod 700 ~/.ssh
MODEL_VERSION=$1

cd /app
pip install --upgrade pip setuptools wheel
GITLAB_TOKEN=$(echo $GITLAB)
export CONFIG_FILE=/app/data-ingestion/repositories/extraction/publication_retrieval/config/config.yaml
git clone https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git
cd data-ingestion

### INSTALL DEPENDENCIES ###
cd /app/data-ingestion/openaire
pip install -U build
python -m build
pip install 'dvc[gs]'
cd /app/data-ingestion || exit
pip install -U -r /app/data-ingestion/repositories/extraction/publication_retrieval/requirements.txt
cd /app/data-ingestion/repositories/extraction/ || exit
dvc pull -v ner_ml/model/tools_model_with_corrections_pretrained_$MODEL_VERSION
cd /app/data-ingestion/repositories/extraction/publication_retrieval
dvc status
dvc repro -p retrieve_pubs
#dvc push
#git status
#git add .
#git config --global user.email "seung-bin.yim@oeaw.ac.at"
#git config --global user.name "Seung-bin Yim"
#tagname=$(date +%y.%m.%d)
#git tag -a $tagname -m "Tool Extraction execution version"
#git push origin $tagname