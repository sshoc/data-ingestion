from unittest import TestCase
from unittest.mock import Mock

from model.sshoc_tool import SshocItem, Triple, SshocPublication
from rest.sshoc_adapter import SshocVocabulary

from repositories.extraction.publication_retrieval.src.publication_tool_extractor import SshocToolCandidateExtractor, \
    Publication, ToolCandidate

TEST_LINK = "TEST_LINK"

TEST_ID = "TEST_ID"

TEST_DESCRIPTION = "TEST_DESCRIPTION"

TEST_LABEL = "TEST_LABEL"

TEST_PERSISTENT_ID_2 = "TEST_PERSISTENT_ID_2"

PDF_LINK = "http://TEST.com/TEST.pdf"

TEST_FILENAME = "TEST_PDF"

TEST_PERSISTENT_ID = "TEST_PERSISTENT_ID"

TEST_PUBLICATION = SshocPublication(
    persistent_id=TEST_PERSISTENT_ID,
    label="1947Partition On The Margins - The Untold Testimonies Of Sikh, Bahawalpur And Marwari Communities",
    description="No description provided.",
    accessible_at=["https://dh2017.adho.org/abstracts/081/081.pdf"],
    properties=[]
)


class TestSshocToolCandidateExtractor(TestCase):
    def setUp(self) -> None:
        self.sshoc_adapter = Mock()
        self.sshoc_tool_extractor = SshocToolCandidateExtractor(self.sshoc_adapter, pdf_save_path="test/pdf/",
                                                                xml_dir="test/output/", _grobid_client=Mock())

    def test_extract_tool_candidates_names(self):
        result: {str: [str]} = self.sshoc_tool_extractor.extract_tool_candidate_names(Publication(TEST_PUBLICATION, "TEST_NAME", TEST_LINK))

        self.assertGreater(len(result), 0)
        assert all(len(el) > 0 for el in result.values())

    def test_retrieve_non_existing_tools(self):
        self.sshoc_adapter.search_item.return_value = []
        tool_label = 'TEST'
        tool_names = [tool_label]

        result: [ToolCandidate] = self.sshoc_tool_extractor.retrieve_tool_candidates_from_marketplace(tool_names)[1]

        self.sshoc_adapter.search_item.assert_called()
        self.assertGreater(len(result), 0)

    def test_retrieve_tool_ids_for_existing_tools(self):
        self.sshoc_adapter.search_item.return_value = [
            SshocItem(TEST_LABEL, TEST_DESCRIPTION, TEST_ID, TEST_PERSISTENT_ID,
                      SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value])]
        tool_names = [TEST_LABEL]

        result: [ToolCandidate] = self.sshoc_tool_extractor.retrieve_tool_candidates_from_marketplace(tool_names)[0]

        self.assertGreater(len(result), 0)
        for candidate in result:
            self.assertIsNotNone(candidate.persistent_id)
            self.assertEqual(candidate.label, TEST_LABEL)

    def test_add_publication_to_tool_relation(self):
        publication_holder = Publication(TEST_PUBLICATION, TEST_FILENAME, PDF_LINK)
        tool_candidate = ToolCandidate(TEST_PERSISTENT_ID_2, TEST_LABEL)
        self.sshoc_adapter.add_item_relation.return_value: Triple = Triple(TEST_PERSISTENT_ID, "mentions",
                                                                           TEST_PERSISTENT_ID_2)
        self.sshoc_adapter.update_publication.return_value: SshocItem = SshocItem(TEST_LABEL, TEST_DESCRIPTION)
        publication_holder.tool_candidates = [tool_candidate]
        publication_core = SshocPublication(
            persistent_id="TEST",
            label="1947Partition On The Margins - The Untold Testimonies Of Sikh, Bahawalpur And Marwari Communities",
            description="No description provided.",
            accessible_at=["https://dh2017.adho.org/abstracts/081/081.pdf"],
            properties=[]
        )
        self.sshoc_adapter.get_publications.return_value = [publication_core]

        added_relations, updated_publication = self.sshoc_tool_extractor.process_publication_tool_relation(
            publication_holder)

        self.sshoc_adapter.add_item_relation.assert_called()
        self.sshoc_adapter.update_publication.assert_called()
        self.assertIsNotNone(updated_publication)
        self.assertIsInstance(updated_publication, SshocItem)
        self.assertIsNotNone(added_relations)
        self.assertEqual(len(added_relations), 1)
        self.assertIsInstance(added_relations[0], Triple)
