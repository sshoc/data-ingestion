import configparser
import unittest

from model.sshoc_tool import SshocPublication
from openapi_client import PublicationCore, ToolCore
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter, SshocVocabulary

from repositories.extraction.publication_retrieval.src.publication_tool_extractor import SshocToolCandidateExtractor, \
    Publication
from repositories.extraction.publication_retrieval.src.format_converter import FormatConverter


class ToolCandidateExtractorIT(unittest.TestCase):
    def setUp(self) -> None:
        self.config = configparser.ConfigParser()
        self.config.read('config.INI')
        self.rest_client = RestClient()
        self.sshoc_adapter = SshocAdapter(self.rest_client)
        self.sshoc_tool_extractor = SshocToolCandidateExtractor(self.sshoc_adapter, pdf_save_path="test/pdf/",
                                                                xml_dir="test/output/")
        self.format_converter = FormatConverter()

    def test_wikidata_retrieval(self):
        self.sshoc_adapter.sign_in(self.config['AUTH']['username'], self.config['AUTH']['password'])
        tnames = ["MAFFT"]
        wikit_candidates, wkt_none = self.sshoc_tool_extractor.retrieve_tool_candidates_from_wikidata(tnames)
        self.assertGreater(len(wikit_candidates), 0)

    def test_wikidata_check_IT(self):
        publication_pdfs = []

        publication = PublicationCore(
            label="1947Partition On The Margins - The Untold Testimonies Of Sikh, Bahawalpur And Marwari Communities",
            description="No description provided.",
            accessible_at=["https://dh2017.adho.org/abstracts/081/081.pdf"]
        )

        publication_pdfs.append(Publication(publication, filename="fqz029", link="i"))

        for pub in publication_pdfs:
            candidate_names: {str} = self.sshoc_tool_extractor.extract_tool_candidate_names(pub)
            pub.tool_candidates, mp_none = self.sshoc_tool_extractor.retrieve_tool_candidates_from_marketplace(
                candidate_names)
            wiki_candidates, wk_none = self.sshoc_tool_extractor.retrieve_tool_candidates_from_wikidata(mp_none)
            pub.tool_candidates = pub.tool_candidates | wiki_candidates

            self.assertGreater(len(wiki_candidates), 0)
            self.assertGreater(len(mp_none), 0)
            self.assertGreater(len(wk_none), 0)

    def test_update_publication_IT(self):
        self.sshoc_adapter.sign_in(self.config['AUTH']['username'], self.config['AUTH']['password'])

        tool = ToolCore(label="KSHIP", description="TEST")
        tool_added = self.sshoc_adapter.create_item(tool, SshocVocabulary.TOOLS_OR_SERVICE)
        publication = PublicationCore(
            label="1947Partition On The Margins - The Untold Testimonies Of Sikh, Bahawalpur And Marwari Communities",
            description="No description provided.",
            accessible_at=["https://dh2017.adho.org/abstracts/081/081.pdf"]
        )
        added: SshocPublication = self.sshoc_adapter.create_item(publication, SshocVocabulary.PUBLICATION)

        publications = self.sshoc_tool_extractor.get_sshoc_publications_with_valid_link(page=1)

        publication2pdf: [Publication] = self.sshoc_tool_extractor.download_publications(publications)

        self.format_converter.convert_pdf_to_xml()

        none_existing = []

        for pub in publication2pdf:
            candidate_names: {str} = self.sshoc_tool_extractor.extract_tool_candidate_names(pub)
            pub.tool_candidates, none = self.sshoc_tool_extractor.retrieve_tool_candidates_from_marketplace(
                candidate_names)
            wiki_candidates, none = self.sshoc_tool_extractor.retrieve_tool_candidates_from_wikidata(none)
            pub.tool_candidates = pub.tool_candidates | wiki_candidates
            none_existing.append(none)

        for pub in publication2pdf:
            added_relations, updated_pub = self.sshoc_tool_extractor.process_publication_tool_relation(pub)
            self.assertGreater(len(added_relations), 0)
            self.assertGreater(len(updated_pub.related_items), 0)
