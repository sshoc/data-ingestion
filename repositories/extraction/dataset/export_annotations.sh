#!/bin/bash
set -e
set -x
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mkdir -p ~/.creds
chmod 700 ~/.ssh

cd /app
pip install --upgrade pip setuptools wheel
GITLAB_TOKEN=$(echo $GITLAB)
git clone https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git

# TODO: remove checkout
cd data-ingestion
cd /app/data-ingestion
git checkout 96-ab-testing

cd /app/data-ingestion/repositories/extraction/
cp /app/prodigy.json /app/data-ingestion/repositories/extraction/prodigy.json

pip install pydantic==1.7.2
pip install 'dvc[gs]'
pip install psycopg2==2.8.6

PRODIGY_LOGGING=verbose prodigy db-out correct_analyze > /app/data-ingestion/repositories/extraction/dataset/all_samples.jsonl

dvc pull data/annotations/ann_manual.jsonl
dvc pull data/annotations/ann_corrected_p1.jsonl
cat data/annotations/ann_manual.jsonl data/annotations/ann_corrected_p1.jsonl dataset/all_samples.jsonl > dataset/all_samples_combined.jsonl
wc -l /app/data-ingestion/repositories/extraction/dataset/all_samples_combined.jsonl
split -l $(( $(wc -l /app/data-ingestion/repositories/extraction/dataset/all_samples_combined.jsonl | xargs | cut -d " " -f 1) * 80 / 100 )) /app/data-ingestion/repositories/extraction/dataset/all_samples_combined.jsonl
ls
mv xaa /app/data-ingestion/repositories/extraction/dataset/trainset.jsonl
mv xab /app/data-ingestion/repositories/extraction/dataset/testset.jsonl

echo "CHECKING SSHOC_TRAINSET"
TRAINSET_EXIST_CHECK=$(echo -n $(prodigy db-out sshoc_trainset . --dry))
echo $TRAINSET_EXIST_CHECK
if [[ "$TRAINSET_EXIST_CHECK" != *"Can't find 'sshoc_trainset'"* ]]; then
  echo "DROPPING SSHOC_TRAINSET"
  prodigy drop sshoc_trainset || true
  echo "DROPPED TRAINSET"
fi
echo "inserting TRAINSET..."
prodigy db-in sshoc_trainset /app/data-ingestion/repositories/extraction/dataset/trainset.jsonl
echo "TRAINSET inserted"

echo "checking SSHOC_TESTSET..."
TESTSET_EXIST_CHECK=$(echo -n $(prodigy db-out sshoc_testset . --dry))
echo $TESTSET_EXIST_CHECK
if [[ "$TESTSET_EXIST_CHECK" != *"Can't find 'sshoc_testset'"* ]]; then
  echo "DROPPING SSHOC_TESTSET"
  prodigy drop sshoc_testset  || true
  echo "DROPPED TESTSET"
fi
echo "inserting TESTSET"
prodigy db-in sshoc_testset /app/data-ingestion/repositories/extraction/dataset/testset.jsonl  || true
echo "TESTSET inserted"

#PRODIGY_LOGGING=verbose prodigy db-in sshoc_trainset /app/data-ingestion/repositories/extraction/dataset/trainset.jsonl

if [ "$DRYRUN" = false ]
then
  echo "committing new dataset"
  git config --global user.email "seung-bin.yim@oeaw.ac.at"
  git config --global user.name "Seung-bin Yim"
  git status
  dvc add /app/data-ingestion/repositories/extraction/dataset/all_samples_combined.jsonl
  dvc add /app/data-ingestion/repositories/extraction/dataset/trainset.jsonl
  dvc add /app/data-ingestion/repositories/extraction/dataset/testset.jsonl
  dvc push
  git add dataset/.gitignore dataset/all_samples_combined.jsonl.dvc dataset/trainset.jsonl.dvc dataset/testset.jsonl.dvc
  tagname=dataset_$(date +%y%m%d)
  git commit -m "Update dataset $tagname"  || true
  git push https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git  || true
  echo 'tagging commit as $tagname'
  git tag $tagname  || true
  git push https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git $tagname  || true
else
  echo "dryrun, no git/dvc add/commit executed"
fi

echo "END"