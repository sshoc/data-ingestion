import spacy

nlp = spacy.load('en_vectors_web_lg')
nlp.add_pipe(nlp.create_pipe('ner'))
nlp.to_disk('model/en_vectors_web_lg_ner')