# Requirements
You need prodigy installed.
Tested with python version 3.8.4

# DATASET


# TRAINING

The manual model was trained using 1000 manual annotations with the following scores :

`prodigy train ner ann_manual en_vectors_web_lg  --output ./tools_model_manual --eval-split 0.2 --n-iter 20`

| Label | Precision | Recall | F-Score |
| ---      |  ------  |----------| -------|
| Tool | 89.324 | 82.566 | 85.812 |


The resulted model is stored as [tools_model_manual](https://gitlab.gwdg.de/sshoc/data-ingestion/-/tree/patch-1/repositories/extraction/models).

The corrected model was trained with 583 additional annotations from correcting the model suggestions (1583 in total) with the following score :


`prodigy train ner ann_manual, ann_corrected_p1 en_vectors_web_lg --output ./tools_model_with_corrections --eval-split 0.2 --n-iter 20`


| Label | Precision | Recall | F-Score |
| ---      |  ------  |----------| -------|
| Tool | 90.406 | 87.814 | 89.091 |


The resulted model is stored as [tools_model_with_corrections](https://gitlab.gwdg.de/sshoc/data-ingestion/-/tree/patch-1/repositories/extraction/models).


The pretrained corrected model was pretrained with the whole corpus :



`prodigy train ner ann_manual, ann_corrected_p1 en_vectors_web_lg  --init-tok2ve data/pretrain/model.bin --output ./tools_model_with_corrections_pretrained --eval-split 0.2 --n-iter 20`


| Label | Precision | Recall | F-Score |
| ---      |  ------  |----------| -------|
| Tool |  91.135 | 92.115 | 91.622 |



The resulted model is stored as [tools_model_with_corrections_pretrained](https://gitlab.gwdg.de/sshoc/data-ingestion/-/tree/patch-1/repositories/extraction/models).
