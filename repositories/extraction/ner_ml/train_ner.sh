#!/bin/sh
set -e
set -x
eval $(ssh-agent -s)
mkdir -p ~/.ssh
mkdir -p ~/.creds
chmod 700 ~/.ssh

cd /app
pip install --upgrade pip setuptools wheel
GITLAB_TOKEN=$(echo $GITLAB)
git clone https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git
cd data-ingestion
cd /app/data-ingestion

#Install dependencies
#cd /app/data-ingestion/openaire
#pip install -U build
#python -m build
pip install pydantic==1.7.2
pip install psycopg2==2.8.6
pip install 'dvc[gs]'
cd /app/data-ingestion || exit
pip install -U -r /app/data-ingestion/repositories/extraction/ner_ml/requirements.txt
cd /app/data-ingestion/repositories/extraction/ || exit
dvc pull -v dataset/trainset.jsonl
dvc pull -v dataset/testset.jsonl
dvc pull -v data/pretrain/model.bin
cd /app/data-ingestion/repositories/extraction/ner_ml

dvc status
ls
dvc repro -f

if [ "$DRYRUN" = false ]
then
  echo "committing new model"
  dvc status
  dvc push

  git config --global user.email "seung-bin.yim@oeaw.ac.at"
  git config --global user.name "Seung-bin Yim"

  git status
  git add model/.gitignore dvc.lock .gitignore

  tagname=new_model_$(date +%y%m%d%S)
  git commit -m "Add new model $tagname"  || true
  git push https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git  || true
  echo 'tagging commit as $tagname'
  git tag $tagname  || true
  git push https://sbyim1:$GITLAB_TOKEN@gitlab.gwdg.de/sshoc/data-ingestion.git $tagname  || true
else
  echo "dryrun, no git/dvc add/commit executed"
fi
