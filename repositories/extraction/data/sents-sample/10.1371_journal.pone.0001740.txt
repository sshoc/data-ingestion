For this study, we used a compilation of 11087 records of forest tree diseases caused by parasitic fungi.
All the observations originated from the database of the Département Santé des Forêts (DSF) for the 1972–2005 period.
DSF is the French governmental organization in charge of forest health monitoring.
It consists of a network of skilled foresters evenly covering the state-owned and private forests of the country.
The primary aim of the DSF network is to prevent disease spread, pest outbreaks and other types of damage by alerting the authorities as soon as a threat to forest health is identified.
The foresters report all types of damage noticed during their daily work in the forest, when they consider that they may reduce the survival or the economical value of trees.
Their reports contain the full description of the observed symptoms and, when applicable, the in situ identification of the biotic agent which is responsible for them.
When the in situ morphological identification is tricky, samples are sent to the National Plant Protection Laboratory (LNPV) which specializes in the molecular identification of plant pests and pathogens.
When the damage of a forest plant has several causes (for instance, an insect attack followed by the spread of a fungal parasite), all of them are recorded and ranked in their order of importance.
Hence, pests and pathogens recorded in the database are not only primary pests and pathogens but also secondary or weakness pests and pathogens.
Over 67,000 cases of insect attack, fungal disease, abiotic stresses or decline were reported between 1972 and 2005.
For this study, we only used observations in which fungi had been identified to the species level.
We updated fungal species names and corrected species synonyms with the Index Fungorum database (www.indexfungorum.org).
We obtained a database of 157 fungal species (see Table S1 in Supplementary Material) and 51 host taxa (see Table S2), forming 547 interactions.
These data will be uploaded to the Interaction Web Database (www.nceas.ucsb.edu/interactionweb/).
All the fungal species belonged to Dikarya, which consists of two monophyletic phyla: Ascomycota and Basidiomycota.
Ascomycota is the largest phylum and is divided into three monophyletic subphyla: Taphrinomycotina, Saccharomycotina and Pezizomycotina.
Basidiomycota is also divided into three subphyla: Pucciniomycotina, Ustilagomycotina and Agaricomycotina [19].
Based on the Index Fungorum database (www.indexfungorum.org) and the NCBI taxonomy browser [20], we classified the fungal species into phyla and subphyla.
Only three species could not be assigned to a subphylum (Table S1).
We also classified the fungal species into 10 nutritional types (referred to as ‘life-history strategies’, as suggested by Garcia-Guzman and Morales [21] based on the parasitic lifestyle (biotroph versus necrotroph) and on the plant organs and tissues attacked: (1) strict foliar necrotroph parasites, (2) canker agents, (3) stem decay fungi, (4) obligate biotroph parasites, (5) root decay fungi, (6) other foliar and twig necrotroph parasites, (7) stem blue stain agents, (8) parasites of fine roots, (9) wilting agents, (10) other root fungi.
We included only the five first strategies, which accounted for 87% of the fungal species, in statistical analyses.
Of the 51 tree taxa, 41 corresponded to true species, 4 corresponded to groups of cultivars belonging to the same genetic continuum and 6 corresponded to groups of several species belonging to the same genus (Table S2).
The tree species were equally distributed between two phyla of Division Spermatophyta: the Magnoliophyta and the Coniferophyta.
We used the Angiosperm Phylogeny Website [22] to classify the tree taxa further, into seven subphyla (Proteales, Malpighiales, Fabales, Rosales, Fagales, Malvales, Sapindales, Lamiales and Pinales).
Moreover, phylogenetic distances between the species belonging to the Magnoliphyta (angiosperms) were estimated by using the Phylomatic software [23].
This tool allowed us to create an hypothesis for the phylogenetic relationships among the tree species based on the dated angiosperm supertree of Davies and collaborators [24].
The resultant tree was ultrametric, with branch length reflecting estimated time between branching events.
Pairwise phylogenetic distances were then extracted by using the cophenetic.phylo function of the R ape package [25].
An estimate of area covered by each tree taxon (hereafter called abundance) was available (Inventaire Forestier National, 2000 census report).
An estimate of the total number of times each tree taxon had been encountered and examined by foresters during their daily work was also available from the DSF database.
This number, further referred to as sampling intensity, was expected to be highly correlated with abundance.
We assumed that over the long period of the survey (1972–2005) and over the large geographical scale considered here (the entire French territory), the probability for a tree taxa of being damaged was on average equal for all tree taxa.
We therefore defined the sampling intensity of a tree taxon as the total number of DSF records for all types of damage (i.e., damage caused by insects, mammals, human activities or abiotic stresses).
Damage caused by parasitic fungi were excluded from the calculation in order to obtain an estimate of sampling intensity independent from the data analysed in this study (i.e., tree/fungus interaction records).
The Inventaire Forestier National, the French agency responsible for monitoring forest productivity and composition, has divided the country into 309 geographical units, each of which is homogeneous in terms of its climate, soil and relief.
Presence/absence data for all the tree taxa except one (Pinus radiata) were available for these 309 geographical units (Inventaire Forestier National, 2000 census report) and were used to estimate the distributional overlap between tree taxa.
Detection of compartmentalization and statistical validation:
We first identified the connected components of the interaction network and characterized them in terms of size, using the clusters function of the R igraph package [26].
The connected components of the network represent, in grossest terms, the pieces of the network: two vertices are in the same component if and only if there is some path between them.
We defined the size of a component as the number of its vertices.
We then applied the clustering algorithm proposed by Girvan and Newman [27] to the largest component, to highlight its structure.
As described by Girvan and Newman [27], we first calculated betweenness for all edges of the largest component, using the edge.betweenness function of the R igraph package [26].
Betweenness, which was originally defined for graph vertices [28], [29] and was then extended to graph edges [27], is approximately equal to the number of shortest paths going through a vertex or an edge.
As described by Girvan and Newman [27], if a network contains clusters that are loosely connected by a small number of edges, then edges connecting clusters have a high edge betweenness, because all shortest paths between different clusters must go along these edges.
Edges with the highest betweenness were therefore removed from the graph, and we recalculated betweenness for all the remaining edges [27].
This sequence was repeated until the clusters were separated.
Each cluster was then split in turn, starting with the largest.
The algorithm was repeated until no edges remained.
The nested hierarchy of clusters was converted into a tree format, using the as.phylo.formula function of the R ape package [25].
The hierarchical tree was represented with TreeView [30].
The order of tree leaves on the hierarchical tree was used to reorder the rows and columns of the interaction matrix.
The clusters were validated statistically by testing whether element similarity (i.e., similarity between fungal species as a function of host and similarity between host taxa as a function of their pathogens) was significantly higher within than between clusters.
We used the multiresponse permutation procedure (MRPP), a non parametric test of differences between predefined groups [31].
The MRPP statistics δ is the weighted mean of within-group means of pair-wise dissimilarity between group elements.
As described by Prado and Lewinshon [13], dissimilarity was calculated as a Jaccard distance and group size was taken as the group weight.
We used the mrpp function of the R vegan package [32] to calculate the expected statistics E(δ) if groups were assembled at random.
The within-group chance-corrected agreement (A), defined as 1- δ/E(δ), has a maximum of 1 when there is no dissimilarity between the elements of any group.
The P-value is the probability of obtaining, by chance, a value of A equal to or larger than the observed value.
Detection of nestedness and statistical validation:
Nestedness was defined as N = (100-T)/100, where T is the matrix temperature—a measure of matrix disorder with values ranging from 0° (perfectly nested) to 100° (perfectly non nested) [4].
Values of N close to 1 therefore indicate a high degree of nestedness.
The adjacency matrix was maximally packed to calculate T (see the article by Atmar and Patterson [33] for further details).
An isocline of perfect nestedness was then calculated and deviations from this isocline (i.e., unexpected presences and absences of interactions deviating from a perfectly nested pattern) were scored.
Matrix temperature T is the average degree of deviation from this isocline.
All nestedness analyses were performed with an improved version of Nestedness Calculator software [33] called ANINHADO v.2.03.
[34].
The significance of nestedness was assessed using two null models.
Null model I is the null model implemented in Nestedness Calculator software [33].
It assumes that each cell of the interaction matrix has the same probability of being occupied.
This probability is estimated as the number of “1s” in the matrix divided by the number of cells.
This model generates networks in which differences in the number of interactions between species are small.
Hence, deviations from this null model may be due to both differences in the number of interactions between species and an asymmetric distribution of interactions between species.
Null model II was developed by Bascompte and collaborators [4] to cope with this problem.
It assumes that the probability of each cell being occupied is the average of the probabilities of occupancy of its row and column.
Hence, deviations from this null model result solely from an asymmetric distribution of interactions between species.
For each type of null model, a population of 1000 random networks was generated using ANINHADO software [34].
The P-value is the probability of a random replicate being at least as nested as the observed matrix.
Influence of species traits on their position in the compartmentalized network:
Phylogeny of tree and fungal species:
We first performed Monte Carlo tests with 10000 replicates, using the chisq.test function of the R stats package [35], to assess whether the distribution of tree species and fungal species in the different network compartments was random with respect to their phyla and subphyla.
In the case of tree species, we also performed the multiresponse permutation procedure (MRPP) to test whether the phylogenetic distance between species was significantly lower within than between compartments, by using the mrpp function of the R vegan package [32] and by taking group size as the group weight.
Distributional range of tree species:
Then we performed the multiresponse permutation procedure (MRPP), by using the mrpp function of the R vegan package [32], to test whether the dissimilarity between tree species in their distributional ranges was significantly lower within than between compartments.
The dissimilarity between two species in their distributional range was defined as the Jaccard distance between their presence/absence vectors [36].
A distance of zero indicates that the two species have identical ranges, whereas a distance of one indicates that ranges do not overlap at all.
As previously, group size was taken as group weight in the permutation procedure.
Life-history strategies of fungal species:
Finally we investigated whether tree taxa from different compartments were linked by fungal species having a particular life-history strategy in common, by calculating the number of tree groups linked together by each fungal species and comparing this number for different strategies, using Kruskal-Wallis rank sum tests, implemented with the kruskal.test function of the R stats package [35].
Influence of species traits on their position in the nested network:
Species position in the nested network was defined as their rank in the interaction matrix when reorganized for nestedness.
We first assessed the relationships between the total number of interactions per species and their rank with Kendall's rank correlation tests, performed with the cor.test function of the R stats package [35], in order to verify that low-ranked species were those belonging to the dense core of interactions of the nested network.
Then we characterized the core tree species by their phylum, abundance and sampling intensity and the core fungal species by their phylum and life-history strategy.
Phylogeny of tree and fungal species:
We compared mean species rank as a function of phylum, using Wilcoxon rank sum tests, carried out with the wilcox.test function of the R stats package [35].
Abundance and sampling intensity of tree species:
The relationships between the abundance of a given tree taxon and rank in the nested structures were assessed with Kendall's rank correlation tests, performed with the cor.test function of the R stats package [35].
The same test was performed to assess the relationships between the sampling intensity of a given tree taxon and rank in the nested structures.
Life-history strategies of fungal species:
We performed Kruskal-Wallis rank sum tests, implemented with the kruskal.test function of the R stats package [35], to compare the rank of fungal species in the nested structures for the different life-history strategies.
