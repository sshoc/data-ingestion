All procedures of subject recruitment and evaluation were approved by the Institutional Review Boards at Rockefeller University, New York State Psychiatric Institute and University of Pretoria.
Written informed consent was obtained from all participants.
The sample used for direct sequencing consisted of 208 individuals with schizophrenia, of European ancestry, recruited from the U.S. and diagnosed by a clinical team specially trained in the use of the Diagnostic Instrument for Genetic Studies (DIGS) [72] and the research application of the Diagnostic and Statistical Manual–4th Edition (DSM-IV) [73].
On the basis of the information gathered in the DIGS, the clinical interviewers assigned appropriate diagnoses according to the DSM-IV.
The control sample used for direct sequencing consisted of 60 parents from CEPH trios (residents of Utah with ancestry from Northern and Western Europe) and an additional set of 240 healthy Caucasians collected from the U.S. by us (MK).
Sequencing analysis was performed as described in Text S2.
To evaluate association between the RTN4R gene and susceptibility to schizophrenia, we recruited participants from 312 Afrikaner families with at least one schizophrenic subject.
Afrikaners are descendants of mostly Dutch immigrants who settled in South Africa beginning in 1652 [74].
Participants were diagnosed in person by specially trained clinicians again using the DIGS, which had been translated and back-translated into Afrikaans.
Patients were classified into two diagnostic categories.
The stricter category, Sch1, includes 348 individuals who meet DSM-IV criteria for schizophrenia or depressed-type schizoaffective disorder.
Family data suggest that the two diagnoses are alternative expressions of the same genotypes [75] and in the past we have considered them together under one phenotypic liability class, LC I [76].
The broader category, Sch2, includes an additional 52 individuals diagnosed with schizoaffective disorder of mainly affective course.
Association analysis was performed as described in Text S2.
Generation of Rtn4r knockout mice:
For the construction of the targeting construct, we replaced an Rtn4r genomic fragment encompassing exon II with the self-excisable pACN cassette including the neo gene selectable marker [77].
Cell culture, embryonic stem (ES) cell electroporation and generation of the chimeric mice were performed essentially as previously described [78].
Approximately 5% of the tested ES cell clones were positive for homologous recombination, and one clone was selected for injection into C57BL/6J blastocysts.
Chimeric males were mated with C57BL/6J females, and DNA from tail biopsy samples from F1 agouti-coat pups was genotyped by Southern blotting at the Rtn4r genomic locus (wild-type fragment: 14.6-kb; recombinant fragment after pACN cassette excision: 3.8-kb).
We mated F1 heterozygous mice and obtained F2 mice of all three genotypes.
The mutation was on a hybrid C57BL/6×129Sv background.
All animal procedures were performed according to protocols approved by the appropriate Institutional Animal Care and Use Committee under the federal and state regulations.
See Text S2.
Behavioral Testing: Equipment and Procedures:
See Text S2.
Analysis following transient postnatal NMDAR blockade:
See Text S2.
