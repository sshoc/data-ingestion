21 healthy volunteers were recruited.
We obtained written consent from all 21 participants, 12 females and 9 males (age: mean = 26.05, SD = 2.35), prior to the scanning session.
All subjects had normal or corrected-to-normal vision.
No subject had a history of neurological, major medical, or psychiatric disorder.
All participants were right handed as assessed by the Edinburgh Inventory [13].
The design consisted of two independent variables, ‘context’ (free-context trials vs. determined-context trials) and ‘agency’ (self-agency vs. external-agency).
Moreover, the degrees of freedom in choice in the self-agency condition varied between 2 and 3 (DF2 and DF3).
The ‘agency cues’ could either be circles or diamonds, associated with either ‘self-agency’ or ‘external-agency’ respectively (see Fig.
1).
At the beginning of each trial, two circles or two diamonds (both conditions referring to a determined-context trial) or the circle together with the diamond (referring to a free-context trial) were presented for 2000 ms.
In the condition with two different cues, the presentation side of the cues was counterbalanced within subjects.
In both free-context and determined-context trials, participants were required to give a response with the index or middle finger of the right hand.
For the determined-context trials, the response button press had no consequence compared to the free-context trials where participants indicated with a spatially compatible mapping whether they accepted or declined agency in the secondary task.
Note that participants had to select a specific response button in all conditions so that mere response selection processes could not account for the differential activation between free-context and determined-context trials.
After a variable delay of a mean duration of 4750 ms, four cues each associated with one of four simple discrimination tasks, i.e., color, orientation, size, and line task, were presented [10].
The cues were foveally presented in a 2×2 grid with a visual angle of 2.8° (Fig.
1).
Each quadrant of the grid contained a semantic abbreviation for one of the four tasks, i.e., ‘FAR’ for ‘Farbe’ which corresponds to color, ‘SPI’ for ‘Spitze’ which corresponds to orientation, ‘GRÖ’ for ‘Groesse’ which corresponds to size, and ‘LIN’ for ‘Linie’ which corresponds to line.
The locations of the cues were balanced across participants.
There were 16 possible ‘cue-location mappings’ which were randomly assigned to the participants.
In order to instruct the participants which task could be chosen, the quadrants where either bold, indicating that this task could be chosen, or not, indicating that this task was not available for choice.
After a constant interval the target (a triangle) was presented.
This target was multivalent, i.e., the target contained one value of each discrimination task (i.e., one out of two colors, one out of two orientations, one out of two sizes and one out of two line types) resulting in 16 possible values of a target.
Corresponding to their choice of task, participants had to respond to the target with either the index or middle finger of the right hand.
In order to check for accuracy and to give valid feedback, the four semantic abbreviations of the four tasks were presented again.
This time, they were horizontally aligned while the locations of the semantic cues were pseudo-randomized, assuring that participants could not prepare for this response during the second decision time.
At this point, participants were required to indicate which task they had actually chosen and responded to with their first response by pressing the spatially compatible key with the fingers of their left hand.
Finally, a feedback for wrong, missed, or correct responses was presented.
We also included 24 catch trials to make sure that participants engaged in the selection of agency at the presentation of the agency cues.
These trials were highly comparable to the experimental trials.
The only difference pertained to the presentation of match cues instead of the target and probe stimuli.
The match cues consisted of 2 words, ‘stimmt’ (i.e., match) or ‘stimmt nicht’ (i.e., no match) which were presented next to each other in the middle of the screen.
The presentation side of the match cues was pseudo-randomized.
Participants had to indicate with their right index or middle finger with a spatially compatible mapping whether the agency option (i.e., self-agency vs. external-agency) presented at the beginning of a trial matched the degrees of freedom of tasks presented in the 2×2 grid.
There were 6 different conditions for the factors context, agency and match which were all counterbalanced within subjects.
Finally, also 10 null events were included, which were pseudo-randomly interspersed.
The null events were introduced to compensate for the overlap of the blood-oxygenation level dependent (BOLD) response between adjacent trials.
The timing of the sequence of trials was triggered from the magnetic resonance imaging (MRI) control every 18 seconds.
The trials started with a variable oversampling interval of 0, 500, 1000 or 1500 ms to obtain an interpolated temporal resolution of 500 ms. Then, a fixation cross was presented for 500 ms followed by the agency cues which were presented for 2000 ms. After the presentation of the agency cues, a variable delay of 4000–5500 ms with a variable oversampling interval of 0, 500, 1000 or 1500 ms was presented.
Subsequently, the 2×2 grid appeared for 2000 ms. Then the target was presented until a response was made with the right hand or until the response interval exceeded 2000 ms. After the first response was given, the probe stimulus was presented.
It remained on the screen until the second response was made with the left hand or until the response interval exceeded 2000 ms.
Finally, valid feedback was presented for 500 ms.
The experiment lasted for about 60 min and consisted of one block starting out with two dummy trials which were excluded from further analysis.
In total, 100 experimental trials (without catch trials and null-events) were presented, resulting in 25 trials for each the determined self-agency and determined external-agency condition and 25.75 trials for the free self-agency and 23.66 trials for the free external-agency condition.
Even though it was not possible to control for the frequencies of self-agency or external-agency in the free-context trials, we pre-selected participants who chose both options approximately equally frequent (see also section ‘pre-selection of participants’).
A comparable constraint applied to the presentation of the degrees of freedom (DF2 and DF3) in the self-agency condition.
There were 14 different combinations of DF and the four discrimination tasks (DF1 = 4 combinations; DF2 = 6 combinations; DF3 = 4 combinations).
Again, these could not be introduced in a pre-randomized task sequence due to the manipulation of self-agency.
Therefore, subsets of each combination of DF and tasks were defined.
Trials were pseudo-randomly chosen from each subset, thereby assuring that each combination was presented with approximately equal frequency.
Furthermore, it is important to note that the trial following the DF3 condition was not constrained to the one task which was not valid for choice before.
Before participants entered the scanner they underwent a training session of 30 min in which they were familiarized with the task.
On average this training session took place 2 days before the scanning session.
In the training session and also before the scanning procedure the participants were instructed to engage in every trial and consciously think about their choice on each trial.
Please note that participants were not instructed to counterbalance between conditions or were otherwise biased to perform a specific selection strategy.
Therefore, the training session was included to select only those participants who assumed and declined agency approximately equally often in the free-context trials and also showed no bias in selecting a specific response button towards the agency cues (see Supplementary Materials S1).
Finally, 21 out of 28 participants taking part in the training session were included in the fMRI study.
Magnetic resonance imaging scanning procedure:
The experiment was carried out on a 3T scanner (Medspec 30/100, Bruker, Ettlingen, Germany).
20 axial slices were acquired (19.2 cm field of view, 64×64 matrix, 4 mm thickness, 1 mm spacing) parallel to the AC-PC plane and covering the whole brain.
Slice gaps were interpolated to generate output data with a spatial resolution of 3×3×3 mm.
We used a single shot, gradient recalled echo planar imaging (EPI) sequence (repetition time 2000 ms, echo time 30 ms, 90° flip-angle).
Prior to the functional runs, correspondingly 20 anatomical MDEFT slices and 20 EPI-T1 slices were acquired.
Stimuli were displayed using a head-mounted mirror-system.
Functional magnetic resonance imaging analysis:
Analysis of functional magnetic resonance imaging (fMRI) data was performed using the inhouse LIPSIA software [14].
First, functional data were corrected for movement artifacts.
The temporal offset between the slices acquired in one scan was then corrected using a sinc interpolation algorithm.
Data were filtered using a spatial Gaussian filter with sigma = 0.8 (this refers to a FWHM = 5.65 mm).
A temporal high-pass filter with a cutoff frequency of 1/160 Hz was used for baseline correction of the signal.
All functional data sets were individually registered into three-dimensional (3D) space using the participants` individual high-resolution anatomical images.
This 3D reference data set was acquired for each participant during a previous scanning session.
The two-dimensional anatomical MDEFT slices, geometrically aligned with the functional slices, were used to compute a transformation matrix containing rotational and translational parameters that register the anatomical slices with the 3D reference T1 data set.
These transformation matrices were normalised to the standard Talairach brain size [15] by linear scaling and finally applied to the functional data.
The statistical evaluation was performed using the general linear model for serially autocorrelated observations [16].
The design matrix was generated with a synthetic hemodynamic response function [17] and its first derivative.
The onsets for the event-related analysis were set to the presentation of the agency cues at the beginning of each trial and to the 2×2 grid.
The model equation was convolved with a Gaussian kernel with a dispersion of 4 sec full width at half maximum.
Contrast maps were generated for each participant.
After the individual functional datasets were all aligned to the same stereotactic reference space, a group analysis was performed.
A one-sample t test of contrast maps across participants (random-effects model) was computed to ascertain whether observed differences between conditions were significantly different from zero.
Subsequently, t values were transformed into z-scores.
To correct for false-positive results, in a first step, an initial voxelwise z-threshold was set to Z = 2.58 (p = 0.005, uncorrected) for the agency×context interaction contrast (100 trials) and to Z = 2.33 (p = 0.01, uncorrected) for the conditional main effect of agency in the free context (50 trials).
In a second step, the results were corrected for multiple comparisons using cluster-size and cluster-value thresholds obtained by Monte-Carlo simulations using a significance level of p = 0.05.
To ensure that the reported activations, namely the MFC and RCZ, are significantly activated at p<.05 (corrected at the cluster-level), a cluster-size of 1620 mm3 (60 contiguous 3×3×3 mm voxels) for the interaction between agency×context and a cluster-size of 2439 mm3 (90 contiguous voxels) for the conditional main effect of agency in the free context was required.
In total we could ensure, that the reported activations we focus our discussion on, namely the aMFC and the RCZ, are significantly activated at p<.05, corrected for multiple comparisons at the cluster-level.
To compute the percent signal change of the hemodynamic response in the aMFC, all significantly activated voxels exceeding the critical threshold in the group-averaged whole-brain interaction between context×agency and belonging to a contiguous cluster in the aMFC were included.
Analogously, we also computed the percent signal change of the hemodynamic response in the RCZ including all significantly activated voxels that exceeded the critical threshold in the group-averaged main effect of agency and belonged to a contiguous cluster in the RCZ.
We then extracted the time course of the signal underlying these activated voxels for each participant from the preprocessed data.
The percent signal change was calculated in relation to the mean signal intensity across all timesteps for these voxels.
The signal change was averaged for each condition for 16 seconds beginning with the presentation of the agency cues and the presentation of the 2×2 grid, respectively.
We then subtracted the time course of the null event from the time course of the relevant conditions to compensate for the overlap of the BOLD response [18].
The logic is that null events are (at least on average) embedded within the same past and future trial conditions as a regular event, and thus have the same preceding and succeeding average BOLD signal.
By subtracting the null event from the relevant condition, one assumes that the brain area, i.e., activated voxels, exhibit no activation for the null event so that the remaining BOLD signal is solely due to the experimental manipulation.
We searched for the largest value of the signal in the time window between 4 to 8 seconds after the presentation of the agency cues and the 2×2 grid, respectively.
Finally, these values were averaged across participants.
To test whether the aMFC and the RCZ reveal distinct functional roles in decision-making processes, additional region of interest (ROI) analyses with unbiased ROIs were conducted.
In a first step, neutral contrasts, i.e., all conditions vs. resting baseline, were computed separately for both onsets.
Based on these contrasts, in a second step, ROIs were generated for the aMFC [3] and the RCZ [2], [7].
The ROI for the aMFC was generated from the neutral contrast, i.e., all conditions vs. resting baseline, for the first onset, and defined as a sphere with 3 mm radius centered on the peak voxel of the activation (see Supplementary Materials S1).
The same procedure was applied to generate the ROI in the RCZ.
Here the neutral contrast, i.e., all conditions vs. resting baseline, for the second onset was taken, and the ROI was centered at the peak voxel of the activation, with a sphere radius of 3 mm (see Supplementary Materials S1).
In a third step, the percent signal change from each ROI, i.e., aMFC and RCZ, was extracted separately for each subject, for each condition, and for each onset.
Finally, these values were subjected to repeated measures ANOVAs to statistically test for three-way interactions between agency, context, and ROI, separately for the first and second onset.
