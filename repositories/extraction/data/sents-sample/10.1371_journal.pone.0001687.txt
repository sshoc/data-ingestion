The study species, the bank vole (Myodes glareolus), is a small rodent species common in northern Europe [43].
The main habitats are forests and fields, and the diet consists of forbs, shoots, seeds, berries and fungi [44].
The density of breeding females is limited through their territoriality [29], and in high densities the maturation of young females is suppressed by social interaction among females [45].
The patterns and amplitude of density variation show considerable geographical variability, and both stable and cyclic populations are found [46].
In our study area, females give birth to a maximum of four litters during the breeding season, which lasts from late April to September.
In addition to large phenotypic [20] and genetic variation in litter size (2–10) and offspring size (1.3–2.5 g), a negative genetic correlation also exists between these traits [19].
Pregnancy lasts for 19–20 days and pups are weaned until the age of three weeks [47].
Bank voles have good trappability, and they are not sensitive to disturbance, which allows monitoring populations by live-trapping.
Experimental animals originated from artificial selection lines at the University of Jyväskylä.
Founder animals for the selection lines were caught in central Finland (62°37′N, 26°20′E).
In the truncation selection experiment, two lines were selected according to litter size of females (see details Schroderus et al.
2007, submitted manuscript).
The selection experiment was based on the between family selection procedure [48], which prevents the possibility of inbreeding.
Reproductive effort of females is determined here by the formula of [49]: RE = (litter size * mean offspring mass 0.75)/mothers post-partum mass 0.75).
In this formula energy requirements to produce offspring are calculated relative to the allometric requirement of the mother (assuming standard metabolism increases to the 0.75 power of mass for mammals [50]).
The study started by mating a fraction of the females (n = 91) obtained from the selection experiment with randomly chosen males of the same line of female (low RE: mean±SE = 0.67±0.02, n = 49; High RE: mean±SE = 0.84±0.02, n = 42).
We further increased the difference between the two groups by choosing only the females with lowest RE (n = 39) and highest RE (n = 33) for the present experiment.
All females were at the same age and in similar reproductive state.
Characteristic of these mothers and their offspring and statistical tests are presented in the Table 3.
These 72 females gave birth in the laboratory in early July, and after that the females with their new-born individually marked pups were released to 13 large outdoor enclosures (each 0.2 ha) [47].
None of the mothers abandoned their pups during the releasing process.
Four males were introduced to each enclosure to keep females in reproductive condition.
These males might mate with the females, but we were not able to measure the success of the possible subsequent breeding events.
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001687.t003
Characteristics (mean±SE) of two female tactics (low or high reproductive effort, RE).
Random assignment to the different manipulation groups (frequency and density) is tested by three-way ANOVA.
All possible two and three-way interactions were non-significant (P>0.11).
***P< = 0.001,**P<0.01,*P<0.05
In the enclosures, both the densities (4 or 8 females/0.2 ha) and frequencies (1∶3 (4 enclosures); 3∶1 (4), 2∶6 (2), 6∶2 (3)) of different reproductive tactics (low or high RE) were manipulated (see the design in Table 4).
The choice of 4 and 8 females per enclosure to low and high experimental densities respectively, was based on our earlier studies showing that 4–6 females can gain a territory and breed simultaneously in the same enclosure [51].
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001687.t004
The design of the experiment.
Number of females per enclosure, number of enclosures (in parenthesis) and total number of individuals in each treatment.
The breeding success of all 72 females was studied during one breeding in July.
Breeding success was determined by monitoring the number and the proportion of offspring surviving to the weaning age (about 25 days old).
For monitoring individual voles, 20 multi-capture live traps were distributed in each enclosure in a 4×5 array with 10 m between the trap stations.
Mothers and offspring were trapped at weaning and at the end of the experiment (offspring about three months old) (see details of trapping procedure [24], [52]).
Survival of only 64 females was determined from the beginning of the experiment (early July) to the end of the experiment (early October), as all the individuals in two enclosures escaped before the end of the experiment.
The females escaped after the first litter was weaned, so we were able to include all females to the analyses of the breeding success (Table 1 and Fig.
1A,B).
Only the successful enclosures (see number of replicates in Fig.
3) were included into the analyses of frequency changes in the population level (see below).
No other indications of unsuccessful replicates were found during the experiment.
Selection for reproductive effort was studied using the analyses of selection gradients [48], [53].
Selection gradients (β) were estimated from the linear regression coefficients of relative fitness on the standardized trait.
Here the relative fitness (number of offspring weaned/mean number of offspring weaned in the population) was estimated in relation to standardized reproductive effort ((REi–REmean)/RESD).
The survival of mothers (Table 2) and their offspring was monitored until the end of the breeding season to estimate the changes in frequencies of RE tactics at the population level (Fig.
3).
Change in frequency was determined by the frequency of the certain breeding tactic (survived mothers and their offspring) at the end of breeding season minus the initial frequency of the breeding tactic in the enclosure.
The data were analysed using SPSS 14.0 for Windows and SAS version 9.1 software.
In the generalized linear mixed model analysis (GLMM), the number of offspring weaned, proportion of offspring surviving or mother survival were explained by RE tactic, frequency, density and their interactions.
The analyses of GLMM were performed both at the individual level (individual values formed the dependent variables) and population level (population means formed the dependent variables).
At the individual level, GLMM allows for the population (enclosure) to be included as a random effect in the models [54].
