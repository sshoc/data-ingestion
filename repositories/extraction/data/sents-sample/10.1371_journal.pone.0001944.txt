This research was performed under approval of the ethics committee of Chiba University Graduate School of Medicine and National Institute of Radiological Science.
The experiments were thoroughly explained to the subjects, and written informed consent was obtained from all.
Twenty schizophrenic patients and 16 age- and gender- matched normal controls with no past history of psychotic disorders or drug dependence were enrolled in the study.
Characteristics of subjects are shown in Table 1.
Due to a few highly educated comparisons, the extent of education and estimated IQ were significantly different between the two groups, but the estimated IQ of all patients was within the normal range.
All patients were outpatients meeting the DSM-IV criteria for schizophrenia [24] and having no other psychiatric disorders.
All patients were taking second-generation neuroleptics: i.e., risperidone (2–12 mg/day, n = 9), olanzapine (5–20 mg/day, n = 5), aripirprazole (6–12 mg/day, n = 4), quetiapine (500 mg/day, n = 1) or perospirone (48 mg/day, n = 1), with no change in their medication for the past month.
Of the patients, twelve were diagnosed as residual type and eight were as paranoid type.
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001944.t001
Characteristics and clinical variables of subjects enrolled in this study
All values are shown as mean±SD (range).a)Chi-squire test,b)Student t-test.
*1: Short form version of Wechsler Adult Intelligence Scale, Revised (WAIS-R)*2: Chlorpromazine equivalent (mg)GAF: Global Assessment of Functioning, BPRS: Brief Psychiatric Rating Scale, SANS: Scale for the Assessment of Negative Symptoms, DIEPSS: Drug Induced Extra-Pyramidal Symptoms Scale
1H-MRS measurement and data analysis:
All data were acquired using the 3T SIGNA EXCITE (GE) with a standard quadrature coil.
GSH spectra were acquired by the MEGA-PRESS sequence [25].
A GSH peak at chemical shift 2.95 ppm originating from cysteinyl β-CH2 was observed by editing pulse at 4.95 ppm α-CH resonance line J-coupled to the observed spins.
Acquisition parameters for the measurement were as follows: echo time (TE) = 94 ms, repetition time (TR) = 1500 ms, number of excitations (NEX) = 512, band width 2.5 kHz, data point 4096.
TE and TR were set experimentally as optimum for our system after confirming the GSH signal changes to be within a certain range (TE: 62–101 ms, TR: 1077 ms–12000 ms) with both phantom solutions and human subjects.
The short TR enabled us to increase NEX and obtain a satisfying signal/noise (S/N) ratio in the human brains.
For the quantification of GSH, we prepared eight phantom solutions containing different concentration of GSH (0.3–30.0 mM) with N-acetyl aspartate (NAA, 10 mM) and creatine (8 mM) to get the reference spectra.
During the phantom data acquisition, the solutions were kept at 37±0.6°C.
For the acquisition of human spectra, an 18.6-ml (28×30×22 mm) volume of interest (VOI) was placed on the posterior medial frontal cortex under the guidance of T2-weighed images (Figure 1A).
The posterior medial frontal cortex was selected since reduction in the GSH levels in this region of schizophrenic patients has been reported previously [10].
To minimize variation in the positioning of the head, subjects were positioned by the same investigator.
The overall examination time was 1 hour or less.
Figure data removed from full text.
Proton MRS of GSH.
(A): T2-weighed magnetic resonance imaging of the targeted region.
The blue boxes show the voxel size (28 x 22 x 30 mm) in the posterior medial frontal cortex of a human brain.
(B): representative data of reference phantom spectra of GSH (0.5, 1.0, 3.0 mM).
Note that the GSH signal increases according to the phantom concentration.
(C): Quantification of GSH.
Plots showing a linear correlation (r2 = 0.994) between the GSH signal area at 2.95 ppm and the concentration of GSH.
(D): Representative data of GSH signals of the posterior medial frontal cortex of a human subject.
The GSH level was calculated as 0.735 mM by applying the linear concentration curve on (C).
doi:10.1371/journal.pone.0001944.g001
For all data acquisition, high-order shim followed by automatic local shim adjustment was used and repeated until the half linewidth was accomplished under 3 Hz (phantom) or 8 Hz (human).
The raw data of both phantom solutions and human subjects were processed on GE analysis software (GE Medical Systems, Milwaukee, WI).
Fourier transform was done with an exponential weighting function of 2 Hz.
The area of the GSH signal was measured on Image J (http://rsb.info.nih.gov/ij/) software.
The Scale for the Assessment of Negative Symptoms (SANS) and Brief Psychiatry Rating Scale (BPRS) were used to evaluate the severity of negative symptoms and psychotic symptoms (positive and negative symptoms), respectively.
The Drug-Induced Extrapyramidal Symptoms Scale (DIEPSS) was used to evaluate and exclude the effects of drug-induced extrapyramidal symptoms which could affect the severity of symptoms in schizophrenic patients.
Functional disability was assessed using the Global Assessment of Functioning (GAF) scale.
Several cognitive function tests were used.
In the Word Fluency Task (letter, category), subjects were given an initial letter (letter fluency task) or a certain category (category fluency task) as a cue [26].
Both tasks consisted of three trials, and the number of words produced in one minute for each trial was recorded for evaluation.
In the Stroop Test, a list of twenty four colored dots (D), a baseline test, and 24 colored words incongruent with the color (C) were used.
The difference between the reaction time (C-D) was assessed [27].
In the Wisconsin Card Sorting Test (WCST), subjects were instructed to sort cards according to a rule (color, shape, or number).
The numbers of achieved categories and perseverative errors were assessed [28].
In the Trail-Making Test part (TMT) A, subjects drew lines as quickly as possible to connect 25 consecutively numbered circles.
In the TMT part B, subjects connected 25 consecutively numbered and lettered circles by alternating between the two sequences.
The time taken to complete each part of the test was recorded in seconds [29].
In the Digit Span Distractibility Test (DSDT), subjects were asked to remember a tape-recorded string of digits read by a female voice while ignoring the digits read by a male voice (distracter) [30].The percentages of digits correctly recalled in conditions with and without distracters were assessed separately.
Genetic analysis for the genes involved in GSH metabolism-GCLM, glutathione peroxidase 1 (GPX1), and several classes of glutathione-S-transferase (GSTM1, GSTO1, GSTP1, GSTT1 and GSTT2)-as performed by the methods described previously [15], [31]–[33].
All calculations were performed with SPSS software (SPSS version 12.0J, Tokyo, Japan).
Student's t-test (unpaired) was employed for the comparison of GSH levels between schizophrenic patients and normal control subjects and of the scores of the cognitive function tests between the two groups.
For the genotyping results, the differences between patients and controls were evaluated by Fisher's exact test.
Pearson's correlation coefficients were examined to identify any correlations of GSH levels with the clinical severity (BPRS, SANS, and DIEPSS) of schizophrenic patients and with the scores of cognitive function tests of all subjects.
A value of p<0.05 was used as the standard for statistical significance in all analyses.
