All individuals of this study participated in the German MI Family Study (total n  =  7,575).
Recruitment process, selection criteria and study details have been reported previously [21], [22].
In brief, we identified families from all parts of Germany with accumulation of premature MI or severe CAD.
Control individuals were unaffected spouses of MI family members and had no genetic relationship to cases.
The Ethics committee of the University of Regensburg approved the study protocol and all participants gave their written informed consent at the time of inclusion and again at the time of follow-up investigations.
The study was in accordance with the principles of the current version of the Declaration of Helsinki.
The baseline home visit was performed by a physician and included a standardized questionnaire, physical examination, and biochemical analyses.
A standardized follow-up interview regarding new medical events was carried out by specially trained telephone interviewers after two and five years.
Cardiovascular events at study entry and follow-up were validated by reviewing medical records.
The phenotype gout was carefully established using two levels of evidence: 1. medical history readings at time of inclusion, two-year and five-year follow-up interview; and 2. self-reported history of gout.
The diagnosis of MI was established according to the MONICA (Monitoring Trends and Determinants in Cardiovascular Disease) diagnostic criteria (http://www.ktl.fi/publications/monica/manual/index.htm).
Severe CAD was defined as treatment with percutaneous coronary intervention or coronary artery bypass graft.
Resting blood pressure was taken according to MONICA guidelines after participants had been resting in a sitting position [23].
Hypertension was defined as blood pressure ≥140/90 mm Hg or ongoing antihypertensive therapy.
Body weight was determined with subjects wearing light clothing.
Body mass index (BMI) was calculated as weight in kilograms divided by the square of height in meters.
Sampling of serum was carried out from non-fasting individuals.
Serum levels of low-density lipoprotein cholesterol (LDL-C) and high-density lipoprotein cholesterol (HDL-C) were measured by standard enzymatic methods.
Hypercholesterolemia was defined as LDL-C ≥160 mg/dL or intake of lipid lowering medication.
History of diabetes mellitus or intake of antidiabetic medication was used to define type 2 diabetes.
Smoking was defined as current or former smoking habit.
A total of n  =  665 individuals (n  =  479 males, n  =  186 females) with the diagnosis of gout were selected from the German MI Family Study.
All gout cases fulfilled the above mentioned criteria for “gout”.
Controls were unrelated individuals from our German MI Family Study who did neither have any indication for gout nor were they medicated with uricostatics or uricosuric agents at any time during follow-up.
We carefully matched the gout-free controls (n  =  665) according to age and gender to the gout cases.
Further phenotypic details are shown in Table 1.
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001948.t001
Characteristics of gout case and control study sample.
Values denote means ± standard deviations unless indicated otherwise.
n. s., not significant; CAD, coronary artery disease; MI, myocardial infarction; LDL-C, low-density lipoprotein cholesterol; HDL-C, high-density lipoprotein cholesterol; BMI, body mass index.aMatching parameter.bDefined as LDL-C ≥160 mg/dL or intake of lipid lowering medication.cDefined as blood pressure ≥140/90 mm Hg or ongoing antihypertensive therapy.dDefined as history of diabetes mellitus or intake of antidiabetic medication.eFormer or current smoking habit.
Futhermore, a large case-control sample was established from the German MI Family Study including 1,473 CAD/MI cases (856 male, 617 female) and 1,241 unrelated CAD/MI-free control individuals (336 male, 905 female).
Cardiovascular risk factors and phenotypic details are summarized in Table 2.
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001948.t002
Characteristics of CAD case and control study sample.
Values denote means ± standard deviations unless indicated otherwise.
CAD, coronary artery disease; MI, myocardial infarction; LDL-C, low-density lipoprotein cholesterol; HDL-C, high-density lipoprotein cholesterol; BMI, body mass index.aDefined as LDL-C ≥160 mg/dL or intake of lipid lowering medication.bDefined as blood pressure ≥140/90 mm Hg or ongoing antihypertensive therapy.cDefined as history of diabetes mellitus or intake of antidiabetic medication.dFormer or current smoking habit.
SNP selection and genetic analyses:
Four GLUT9 SNPs were selected from a public reference database (dbSNP, http://www.ncbi.nlm.nih.gov/SNP/) based on the following criteria: (1) evidence for significant association with serum UA levels from two recent publications (rs6855911 and rs7442295) [18], [19], (2) minor allele frequency of >0.10 in a Caucasian population, (3) evidence of validation status, and (4) compatibility with our genotyping platform.
To allow determination of the extend of LD beyond the boundaries of the gene, one SNP in the 5′ intergenic region was included.
Genomic DNA was isolated from whole blood samples using the PureGene DNA Blood Kit (Gentra, Minneapolis, MN, USA).
DNA samples were genotyped using 5′ exonuclease TaqMan® technology (Applied Biosystems, Foster City, CA, USA).
For each genotyping experiment 10 ng DNA was used in a total volume of 5 µl containing 1× TaqMan® Genotyping Master Mix (Applied Biosystems).
PCR reaction and post-PCR endpoint plate read was carried out according to the manufacturer's instructions using the Applied Biosystems 7900HT Real-Time PCR System.
Sequence Detection System software version 2.3 (Applied Biosystems) was used to assign genotypes applying the allelic discrimination test.
Case and control DNA was genotyped together on the same plates with duplicates of samples (15%) to assess intraplate and interplate genotype quality.
No genotyping discrepancies were detected.
Assignment of genotypes was performed by a person without knowledge of the proband's affection status.
Additionally, comparison between genotype calls from TaqMan assays and Affymetrix GeneChip® Human Mapping 500K Array Set were carried out in 288 DNA samples on all four SNPs (rs6855911, rs7442295, rs6449213, and rs12510549).
Again, no discrepancies were found.
SNPs were mapped to the GLUT9 gene according to NM_001001290 (position on human genome build 18: chromosome 4; 9,436,948-9,650,970).
Genome-wide data from two recently published screens for polymorphisms associated with CAD or MI were employed to survey the complete GLUT9 gene region [12].
All successfully genotyped SNPs on the Affymetrix GeneChip® Human Mapping 500K Array Set on chromosome 4 in the GLUT9 region with 500 kb on either side of the gene (position 8,936,948 and 10,150,970; human genome build 18) were included (Fig.
1).
Figure data removed from full text.
Figure identifier and caption: 10.1371/journal.pone.0001948.g001
Schematic representation of a ∼1,214 kb region on human chromosome 4p16.1 (position 8,936,948 and 10,150,970; human genome build 18) used for association analyses.Position numbers are given on top.
(A) Relative positions of SNPs analyzed in this study.
GWA SNPs from WTCCC and Cardiogenics CAD/MI case-control study are shown above the four SNPs genotyped in this work (gt SNPs).
From left to right: rs6855911, rs7442295, rs6449213, and rs12510549.
(B) Graphic illustration of genes in this genomic region.
Relative positions of RefSeq genes with gene names are presented.
Exons are depicted as vertical blocks and orientation of genes is indicated by arrows on the horizonal lines representing introns.
GLUT9 gene is highlighted with bold letters.
(C) Delineation of LD structure.
Pairwise r2-values between markers from HapMap phase II release 22 for the CEPH (CEU) sample were calculated and presented.
Red denotes perfect LD with r2 = 1 and decreasing r2-values are shown in brighter color with white representing r2 = 0.
Data of the HapMap phase II release 22 data were used to assess linkage disequilibrium (LD) patterns of the GLUT9 gene region [24].
To determine whether the genotypes of cases and controls of all GLUT9 SNPs deviated from Hardy-Weinberg equilibrium, actual and predicted genotype counts of both groups were compared by χ2-test.
Differences in allele frequencies between dichotomous traits were calculated employing the same method.
Genotypes were coded for both dominant and recessive effects (genotype 22+12 versus 11 and genotype 22 versus 11+12, respectively, with the minor allele coded as 2).
The additive genetic model was calculated using Armitage's trend test.
Multiple logistic regression analysis was used to examine the association of GLUT9 SNPs with either phenotype (gout or CAD/MI) allowing adjustment for relevant covariates (Tables 1, 2).
Differences in continuous variables between groups were calculated using a two-tailed unpaired t-test.
Prevalence odds ratios (OR) with their 95% confidence intervals (CI) were reported.
A two-sided p-value ≤0.05 was considered statistically significant.
Association analyses were performed using JMP IN 7.0.1 (SAS Institute Inc, Cary, NC, USA) and PLINK v1.00 [25].
For haplotype analysis and permutation testing PLINK v1.00 [25] and HaploView v4.0 [26] were employed.
Power analysis was carried out using G*Power 3.0.8 [27].
