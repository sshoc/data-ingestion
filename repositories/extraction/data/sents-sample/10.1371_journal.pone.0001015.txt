Pregnant outbred CD-1 female mice (purchased from Harlan, 20050 Correzzana, MI, Italy) were housed in standard polycarbonate cages (33.0×13.0×14.0 cm) with sawdust bedding and ad libitum water and rodent pellets (Enriched standard diet purchased from Mucedola, Settimo Milanese, Italy).
They were maintained on a reversed 12:12 h light:dark cycle (lights on at 1900 h) with temperature at 21±1°C and relative humidity of 60±10%.
Dams were inspected daily at 0930 h for delivery and day of birth was designated as postnatal day 1 (PND 1).
Between delivery and weaning all subjects were kept under standard facility rearing conditions (cage cleaning once a week).
Litters were not culled until weaning and dams that delivered less than six pups or litters in which male to female ratio was heavily skewed in one or the other direction (more than 75% of same sex pups) were excluded from the experiment.
Due to a failure to meet these requirements 6 dams (two dams per group) were excluded from the experiment (n = 8 dams per group).
Only male mice were used in the study.
To avoid litter effects, no more than one mouse per dam was used for each experimental procedure, resulting in 6-8 mice per group per test (see table 1 for details about littermates allocation to experimental testing).
At PND 21, offspring were weaned and groups of three male subjects were formed.
Post-weaning cage composition was counterbalanced across groups: thus, each cage consisted of one male mouse per neonatal treatment group (i.e., one AFR, one L-CORT and one H-CORT subject).
Mice were marked at weaning using permanent marker.
Table data removed from full text.
Table identifier and caption: 10.1371/journal.pone.0001015.t001
Allocation of littermates to experimental tests
Each dam provided four male mice to the study.
Littermates were tested according to the schedule reported in the table.
E.g.
mouse 2 performed the novelty-seeking test between PND 37-41 and was then assessed for immune response between PND 90-104.
This schedule was adopted in order to maximize the information obtained by each mouse and to minimize the potential carry-over effects due to previous testing.
In one case we could not use naïve mice; therefore we first exposed mice to the least stressful procedure (novelty seeking) and then waited a long time before the second test (immune resistance).
All animal handling and experimental procedures were performed according to European Communities guidelines (EC Council Directive 86/609), Italian legislation on animal experimentation (Decreto L.vo 116/92) and NIH guide for the care and use of laboratory animals.
The day of birth was counted as PND 1, and, starting on PND 2, 10 mothers were maintained on tap water (AFR control), 10 mothers had free access to a solution of 33 µg/ml of corticosterone 21-sulfate (Sigma, St Louis, Mo., USA, L-CORT) and 10 mothers had free access to a solution of 100 µg/ml of CORT (H-CORT).
Other than solution in the drinking bottle (water or CORT) environmental conditions were identical among groups.
The treatment lasted until PND 8.
The treatment period was selected based on the following reasons.
Classical neonatal manipulations (e.g., brief and long maternal separations) have been shown to modify offspring phenotype when applied during the first week of life, and longer periods are as effective [11].
Mouse HPA reactivity during the first week of life is constantly down regulated [29].
The doses were based on previous literature in rats [18] and mice [24].
Compared to rats, mouse dams show a four-fold higher water intake (see [18] and [30] for a comparison).
We therefore selected the low dose in order to have comparable levels of corticosterone intake per g of body weight in dams between our study and the previous studies performed by Catalani and colleagues [20].
The doses selected resulted in a daily average corticosterone intake of 2,7±0,1 mg/mouse and of 0,9±0,03 mg/mouse in the H-CORT and in the L-CORT group, respectively.
This administration route in mice results in a robust increase in corticosterone levels both in the dams and in the pups [24].
Importantly, corticosterone raise can be observed both in the serum and in the milk of the lactating female.
As shown by a comparison between the studies performed by Catalani and colleagues [18] and Leonhardt and colleagues [19], this procedure produces plasma levels of the hormone in the range of those produced by a mild psychic stress (from 4.3+/−0.5 to 9.5+/−1.8 micrograms/100 ml in the dams, and from 0.7+/−0.1 to 1.2+/−0.2 micrograms/100 ml in the pups at 10 days of lactation).
The behavior of the dams was scored between PND 2-8 according to a detailed ethogram [25].
Maternal care was observed daily for three 1-h sessions, distributed during the dark phase (starting at 900, 1200 and 1500 hours respectively), by instantaneous sampling at an interval of 6 min (30 samples per day for each dam).
In the present paper, the following behaviors are reported:
high kyphosis, low kyphosis, partial kyphosis, and licking (but not prone nursing and supine nursing; cf.
[23]).
The dam lies flat on top of the pups with little or no limb support.
time spent by the dams in contact with pups.
the dam is not in any form of physical contact with her pups.
This behavioral category comprised feeding and drinking, active behavior outside the nest, resting and self-grooming (licking, scratching and washing of the head and body).
General locomotion out of the nest:
dams wondering about the cage and not in any form of physical contact with pups.
To evaluate mice response to a novel environment in a test that has been validated in our lab and for which we developed a standardized protocol in mice [31], both adolescent (PND 37-41) and adult (PND 77-81) subjects were first familiarized for three days with a compartment of an apparatus and then allowed to freely explore both the familiar and the unfamiliar compartment (see SI).
Novelty preference is considered an inverse index of anxiety whereby an anxious mouse tends to avoid a novel and unknown environment [32], [33].
Novelty seeking was evaluated in both adolescent (pnd 37-41) and adult (pnd 77-81) subjects.
Adolescent subjects have been proposed to be characterized by higher variation compared to adults [34], [35].
Furthermore, one of the authors (G.L.)
has a long-held experience in adolescent mouse behavior and has previously observed [31] spontaneously elevated levels of novelty in adolescent mice compared to adults.
Therefore, analyzing novelty in adolescent and adult subjects would serve two objectives: (i) test whether adolescent and adult subjects do indeed show differential variation and how this difference can be modified by neonatal corticosterone treatment; (ii) evaluate whether novelty preference test is valid and resistant to the challenge of repeated testing approximately 10 years after the original observation.
The experimental apparatus consisted of an opaque Plexiglas rectangular box with smooth walls, which was subdivided into two compartments (20×14×27 cm).
The door between the two compartments could be closed by means of a temporary partition.
One distinctive visual cue was associated with each compartment.
One compartment had white walls and black floor, whereas the other one had black walls and white floor.
Each compartment was provided with four pairs of infrared photobeams, placed on the wall at few cm from the floor, 5.5 cm apart.
Each beam interruption eventually caused by mice was recorded by an IBM computer equipped with dedicated software.
The following measures were obtained automatically: 1) time spent in each compartment, 2) activity rate in each compartment (number of beam interruptions/second), 3) frequency of passages between the two compartments (number of passages/minute), and 4) latency (time between the opening of the partition and the first entrance in the novel compartment).
The whole session was automatically subdivided into 5-min intervals.
One compartment of the apparatus (black floor) was the familiar one, whereas the other compartment was the novel one [31].
The preference for the novel compartment has been shown to be independent of the environmental cues provided [36].
Day 1,2,3: Familiarization.
Animals were placed for 20 min in the familiar compartment of the apparatus for a 20-min session.
Day 4 (wash-out).
A wash-out interval of 48 h was introduced between the last day of familiarization and the test.
Day 5: Novelty preference test.
Animals were weighed and immediately placed in the familiar compartment, for a 5-min session.
The partition separating the two compartments of the apparatus was then removed, and mice were thus allowed to freely explore the whole apparatus (both the familiar and the novel sides) for 20 min.
The apparatus consisted of a metal plate 25×25 cm (Socrel Mod.
DS-37, Ugo Basile, Italy) heated to a constant temperature of 48±0.1°C, around which a plastic cylinder 20 cm in diameter, 18 cm high was placed.
The temperature was chosen so as to minimize suffering and distress in the experimental subjects.
The latency (s) was recorded from the moment the animal was inserted onto the plate up to when it first licked one of its paws.
The measurement was terminated if the latency exceeded the cut-off time of 60s (e.g., [37]).
Hind-paw liking has often been adopted as the main end-point.
However, several other authors reported recorded ‘the latency to the first sign of discomfort’ [38].
Additionally, the specificity and sensitivity of the test has been reported to be increased by measuring the reaction time of the first evoked behavior regardless of whether it is paw-licking or jumping [39].
Furthermore, the key issue addressed in our study was the reproducibility of experimental findings.
We therefore decided to adopt testing strategies in which the experimenter performing the test had a substantial degree of confidence thereby minimizing the possibility to introduce experimenter-dependent variability.
We therefore chose a procedure that we have already adopted several times in the past (e.g., [37], [40]).
Finally, this protocol also minimizes the time that mice spent on the hot plate and the suffering associated with it.
Plasma corticosterone response to restraint stress:
At the age of about 16 weeks, from each litter one male was taken from the home cage, carried by a familiar experimenter to an adjacent room and bled (t0) from the tail (0.2–0.4 ml collected into prechilled ethylenediamine tetraacetic acid (EDTA)-coated tubes; Microvette; Sarstedt, Sevelen, Switzerland) by tail incision [41] within 2 min from entering the colony room to obtain a blood sample for analysis of basal plasma corticosterone.
Subsequently, mice were placed in a transparent Plexiglas restraint tube (2.8 cm in diameter) of adjustable length for 25 min to induce a stress response.
After 25 min, they were bled from the same tail incision for a second time (t25) to obtain blood samples for analysis of the peak stress response before release from restraint and transport back to the home cage.
At times t60 and t120, mice were again transported to the adjacent room, bled from additional tail incision for a third and fourth time to obtain blood samples for analysis of recovery from the stressor, and returned to the colony room.
Blood was sampled between 1030 and 1300 hours.
Samples were cool centrifuged, and the plasma stored at −80°C until assayed.
CORT was measured by a commercial radioimmunoassay (RIA) kit (ICN Biomedicals, Costa Mesa, CA).
Sensitivity of the assay was 0.125 mg/dl, inter- and intra-assay variation was less than 10 and 5%, respectively.
In order to assess the effect of treatment upon the capability of mice to respond to an infectious disease we used an experimental model of infection due to Brucella melitensis [42].
The capability of mice from the three groups to restrict the spleen infection after a standardized virulent challenge was evaluated.
Six mice per group were challenged i.p.
with B melitensis 16 M, at 2×105 CFU each, at 40 dpv.
Two weeks later, mice were killed by cervical dislocation and spleens were removed for bacteriological examination as described below.
A mean value for each spleen count was obtained after logarithmic conversion.
To detect Brucella organisms, spleens were aseptically removed from sacrificed mice, individually weighed, and diluted 1/10 (wt/wt) in sterile phosphate-buffered saline.
Further dilutions were made, and 0.1 ml of each dilution was plated in triplicate onto BAS medium and incubated at 37°C for 5 days.
The Brucella isolates were identified by Gram staining, colony morphology, and Brucella-specific PCR procedures.
Within-group variability was analyzed through Levene test for homogeneity of variances.
The general model for Levene test depended on the parameter of interest.
For analysis of novelty seeking, the model was 2 age×3 treatments×4 time bins.
For analysis of plasma levels of corticosterone, it included 3 treatments×4 time points.
Finally, for analysis of hot plate and immune response, treatment (3 levels) was the only factor analyzed.
Age and treatment were between-litter factors while time bins and time points were within-litter factors.
To evaluate the frequency distribution, data from all tests were standardized, pooled and finally analyzed for normality distribution through Shapiro-Wilks test.
Most of our data failed to meet the homoschedasticity assumption.
Therefore, data analysis on absolute values was based on non-parametric methods, which do not allow the analysis of complex split-plot designs.
Repeated measures were pooled using a unitary parameter, the average of the four time-bins for the response to novelty test and the area under the curve for corticosterone response.
We then performed Kruskal-Wallis analysis of variance (ANOVA) followed by Mann-Whitney U test for paired comparisons.
For response to novelty, the Kruskal-Wallis ANOVA was performed on the six treatment×age groups, followed by orthogonal chi-square partitioning to test main effects of treatment and age, and their interaction.
Immune parameters were log-transformed and analyzed through parametric ANOVA.
Maternal care was analyzed by repeated-measures ANOVA for split-plot designs.
The general model was 3 treatments×7 days×3 hours.
Treatment was a between-litter factor while all other variables were within-litter factors.
