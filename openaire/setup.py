import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sshoc_python_client_sbyim", # Replace with your own username
    version="0.0.7",
    author="Seung-bin Yim",
    author_email="seung-bin.yim@oeaw.ac.at",
    description="Python Client for SSHOC Marketplace API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.gwdg.de/sshoc/data-ingestion",
    project_urls={
        "Bug Tracker": "https://gitlab.gwdg.de/sshoc/data-ingestion",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
)