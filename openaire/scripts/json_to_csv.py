import csv
import json
import sys

json_file = open('../result/counts.json')
tool_count = json.load(json_file)['tools']

fields = ['mp_id', 'label', 'software_count', 'publication_count', 'research_data_count', 'project_count',
          'other_count']

with open('../result/openaire_count_2.csv', 'w+') as fp:
    dict_w = csv.DictWriter(fp, fields)
    dict_w.writeheader()
    for key, val in tool_count.items():
        row = {'mp_id': key,
               'label': val['label'],
               'software_count': val['software']['count'],
               'publication_count': val['publication']['count'],
               'research_data_count': val['research_data']['count'],
               'project_count': val['project']['count'],
               'other_count': val['other']['count']}
        dict_w.writerow(row)
