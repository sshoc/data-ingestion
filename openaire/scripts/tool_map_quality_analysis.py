import pickle
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import json


def to_dataframe(artifact_type):
    data = []

    for tool in artifact_type.items():
        matches = tool[1]['result']
        sshoc_id = tool[0]
        for match in matches:
            tool_data = [sshoc_id,
                         match['_id'],
                         match['_main_title'],
                         match['_alternative_title'],
                         match['_creator'],
                         match['_contributor'],
                         match['_date_of_acceptance'],
                         match['_description'],
                         match['_format'],
                         match['_publisher'],
                         match['_repository_url'],
                         '|'.join(match['_rels']),
                         ','.join(match['_rels_types']),
                         ','.join(match['_languages']),
                         ','.join(match['_keywords'])]
            data.append(tool_data)

    return pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                       'date_of_acceptance', 'description', 'format', 'publisher',
                                       'repository_url', 'rels', 'rels_types', 'languages', 'keywords'])


def rels_type_counter(df, type):
    df_keywords = df[df['rels_types'] != '']

    cv = CountVectorizer(tokenizer=lambda x: x.split(','))
    results = cv.fit_transform(df_keywords['rels_types'])

    features = cv.get_feature_names()
    print('# of distinct rels type: ' + str(len(cv.vocabulary_)))

    freqs = zip(cv.get_feature_names(), results.sum(axis=0).tolist()[0])
    vocabulary_sorted = list(sorted(freqs, key=lambda x: -x[1]))
    with open('../result/' + type + 'rels_types.json', 'w') as outfile:
        json.dump(vocabulary_sorted, outfile)


def keyword_counter(df, type):
    df_keywords = df[df['keywords'] != '']

    cv = CountVectorizer(tokenizer=lambda x: x.split(','))
    results = cv.fit_transform(df_keywords['keywords'])

    features = cv.get_feature_names()
    print('# of distinct keywords: ' + str(len(cv.vocabulary_)))

    freqs = zip(cv.get_feature_names(), results.sum(axis=0).tolist()[0])
    vocabulary_sorted = list(sorted(freqs, key=lambda x: -x[1]))
    with open('../result/' + type + 'vocabulary.json', 'w') as outfile:
        json.dump(vocabulary_sorted, outfile)


def calculate_field_coverage_by_column(df, column):
    exists = len(df[df[column] != '']) / len(df.index)
    print(column + ' not empty: ' + str(exists * 100) + "%")


def calculate_field_coverage_by_row(row):
    exists = 0
    for field in row.index:
        if not row[field]:
            exists += 1

    return (exists / len(row.index)) * 100


def calculate_rels_count(row):
    rels = row['rels'].split('|') if row['rels'] else []

    return len(rels)


def count_values(df, type, column):
    count = df[column].value_counts().to_json()
    with open('../result/' + type + '_' + column, 'w') as outfile:
        json.dump(count, outfile)


def calculate_metrics(df, type):
    print("number of total matches " + str(len(df.index)))

    df['field_coverage'] = df.apply(lambda x: calculate_field_coverage_by_row(x), axis=1, result_type='reduce')
    df['rels_count'] = df.apply(lambda x: calculate_rels_count(x), axis=1, result_type='reduce')

    print('field coverage by row description: ' + str(df['field_coverage'].describe()))
    print('Relations statistics' + str(df['rels_count'].describe()))
    # print('field coverage by row median: ' + str(df['field_coverage'].median))

    # print(df.groupby('sshoc_id').agg('count')
    for id in ['id', 'main_title', 'alternative_title', 'creator', 'contributor',
               'date_of_acceptance', 'description', 'format', 'publisher',
               'repository_url', 'rels', 'languages', 'keywords']:
        calculate_field_coverage_by_column(df, id)


with open('../result/all_artifacts.pkl', 'rb') as f:
    tool_map = pickle.load(f)
    software = to_dataframe(tool_map['software'])
    publication = to_dataframe(tool_map['publication'])
    other = to_dataframe(tool_map['other'])
    dataset = to_dataframe(tool_map['dataset'])
    all = software.append(publication).append(other).append(dataset)

    print('------ Metrics for Software ---------')
    rels_type_counter(software, 'software')
    keyword_counter(software, 'software')
    count_values(software, 'software', 'languages')
    count_values(software, 'software', 'format')
    calculate_metrics(software, 'software')

    print('------ Metrics for Publication ---------')
    rels_type_counter(publication, 'publication')
    count_values(publication, 'publication', 'languages')
    count_values(publication, 'publication', 'format')
    calculate_metrics(publication, 'publication')

    print('------ Metrics for Other ---------')
    rels_type_counter(other, 'other')
    count_values(other, 'other', 'languages')
    count_values(other, 'other', 'format')
    calculate_metrics(other, 'other')

    print('------ Metrics for Dataset ---------')
    rels_type_counter(dataset, 'dataset')
    count_values(dataset, 'dataset', 'languages')
    count_values(dataset, 'dataset', 'format')
    calculate_metrics(dataset, 'dataset')

    all_df = software.append(publication).append(other).append(dataset)
    print(all_df.shape)
