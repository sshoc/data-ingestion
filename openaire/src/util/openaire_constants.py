from enum import Enum


class OpenaireConstants:
    CREATOR = 'creator'
    CONTRIBUTOR = 'contributor'
    CODE_REPOSITORY_URL = 'codeRepositoryUrl'
    DATE_OF_ACCEPTANCE = 'dateofacceptance'
    FORMAT = 'format'
    OAF_ENTITY = (
        'results/result/metadata/{http://namespace.openaire.eu/'
        'oaf}entity'
    )
    OAF_RESULT = '{http://namespace.openaire.eu/oaf}result'
    OBJ_IDENTIFIER = (
        'header/{http://www.driver-repository.eu/namespace/'
        'dri}objIdentifier'
    )
    OPENAIRE_EU_OAF_RESULT = (
        'metadata/{http://namespace.openaire.eu/oaf}entity/'
        '{http://namespace.openaire.eu/oaf}result'
    )
    OPENAIRE_EU_OAF_PROJECT = (
        'metadata/{http://namespace.openaire.eu/oaf}entity/'
        '{http://namespace.openaire.eu/oaf}project'
    )
    PAGE_SIZE = 'header/size'
    PUBLISHER = 'publisher'
    RELS = 'rels'
    RESULTS = 'results/result'
    TITLE = 'title'
    TOTAL_COUNT = 'header/total'


class OpenaireEndpoint(Enum):
    SOFTWARE = 'http://api.openaire.eu/search/software'
    PUBLICATION = 'http://api.openaire.eu/search/publications'
    DATASET = "http://api.openaire.eu/search/datasets"
    PROJECT = "http://api.openaire.eu/search/projects"
    OTHER = "http://api.openaire.eu/search/other"


