from pyaml_env import parse_config


def load_openaire_config(config_file):
    return parse_config(config_file)
