from util.openaire_constants import OpenaireConstants, OpenaireEndpoint


class OpenaireResultExtractor:
    def __init__(self):
        pass

    @staticmethod
    def extract_innertext(elem):
        try:
            return elem.text
        except (StopIteration, AttributeError):
            pass

    @staticmethod
    def extract_titles(root):
        root.find(OpenaireConstants.OAF_ENTITY) \
            .find(OpenaireConstants.OAF_RESULT)

    @staticmethod
    def extract_contributors(entity):
        contributors = []

        for contributor in entity.findall(OpenaireConstants.CONTRIBUTOR):
            text = OpenaireResultExtractor.extract_innertext(contributor)
            if text is not None:
                contributors.append(text)

        return None if len(contributors) == 0 else contributors


    @staticmethod
    def extract_rels(entity):
        rels = []
        rels_types = []

        for rel in list(entity.find(OpenaireConstants.RELS)):
            text = []
            for elem in list(rel):
                if elem.text:
                    text.append(elem.text)
                if elem.tag == 'to':
                    rels_types.append(elem.attrib['class'])

            rels.append(';'.join(text))
        return (None, None) if len(rels) == 0 else (rels, rels_types)

    @staticmethod
    def extract_communities(entity):
        communities = []
        for community in entity.findall('context'):
            if community.get('type') == 'community' or community.get('type') == 'ri':
                communities.append(community.get('id'))
        return None if len(communities) == 0 else communities

    @staticmethod
    def extract_languages(entity):
        languages = []
        for lang in entity.findall('language'):
            languages.append(lang.get('classname'))

        return None if len(languages) == 0 else languages


    @staticmethod
    def extract_keywords(entity):
        keywords = []
        for keyword in entity.findall('subject'):
            if keyword.get('classid') == 'keyword':
                keywords.append(keyword.text)
        return None if len(keywords) == 0 else keywords

    @staticmethod
    def extract_entity(endpoint, tool):
        if endpoint == OpenaireEndpoint.PROJECT:
            entity = tool.find(OpenaireConstants.OPENAIRE_EU_OAF_PROJECT)
        else:
            entity = tool.find(OpenaireConstants.OPENAIRE_EU_OAF_RESULT)
        return entity

    @staticmethod
    def extract_title(titles, title_type):
        for title in titles:
            if title.get('classid') == title_type:
                return OpenaireResultExtractor.extract_innertext(title)

    @staticmethod
    def extract_count(root_element):
        return int(OpenaireResultExtractor.extract_innertext(root_element.find(OpenaireConstants.TOTAL_COUNT)))

