import logging
from os import path

import pandas as pd
import yaml

from openaire_tool_extractor import OpenaireToolExtractor
from rest.openaire_adapter import OpenaireAdapter
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter
import pickle

from util.config_helper import load_openaire_config

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
rest_client = RestClient()

openaire_config = load_openaire_config()
openaire_adapter = OpenaireAdapter(rest_client,
                                   page_size=openaire_config['openaire_adapter']['size'],
                                   max_result=openaire_config['openaire_adapter']['max_result'],
                                   phrase_match=openaire_config['openaire_adapter']['phrase_match'],
                                   result_file_path=openaire_config['openaire_adapter']['result_file_path'])
sshoc_adapter = SshocAdapter(rest_client)
tool_mapper = OpenaireToolExtractor(sshoc_adapter, openaire_adapter)

if not path.exists("result/all_artifacts.pkl"):
    all = tool_mapper.get_openaire_tools()

    with open('result/all_artifacts.pkl', 'wb') as f:
        pickle.dump(all, f)


def to_list(artifacts):
    data = []
    for item in artifacts.items():
        matches = item[1]['result']
        sshoc_id = item[0]
        for match in matches:
            tool_data = [sshoc_id,
                         match['_id'],
                         match['_main_title'],
                         match['_alternative_title'],
                         match['_creator'],
                         match['_contributor'],
                         match['_description'],
                         match['_repository_url'],
                         '|'.join(match['_rels']),
                         ','.join(match['_keywords']),
                         ','.join(match['_communities'])]
            data.append(tool_data)

    return data

with open('result/all_artifacts.pkl', 'rb') as f:
    all = pickle.load(f)

    software = all['software']

    data = to_list(software)

    df = pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                       'description',
                                       'repository_url', 'rels', 'keywords','communities'])

    with open('result/software.pkl', 'wb') as fp:
        pickle.dump(df, fp)


    publications = all['publication']

    data = to_list(publications)

    df = pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                     'description',
                                     'repository_url', 'rels', 'keywords','communities'])

    with open('result/publication.pkl', 'wb') as fp:
        pickle.dump(df, fp)


    projects = all['project']
    data = to_list(projects)

    df = pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                     'description',
                                     'repository_url', 'rels', 'keywords','communities'])


    with open('result/project.pkl', 'wb') as fp:
        pickle.dump(df, fp)


    datasets = all['dataset']
    data = to_list(datasets)

    df = pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                     'description',
                                     'repository_url', 'rels', 'keywords','communities'])


    with open('result/dataset.pkl', 'wb') as fp:
        pickle.dump(df, fp)


    others = all['other']
    data = to_list(others)

    df = pd.DataFrame(data, columns=['sshoc_id', 'id', 'main_title', 'alternative_title', 'creator', 'contributor',
                                     'description',
                                     'repository_url', 'rels', 'keywords','communities'])

    with open('result/other.pkl', 'wb') as fp:
        pickle.dump(df, fp)




    #with open('result/software_extracted.json', 'w+') as fp:
    #    json.dump(result, fp)
