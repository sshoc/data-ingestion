import logging

import yaml

from openaire_tool_extractor import OpenaireToolCounter
from rest.openaire_adapter import OpenaireAdapter
from rest.rest_client import RestClient
from rest.sshoc_adapter import SshocAdapter
import json

from pathlib import Path


def load_openaire_config():
    file = open('config/config.yaml', 'r')
    return yaml.load(file, Loader=yaml.FullLoader)

path = Path('result/counts.json')
path.parent.mkdir(parents=True, exist_ok=True)

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

rest_client = RestClient()

openaire_config = load_openaire_config()
openaire_adapter = OpenaireAdapter(rest_client, max_result = openaire_config['openaire_adapter']['max_result'])
sshoc_adapter = SshocAdapter(rest_client)

tool_mapper = OpenaireToolCounter(sshoc_adapter, openaire_adapter)

result = tool_mapper.get_openaire_tool_counts()

print(result)

with open(path, 'w+') as fp:
    json.dump(result, fp)
