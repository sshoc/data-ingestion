import json
from xml.etree import ElementTree

import requests
from http_constants.headers import HttpHeaders


class RestClient(object):
    XML = 'application/xml;charset=UTF-8'
    CONTENT_TYPE = 'Content-Type'
    JSON = 'application/json'

    @staticmethod
    def get(endpoint, **kwargs):
        response = requests.get(endpoint, **kwargs)

        if response.status_code == 200:
            if response.headers[HttpHeaders.CONTENT_TYPE] == RestClient.XML:
                return ElementTree.XML(response.content)
            elif response.headers[HttpHeaders.CONTENT_TYPE] == RestClient.JSON:
                return response.json()
            else:
                return response
        else:
            response.raise_for_status()

    @staticmethod
    def post(endpoint, body, token=None):
        headers = {"Content-Type": "application/json",
                   "Authorization": token}
        response = requests.post(endpoint, data=json.dumps(body), headers=headers)

        if response.status_code == 400 or response.status_code == 401 or response.status_code == 403 or response.status_code == 500:
            response.raise_for_status()

        return response

    @staticmethod
    def put(endpoint, body, token=None):
        headers = {"Content-Type": "application/json",
                   "Authorization": token}
        response = requests.put(endpoint, data=json.dumps(body), headers=headers)

        if response.status_code == 400 or response.status_code == 401 or response.status_code == 403 or response.status_code == 500:
            response.raise_for_status()

        return response
