import logging
import os
import re
import time
from xml.etree import ElementTree
from requests import HTTPError

from exceptions.exceptions import OpenaireRetrievalException
from rest.rest_client import RestClient

from model.openaire_tool import OpenaireTool
from util.openaire_constants import OpenaireConstants, OpenaireEndpoint
from util.openaire_extractor import OpenaireResultExtractor


class OpenaireAdapter(object):
    def __init__(self, rest_client: RestClient, page_limit=1000, page_size=200, max_result=10000, phrase_match=False,
                 result_file_path="result/xml/all/"):
        self.rest_client = rest_client
        self.page_limit = page_limit
        self.search_param_map = {
            OpenaireEndpoint.SOFTWARE: "keywords",
            OpenaireEndpoint.PUBLICATION: "keywords",
            OpenaireEndpoint.DATASET: "keywords",
            OpenaireEndpoint.OTHER: "keywords",
            OpenaireEndpoint.PROJECT: "keywords",
        }
        self.INITIAL_PAGE_NO = 1
        self.WAIT_INTERVAL = 0.4
        self.logger = logging.getLogger(__name__)
        self.max_result = max_result
        self.page_size = page_size
        self.phrase_match = phrase_match
        self.result_file_path = result_file_path

    def get_tool_count(self, endpoint_type, tool):
        self.logger.info("getting counts for " + str(tool.label))

        query_param = self.__prepare_query_params(endpoint_type, tool, self.INITIAL_PAGE_NO)

        root_element = self.rest_client.get(endpoint_type.value, params=query_param)

        return OpenaireResultExtractor.extract_count(root_element)

    def others(self, tool):
        self.logger.info("Retrieving OPENAIRE others for" + str(tool.label))

        others = []

        try:
            others = self.__call_openaire_with_pagination(tool, OpenaireEndpoint.OTHER)
        except OpenaireRetrievalException as ore:
            self.logger.debug(ore)

        return others

    def datasets(self, tool):
        self.logger.info("Retrieving OPENAIRE datasets for" + str(tool.label))
        datasets = []
        try:
            datasets = self.__call_openaire_with_pagination(tool, OpenaireEndpoint.DATASET)
        except OpenaireRetrievalException as ore:
            self.logger.debug(ore)

        return datasets

    def projects(self, tool):
        self.logger.info("Retrieving OPENAIRE projects for" + str(tool.label))
        projects = []
        try:
            projects = self.__call_openaire_with_pagination(tool, OpenaireEndpoint.PROJECT)
        except OpenaireRetrievalException as ore:
            self.logger.debug(ore)

        return projects

    def publications(self, tool):
        self.logger.info("Retrieving OPENAIRE publications for" + str(tool.label))
        publications = []
        try:
            publications =  self.__call_openaire_with_pagination(tool, OpenaireEndpoint.PUBLICATION)
        except OpenaireRetrievalException as ore:
            self.logger.debug(ore)

        return publications

    def get_tool(self, tool):
        self.logger.info("Retrieving OPENAIRE tools for " + str(tool.label))
        tools = []
        try:
            tools = self.__call_openaire_with_pagination(tool, OpenaireEndpoint.SOFTWARE)
        except OpenaireRetrievalException as ore:
            self.logger.debug(ore)
        return tools

    def __prepare_query_params(self, endpoint_type, tool, page):
        keywords = self.__prepare_keywords(tool.label)

        query_param = {'page': page, self.search_param_map[endpoint_type]: keywords, 'size': self.page_size}

        return query_param

    def __call_openaire_with_pagination(self, tool, endpoint):
        query_param = self.__prepare_query_params(endpoint, tool, self.INITIAL_PAGE_NO)

        try:
            root_element = self.rest_client.get(endpoint.value, params=query_param)
            tools = self.__extract_openaire_tools(root_element, endpoint, tool)
            total_pages = self.__get_total_pages(root_element)
            match_count = OpenaireResultExtractor.extract_count(root_element)
        except HTTPError as http_error:
            self.logger.debug('Error occured while retrieving' + endpoint.value + str(query_param))
            raise OpenaireRetrievalException('Error occured while retrieving' + endpoint.value + str(query_param)) from http_error

        if len(tools) <= self.max_result and len(tools) == match_count:
            return tools

        # self.__write_result_to_file(root_element, query_param.get('keywords'), endpoint, self.INITIAL_PAGE_NO)

        self.__call_openaire_in_loop(endpoint, tool, tools, total_pages)

        self.logger.debug("Completed extracting tools for: " + str(self.logger.info(tools)))

        return tools

    def __write_result_to_file(self, xml, name, endpoint, page):
        results = xml.findall(OpenaireConstants.RESULTS)
        for result in results:
            if endpoint == OpenaireEndpoint.PROJECT:
                titles = result.find(OpenaireConstants.OPENAIRE_EU_OAF_PROJECT).findall('title')
            else:
                titles = result.find(OpenaireConstants.OPENAIRE_EU_OAF_RESULT).findall('title')

            try:
                main_title = OpenaireResultExtractor.extract_title(titles, 'main title').replace('/', ' ')
            except AttributeError:
                self.logger.debug('Attribute main title not found')
                continue

            file_name = self.result_file_path + main_title + '.xml'
            os.makedirs(os.path.dirname(file_name), exist_ok=True)
            try:
                with open(file_name, 'w+') as f:
                    ElementTree.ElementTree(result).write(file_name)
            except OSError:
                self.logger.debug("Filename too long: " + file_name)

    def __call_openaire_in_loop(self, endpoint, keywords, tools, total_pages):
        for page in range(2, total_pages + 1):
            if page > self.page_limit:
                break

            query_param = self.__prepare_query_params(endpoint, keywords, page)

            try:
                root_element = self.rest_client.get(endpoint.value, params=query_param)
            except HTTPError:
                print('HTTPError')
                continue

            tools.extend(self.__extract_openaire_tools(root_element, endpoint, keywords))
            self.__write_result_to_file(root_element, query_param.get('keywords'), endpoint, page)
            self.logger.debug(page)

            time.sleep(self.WAIT_INTERVAL)

        return tools

    def __extract_openaire_tools(self, root, endpoint, keyword):
        tools = []
        results = root.findall(OpenaireConstants.RESULTS)

        for tool in results:
            entity = OpenaireResultExtractor.extract_entity(endpoint, tool)
            try:
                openaire_tool = OpenaireTool.from_entity(entity, tool)
                self.__append_tool(keyword, openaire_tool, tools)
            except AssertionError:
                self.logger.debug("No title found: " + str(tool))
                continue

        return tools

    def __append_tool(self, keyword, openaire_tool, tools):
        if not self.phrase_match:
            tools.append(openaire_tool.to_dict())
        elif openaire_tool.phrase_match(keyword.label):
            tools.append(openaire_tool.to_dict())

    def __get_total_pages(self, root_element):
        total = OpenaireResultExtractor.extract_count(root_element)

        if total > self.max_result:
            total = self.max_result

        page_size = int(OpenaireResultExtractor.extract_innertext(root_element.find(OpenaireConstants.PAGE_SIZE)))
        max_page = self.max_result // page_size
        no_residue = total % page_size == 0
        pages = total // page_size
        if pages < max_page:
            pages += 1
        total_pages = total // page_size if no_residue else pages

        return total_pages

    @staticmethod
    def __prepare_keywords(tool_name):
        tool_name = re.sub(r'https?:\/\/.*[\r\n]*', '', tool_name, flags=re.MULTILINE)
        tool_name = tool_name.replace('/', ' ') \
            .replace('–', ' ') \
            .replace('’', '') \
            .replace('ā', 'a') \
            .replace('|', '') \
            .replace('>', '') \
            .replace('<', '') \
            .replace('(', '') \
            .replace(')', '') \
            .replace('🎵', '') \
            .replace('•', ' ')

        return tool_name
