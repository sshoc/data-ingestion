import json
import logging
import os
import re
import time

from openapi_client.models import *
from requests.exceptions import HTTPError

from model.sshoc_tool import SshocItem, SshocPropertyType, Triple, SshocPublication
from rest.rest_client import RestClient
from enum import Enum

from util.config_helper import load_openaire_config


class SshocVocabulary():
    class VocabularyType(Enum):
        RESOURCE = 'RESOURCE'
        CATEGORY = 'CATEGORY'
        RESPONSE_CONTAINER = 'RESPONSE_CONTAINER'
        ENTITY_TYPE = 'ENTITY_TYPE'

    TOOLS_OR_SERVICE = {
        'RESOURCE': 'tools-services',
        'CATEGORY': 'tool-or-service',
        'RESPONSE_CONTAINER': 'tools',
        'ENTITY_TYPE': 'tool-or-service'
    }

    PUBLICATION = {
        'RESOURCE': 'publications',
        'CATEGORY': 'publications',
        'RESPONSE_CONTAINER': 'publications',
        'ENTITY_TYPE': 'publication'
    }

    PROPERTY_TYPE = {
        'RESOURCE': 'property-types',
        'CATEGORY': 'propertyTypes',
        'RESPONSE_CONTAINER': 'propertyTypes'
    }

    ITEM_SEARCH = {
        'RESOURCE': 'item-search',
        'CATEGORY': 'item-search',
        'RESPONSE_CONTAINER': 'items'
    }

    ITEMS_RELATIONS = {
        'RESOURCE': 'items-relations',
        'CATEGORY': 'items-relations',
    }


class SshocAdapter(object):
    TOOLS_RESOURCE = "tools-services"
    PUBLICATIONS_RESOURCE = "publications"
    SIGN_IN_RESOURCE = "auth/sign-in"
    PROPERTY_TYPES_RESOURCE = 'property-types'
    ITEM_SEARCH_RESOURCE = 'item-search'
    ITEMS_RELATIONS_RESOURCE = 'items-relations'

    def __init__(self, rest_client):
        config = load_openaire_config(os.environ.get('CONFIG_FILE'))
        self.logger = logging.getLogger(__name__)
        self._rest_client: RestClient = rest_client
        self._INITIAL_PAGE_NO = 1
        self._WAIT_INTERVAL = 0.1
        self._auth_token = None
        self.SSHOC_MP_ENDPOINT = config['sshoc']['api_base_uri']
        self.TOOLS_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocVocabulary.TOOLS_OR_SERVICE['RESOURCE']
        self.PUBLICATIONS_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocVocabulary.PUBLICATION['RESOURCE']
        self.SIGN_IN_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocAdapter.SIGN_IN_RESOURCE
        self.PROPERTY_TYPES_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocVocabulary.PROPERTY_TYPE['RESOURCE']
        self.ITEM_SEARCH_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocVocabulary.ITEM_SEARCH['RESOURCE']
        self.ITEMS_RELATIONS_ENDPOINT = self.SSHOC_MP_ENDPOINT + SshocVocabulary.ITEMS_RELATIONS['RESOURCE']

    @property
    def auth_token(self):
        return self._auth_token

    @auth_token.setter
    def auth_token(self, authToken):
        self._auth_token = authToken

    def sign_in(self, username, password):
        self.logger.info("API authentication..")

        body = {
            "username": username,
            "password": password
        }

        result = self._rest_client.post(self.SSHOC_MP_ENDPOINT + self.SIGN_IN_RESOURCE, body)
        self.auth_token = result.headers['Authorization']
        return self.auth_token

    def add_item_relation(self, subj: str, pred: str, obj: str):
        body = {
            "code": pred
        }
        try:
            response = self._rest_client.post(self.ITEMS_RELATIONS_ENDPOINT + '/' + subj + '/' + obj, body, self.auth_token)
            return self.__extract_items([json.loads(response.content)],
                                        SshocVocabulary.ITEMS_RELATIONS)[0]
        except HTTPError:
            raise HTTPError



    def create_item(self, item, entity_type: SshocVocabulary):
        self.logger.info("Ingesting item " + str(item))
        try:
            response = self._rest_client.post(
                self.SSHOC_MP_ENDPOINT + entity_type[SshocVocabulary.VocabularyType.RESOURCE.value],
                self.model_to_json(item), self.auth_token)
            result = self.__extract_items([json.loads(response.content)], entity_type)[0]
        except HTTPError:
            self.logger.warning('Creating item failed ' + HTTPError.response)
        return result

    def get_items(self, endpoint: str, category, page_limit=None):
        self.logger.info("Getting " + endpoint)

        items = []

        response = self._rest_client.get(endpoint, params={'page': self._INITIAL_PAGE_NO})
        response_type = category[SshocVocabulary.VocabularyType.RESPONSE_CONTAINER.value]
        items.extend(self.__extract_items(response[response_type], category))
        total_pages: int = response['pages']

        items.extend(self.__call_sshoc_in_loop(endpoint, total_pages, category, limit=page_limit))

        self.logger.info("Completed..")
        self.logger.info(items)

        return items

    def get_publication(self, persistent_id) -> SshocPublication:
        self.logger.info("Getting Publication: " + persistent_id)

        response = self._rest_client.get(self.SSHOC_MP_ENDPOINT + self.PUBLICATIONS_RESOURCE + '/' + persistent_id)

        result = self.__extract_items([response], SshocVocabulary.PUBLICATION)[0]

        return result

    def get_publications(self, page_limit=None) -> [SshocItem]:
        self.logger.info("Getting Publications")

        return self.get_items(self.SSHOC_MP_ENDPOINT + self.PUBLICATIONS_RESOURCE,
                              SshocVocabulary.PUBLICATION, page_limit=page_limit)

    def update_publication(self, persistent_id, publication: PublicationCore):
        self.logger.info("Updating publication")

        response = self._rest_client.put(self.PUBLICATIONS_ENDPOINT + '/' + persistent_id,
                                         self.model_to_json(publication), self.auth_token)

        result = self.__extract_items([json.loads(response.content)], SshocVocabulary.PUBLICATION)[0]
        return result

    def get_tools(self):
        self.logger.info("Getting Tools")

        return self.get_items(self.SSHOC_MP_ENDPOINT + self.TOOLS_RESOURCE,
                              SshocVocabulary.TOOLS_OR_SERVICE)

    def get_property_types(self):
        self.logger.info("Getting Property Types")

        return self.get_items(self.SSHOC_MP_ENDPOINT + self.PROPERTY_TYPES_RESOURCE, SshocVocabulary.PROPERTY_TYPE)

    def get_items_of_page(self, endpoint, sshoc_category, page):
        result = []

        query_param = {'page': page}
        response = self._rest_client.get(endpoint, params=query_param)

        result.extend(
            self.__extract_items(response[sshoc_category[SshocVocabulary.VocabularyType.RESPONSE_CONTAINER.value]],
                                 sshoc_category))

        return result

    def search_item(self, search_term, category, order="score"):
        result = []

        query_param = {
            'q': search_term,
            'categories': category,
            'order': order
        }

        response = self._rest_client.get(self.ITEM_SEARCH_ENDPOINT, params=query_param)

        result.extend(self.__extract_items(
            response[SshocVocabulary.ITEM_SEARCH[SshocVocabulary.VocabularyType.RESPONSE_CONTAINER.value]],
            SshocVocabulary.ITEM_SEARCH))

        return result

    def __call_sshoc_in_loop(self, endpoint, total_pages, sshoc_type, limit=None):
        tools = []
        total_pages = limit if limit is not None else total_pages
        for page in range(2, total_pages + 1):
            self.logger.info("Retrieving page: " + str(page))

            tools.extend(self.get_items_of_page(endpoint, sshoc_type, page))

            time.sleep(self._WAIT_INTERVAL)
            self.logger.debug(page)

        return tools

    def __extract_items(self, results, type):
        items = []
        # TODO: make this more readable
        for item in results:
            if (type[SshocVocabulary.VocabularyType.RESOURCE.value] == SshocVocabulary.TOOLS_OR_SERVICE[
                SshocVocabulary.VocabularyType.RESOURCE.value]):
                tool_dto = ToolDto(label=item['label'], description=item['description'], id=item['id'],
                                   category=item['category'], persistent_id=item['persistentId'])
                if item['accessibleAt']:
                    setattr(tool_dto, 'accessible_at', item['accessibleAt'])
                if hasattr(item, 'properties'):
                    properties = []
                    for prop in item['properties']:
                        prop_type = PropertyTypeDto(code=prop['type']['code'], label=prop['type']['label'],
                                                    type=prop['type']['type'])
                        prop_dto = PropertyDto(id=prop['id'], type=prop_type, value=prop['value'])
                        properties.append(prop_dto)

                    tool_dto['properties'] = properties

                tool = SshocItem.fromDto(dto=tool_dto)
                items.append(tool)
            elif type[SshocVocabulary.VocabularyType.RESOURCE.value] == SshocVocabulary.PUBLICATION[
                SshocVocabulary.VocabularyType.RESOURCE.value]:

                publication_domain = SshocPublication(persistent_id=item['persistentId'])
                self.map_attributes(item, publication_domain)
                items.append(publication_domain)

            elif type[SshocVocabulary.VocabularyType.RESOURCE.value] == SshocVocabulary.PROPERTY_TYPE[
                SshocVocabulary.VocabularyType.RESOURCE.value]:
                items.append(SshocPropertyType(item['code'], item['label'], item['type'], item['ord'],
                                               item['allowedVocabularies']))
            elif type[SshocVocabulary.VocabularyType.RESOURCE.value] == SshocVocabulary.ITEM_SEARCH[
                SshocVocabulary.VocabularyType.RESOURCE.value]:
                items.append(SearchItem(id=item['id'],
                                        persistent_id=item['persistentId'],
                                        label=item['label'],
                                        description=item['description'],
                                        contributors=item['contributors'],
                                        properties=item['properties'],
                                        category=item['category'],
                                        status=item['status'],
                                        owner=item['owner']
                                        ))
            elif type[SshocVocabulary.VocabularyType.RESOURCE.value] == SshocVocabulary.ITEMS_RELATIONS[
                SshocVocabulary.VocabularyType.RESOURCE.value]:
                items.append(Triple(subj=item['subject']['persistentId'], pred=item['relation']['code'],
                                    obj=item['object']['persistentId']))


        return items

    def map_attributes(self, source, target):
        for key in source.keys():
            key_snake = re.sub(r'(?<!^)(?=[A-Z])', '_', key).lower()
            if source[key] is not None and key_snake in target.attribute_map:
                if key == 'contributors':
                    contributors = self.__to_item_contributor_ids(key, source)
                    setattr(target, key_snake, contributors)
                elif key == 'properties':
                    properties = self.__to_property_cores(key, source)
                    setattr(target, key_snake, properties)
                elif key == 'externalIds':
                    external_ids = self.__to_external_ids(key, source)
                    setattr(target, key_snake, external_ids)
                elif key == 'source':
                    setattr(target, key_snake, SourceId(source['source']['id']))
                elif key == 'relatedItems':
                    related_items = self.__to_related_items(key, source)
                    setattr(target, key_snake, related_items)
                else:
                    setattr(target, key_snake, source[key])

    @staticmethod
    def __to_related_items(key, source):
        related_items = []
        for related_item in source[key]:
            item_relation_id = ItemRelationId(code=related_item['relation']['code'])
            related_items.append(RelatedItemCore(persistent_id=related_item['persistentId'], relation=item_relation_id))
        return related_items

    @staticmethod
    def __to_external_ids(key, source):
        external_ids = []
        for external_id in source[key]:
            item_external_id = ItemExternalIdId(code=external_id['identifierService']['code'])
            external_ids.append(ItemExternalIdCore(service_identifier=item_external_id,
                                                   identifier=external_id['identifier']))
        return external_ids

    @staticmethod
    def __to_property_cores(key, source):
        properties = []
        for prop in source[key]:
            property_type_id = PropertyTypeId(code=prop['type']['code'])
            property_concept = None
            property_core = None

            if 'concept' in prop and prop['concept'] is not None:
                vocabulary_id = VocabularyId(code=prop['concept']['vocabulary']['code'])
                property_concept = ConceptId(code=prop['concept']['code'], vocabulary=vocabulary_id,
                                             uri=prop['concept']['uri'])
            else:
                property_core = PropertyCore(type=property_type_id, value=prop['value'], concept=property_concept)

            properties.append(property_core)

        return properties

    @staticmethod
    def __to_item_contributor_ids(key, source):
        contributors = []
        for contributor in source[key]:
            actor_id = ActorId(id=contributor['actor']['id'])
            role_id = ActorRoleId(code=contributor['role']['code'])
            contributor_id = ItemContributorId(actor=actor_id, role=role_id)
            contributors.append(contributor_id)
        return contributors

    # TODO make this more elegant
    def model_to_json(self, model: PublicationCore):
        if type(model) == str:
            return model
        model_dict = model.to_dict()
        for key in list(model_dict):
            camel_key = model.attribute_map[key]
            if type(model_dict[key]) == dict:
                model_dict[key] = self.model_to_json(getattr(model, key))
            if type(model_dict[key]) == list:
                items = []
                for item in getattr(model, key):
                    items.append(self.model_to_json(item))
                model_dict[key] = items
            if key != camel_key:
                model_dict[camel_key] = model_dict[key]
                del model_dict[key]

        return model_dict
