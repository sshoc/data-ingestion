from util.openaire_constants import OpenaireConstants
from util.openaire_extractor import OpenaireResultExtractor


class OpenaireTool(object):
    def __init__(self, id_, main_title,
                 alternative_title='',
                 repository_url='',
                 date_of_acceptance='',
                 description='',
                 creator='',
                 contributor='',
                 format='',
                 languages=None,
                 publisher='',
                 rels=None,
                 rels_types = None,
                 communities = None,
                 keywords=None):
        assert id_ is not None
        assert main_title is not None

        self._id = id_
        self._main_title = main_title
        self._creator = '' if creator is None else creator
        self._contributor = '' if contributor is None else contributor
        self._date_of_acceptance = '' if date_of_acceptance is None else date_of_acceptance
        self._alternative_title = '' if alternative_title is None else alternative_title
        self._description = '' if description is None else description
        self._format = '' if format is None else format
        self._publisher = '' if publisher is None else publisher
        self._repository_url = '' if repository_url is None else repository_url
        self._rels = rels
        self._rels_types = rels_types
        self._communities = communities
        self._languages = languages
        self._keywords = keywords
        if languages is None:
            self._languages = []
        if rels is None:
            self._rels = []
        if rels_types is None:
            self._rels_types = []
        if communities is None:
            self._communities = []
        if keywords is None:
            self._keywords = []

    @staticmethod
    def from_entity(entity, tool):
        id_ = OpenaireResultExtractor.extract_innertext(
            tool.find(OpenaireConstants.OBJ_IDENTIFIER))
        titles = entity.findall(OpenaireConstants.TITLE)
        main_title = OpenaireResultExtractor.extract_title(titles, 'main title')

        alternative_title = OpenaireResultExtractor.extract_title(titles, 'alternative title')
        code_repo = OpenaireResultExtractor.extract_innertext(entity.find(OpenaireConstants.CODE_REPOSITORY_URL))
        contributor = OpenaireResultExtractor.extract_contributors(entity)
        creator = OpenaireResultExtractor.extract_innertext(entity.find(OpenaireConstants.CREATOR))
        date_of_acceptance = OpenaireResultExtractor.extract_innertext(entity.find(OpenaireConstants.DATE_OF_ACCEPTANCE))
        description = entity.find('description').text if entity.find('description') is not None else ''
        format = OpenaireResultExtractor.extract_innertext(entity.find(OpenaireConstants.FORMAT))
        languages = OpenaireResultExtractor.extract_languages(entity)
        communities = OpenaireResultExtractor.extract_communities(entity)
        publisher = OpenaireResultExtractor.extract_innertext(entity.find(OpenaireConstants.PUBLISHER))
        keywords = OpenaireResultExtractor.extract_keywords(entity)
        rels = OpenaireResultExtractor.extract_rels(entity)[0]
        rels_types = OpenaireResultExtractor.extract_rels(entity)[1]

        return OpenaireTool(id_, main_title, alternative_title=alternative_title, creator=creator, contributor=contributor,
                            date_of_acceptance=date_of_acceptance,
                            description=description, format=format, keywords=keywords,
                            languages=languages, publisher=publisher, rels=rels, rels_types = rels_types, repository_url=code_repo, communities=communities)


    @property
    def id(self):
        return self._id

    @property
    def main_title(self):
        return self._main_title

    @property
    def repository_url(self):
        return self._repository_url

    @property
    def alternative_title(self):
        return self._alternative_title

    @property
    def creator(self):
        return self._creator

    @property
    def contributor(self):
        return self._contributor

    @property
    def date_of_acceptance(self):
        return self._date_of_acceptance

    @property
    def format(self):
        return self._format

    @property
    def languages(self):
        return self._languages

    @property
    def publisher(self):
        return self._publisher

    @property
    def rels(self):
        return self._rels


    @property
    def rels_types(self):
        return self._rels_types

    @property
    def description(self):
        return self._description

    @property
    def keywords(self):
        return self._keywords

    @property
    def communities(self):
        return self._communities

    def phrase_match(self, phrase):
        phrase = phrase.lower()
        bool = True if phrase in self.main_title.lower() or phrase in self.alternative_title.lower() or \
                       phrase in self.description.lower() or phrase in self.keywords or \
                       phrase in self.publisher else False
        return bool

    def to_dict(self):
        return vars(self)

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.id == other.id and
                self.main_title == other.main_title and
                self.alternative_title == other.alternative_title and
                self.date_of_acceptance == other.date_of_acceptance and
                self.description == other.description and
                self.format == other.format and
                self.creator == other.creator and
                self.contributor == other.contributor and
                self.publisher == other.publisher and
                self.rels == other.rels and
                self.rels_types == other.rels_types and
                self.languages == other.languages and
                self.keywords == other.keywords and
                self.communities == other.communities and
                self.repository_url == other.repository_url

        )

    def __hash__(self):
        return hash(self._id)
