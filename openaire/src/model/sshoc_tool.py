from openapi_client.models.publication_core import PublicationCore
from openapi_client.models.tool_core import ToolCore
from openapi_client.models.tool_dto import ToolDto


class SshocPropertyType(object):
    def __init__(self, code, label, type, ord, allowed_vocabularies):
        assert code is not None
        assert label is not None
        assert type is not None
        assert ord is not None
        assert allowed_vocabularies is not None

        self._code = code
        self._label = label
        self._type = type
        self._ord = ord
        self._allowedVocabularies = allowed_vocabularies

    @property
    def code(self):
        return self._code

    @property
    def label(self):
        return self._label

    @property
    def type(self):
        return self._type

    @property
    def ord(self):
        return self._ord

    @property
    def allowedVocabularies(self):
        return self.allowedVocabularies

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.code == other.code and
                self.label == other.label and
                self.type == other.type and
                self.ord == other.ord

        )

    def __hash__(self):
        return hash(self._code)


class SshocItem(object):
    def __init__(self, label, description, id_=None, persistent_id=None, category=None, accessible_at=None,
                 properties=None):
        assert label is not None
        assert description is not None

        self._id = id_
        self._persistent_id = persistent_id
        self._label = label
        self._description = description
        self._category = category
        self._accessible_at = accessible_at
        self._properties = properties

    @property
    def id(self):
        return self._id

    @property
    def label(self):
        return self._label

    @property
    def category(self):
        return self._category

    @property
    def description(self):
        return self._description

    @property
    def accessible_at(self):
        return self._accessible_at

    @accessible_at.setter
    def accessible_at(self, accessible_at):
        self._accessible_at = accessible_at

    @property
    def persistent_id(self):
        return self._persistent_id

    @property
    def properties(self):
        return self._properties

    @properties.setter
    def properties(self, properties):
        self._properties = properties

    @staticmethod
    def fromDto(dto: ToolDto):
        item = SshocItem(label=dto.label, description=dto.description, id_=dto.id, persistent_id=dto.persistent_id,
                         category=dto.category)
        if hasattr(dto, 'accessible_at'):
            item.accessible_at = dto.accessible_at

        if hasattr(dto, 'properties'):
            item.properties = dto.properties

        return item

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.id == other.id and
                self.label == other.label and
                self.description == other.description and
                self.category == other.category

        )

    def __hash__(self):
        return hash(self._id)

    def to_dict(self):
        return {
            'id': self.id,
            'label': self.label,
            'description': self.description,
            'category': self.category,
            'accessibleAt': self.accessible_at
        }


class SshocPublication(PublicationCore):
    def __init__(self, persistent_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.persistent_id = persistent_id


class Triple(object):
    def __init__(self, subj, pred, obj):
        self._subj = subj
        self._pred = pred
        self._obj = obj

    @property
    def subj(self):
        return self._subj

    @property
    def pred(self):
        return self._pred

    @property
    def obj(self):
        return self._obj

    def __eq__(self, other):
        return (
                self.__class__ == other.__class__ and
                self.subj == other.subj and
                self.pred == other.pred and
                self.obj == other.obj
        )

    def __hash__(self):
        return hash(self.subj, self.pred, self.obj)
