import logging
import os.path
import pickle
from os import path
from rest.openaire_adapter import OpenaireEndpoint


class OpenaireToolCounter(object):

    def __init__(self, sshoc_adapter, openaire_adapter):
        self.sshoc_adapter = sshoc_adapter
        self.openaire_adapter = openaire_adapter

    def get_openaire_tool_counts(self):
        sshoc_tools = self.sshoc_adapter.get_tools()

        tool_counts = self.__retrieve_tool_counts(sshoc_tools)
        return tool_counts

    def __retrieve_tool_counts(self, sshoc_tools):
        counts: dict = self.__retrieve_counts_in_loop(sshoc_tools)

        self.sum_counts_by_type(counts)

        counts['mp_total'] = len(sshoc_tools)

        return counts

    def __retrieve_counts_in_loop(self, sshoc_tools):
        counts = {'tools': {}}
        for tool in sshoc_tools:
            other_count, project_count, publication_count, research_data_count, software_count = \
                self.get_openaire_counts(tool)

            if any(count > 0 for count in
                   [other_count, project_count, publication_count, research_data_count, software_count]):
                self.__assemble_results(counts, other_count, project_count, publication_count, research_data_count,
                                        software_count, tool)
        return counts

    def sum_counts_by_type(self, counts):
        counts['publications_total'] = self.sum_count(counts['tools'], 'publication')
        counts['software_total'] = self.sum_count(counts['tools'], 'software')
        counts['dataset_total'] = self.sum_count(counts['tools'], 'research_data')
        counts['project_total'] = self.sum_count(counts['tools'], 'project')
        counts['other_total'] = self.sum_count(counts['tools'], 'other')

    def get_openaire_counts(self, tool):
        software_count = self.openaire_adapter.get_tool_count(OpenaireEndpoint.SOFTWARE, tool)
        publication_count = self.openaire_adapter.get_tool_count(OpenaireEndpoint.PUBLICATION, tool)
        research_data_count = self.openaire_adapter.get_tool_count(OpenaireEndpoint.DATASET, tool)
        project_count = self.openaire_adapter.get_tool_count(OpenaireEndpoint.PROJECT, tool)
        other_count = self.openaire_adapter.get_tool_count(OpenaireEndpoint.OTHER, tool)

        return other_count, project_count, publication_count, research_data_count, software_count

    @staticmethod
    def __assemble_results(counts, other_count, project_count, publication_count, research_data_count,
                           software_count, tool):
        counts['tools'][str(tool.id)] = {'label': tool.label}
        counts['tools'][str(tool.id)]['software'] = {'count': software_count}
        counts['tools'][str(tool.id)]['publication'] = {'count': publication_count}
        counts['tools'][str(tool.id)]['research_data'] = {'count': research_data_count}
        counts['tools'][str(tool.id)]['project'] = {'count': project_count}
        counts['tools'][str(tool.id)]['other'] = {'count': other_count}

    @staticmethod
    def sum_count(tool_count, type_):
        count = 0
        for id_, tool in tool_count.items():
            count += tool[type_]['count']

        return count


class OpenaireToolExtractor:
    def __init__(self, sshoc_adapter, openaire_adapter):
        self.sshoc_adapter = sshoc_adapter
        self.openaire_adapter = openaire_adapter
        self.logger = logging.getLogger(__name__)

    def get_openaire_tools(self):
        sshoc_tools = self.sshoc_adapter.get_tools()

        tool_map = self.__extract_tools_in_loop(self.sshoc_adapter.get_tools())
        tool_map['publications_total'] = len(tool_map['publication'].keys())
        tool_map['software_total'] = len(tool_map['software'].keys())
        tool_map['mp_total'] = len(sshoc_tools)

        return tool_map

    def __extract_tools_in_loop(self, sshoc_tools):
        tool_map = {'software': {}, 'publication': {}, 'project': {}, 'dataset': {}, 'other': {}}
        stopwords = self.__load_stopwords()

        for tool in sshoc_tools:
            if tool.label in stopwords:
                continue

            #others = self.openaire_adapter.others(tool)
            #projects = self.openaire_adapter.projects(tool)
            #datasets = self.openaire_adapter.datasets(tool)
            publications = self.openaire_adapter.publications(tool)
            tools = self.openaire_adapter.get_tool(tool)

            results = [#{"type": "other", "data": others},
                       #{"type": "project", "data": projects},
                        #{"type": "dataset", "data": datasets},
                        {"type": "publication", "data": publications},
                        {"type": "software", "data": tools}]
            #results = [{"type": "publication", "data": publications},
            #           {"type": "software", "data": tools}]

            self.__enrich_tool_map(results, tool, tool_map)
        return tool_map

    def __load_stopwords(self):
        stopwords = []
        filepath = 'config/stopwords.txt'
        try:
            with open(filepath) as fp:
                line = fp.readline()
                cnt = 1
                while line:
                    print("Line {}: {}".format(cnt, line.strip()))
                    line = fp.readline()
                    stopwords.append(line.replace('\n', ''))
                    cnt += 1
        except IOError:
            self.logger.info("No stopwords file... Continue without stopwords")
        return stopwords

    @staticmethod
    def __enrich_tool_map(results, tool, tool_map):
        for res in results:
            data_ = res['data']
            if len(data_) > 0:
                tool_map[res['type']][str(tool.id)] = {'count': len(data_), 'result': data_}
