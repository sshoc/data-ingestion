# BUILD SSHOC MP CLIENT
```
python -m build
```
Install the built package with
```
pip install dist/sshoc_python_client_sbyim-0.0.1-py3-none-any.whl
```

# CONFIG FILE
config.INI file should be placed in the context path and should contain following properties.
'''
[SSHOC]
API_BASE_URI=http://localhost:8080/api/

[AUTH]
username=xxxx
password=xxxx
'''
# Tests
Integration tests run only with running SSHOC Marketplace Service
