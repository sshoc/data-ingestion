import unittest
from unittest.mock import Mock, call

from openaire_tool_extractor import OpenaireToolCounter, OpenaireToolExtractor
from rest.openaire_adapter import OpenaireAdapter, OpenaireEndpoint
from rest.sshoc_adapter import SshocAdapter
from fixture.fixture import OPENAIRE_TOOL_1, mock_content, SSHOC_TEST_TOOL_1, SSHOC_TEST_TOOL_2, OPENAIRE_TOOL_2


class OpenaireToolExtractorTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.sshoc_adapter = Mock()
        sshoc_tools = [SSHOC_TEST_TOOL_1, SSHOC_TEST_TOOL_2]
        self.sshoc_adapter.get_tools.return_value = sshoc_tools
        self.openaire_adapter = Mock()

    def test_get_tool_counts(self):
        self.openaire_adapter.get_tool_count.return_value = 1
        self.openaire_adapter.publications.return_value = [OPENAIRE_TOOL_2]
        tool_mapper = OpenaireToolCounter(self.sshoc_adapter, self.openaire_adapter)

        tools = tool_mapper.get_openaire_tool_counts()

        self.sshoc_adapter.get_tools.assert_called_once()
        self.openaire_adapter.get_tool_count.assert_has_calls([call(OpenaireEndpoint.SOFTWARE, SSHOC_TEST_TOOL_1),
                                                               call(OpenaireEndpoint.PUBLICATION, SSHOC_TEST_TOOL_1),
                                                               call(OpenaireEndpoint.DATASET, SSHOC_TEST_TOOL_1),
                                                               call(OpenaireEndpoint.PROJECT, SSHOC_TEST_TOOL_1),
                                                               call(OpenaireEndpoint.OTHER, SSHOC_TEST_TOOL_1),
                                                               call(OpenaireEndpoint.SOFTWARE, SSHOC_TEST_TOOL_2),
                                                               call(OpenaireEndpoint.PUBLICATION, SSHOC_TEST_TOOL_2),
                                                               call(OpenaireEndpoint.DATASET, SSHOC_TEST_TOOL_2),
                                                               call(OpenaireEndpoint.PROJECT, SSHOC_TEST_TOOL_2),
                                                               call(OpenaireEndpoint.OTHER, SSHOC_TEST_TOOL_2)])

        self.assertEqual(tools['tools'][str(SSHOC_TEST_TOOL_1.id)]['label'], SSHOC_TEST_TOOL_1.label)
        self.assertGreater(tools['tools'][str(SSHOC_TEST_TOOL_1.id)]['software']['count'], 0)
        self.assertEqual(tools['tools'][str(SSHOC_TEST_TOOL_2.id)]['label'], SSHOC_TEST_TOOL_2.label)
        self.assertGreater(tools['tools'][str(SSHOC_TEST_TOOL_2.id)]['publication']['count'], 0)
        self.assertEqual(tools['tools'][str(SSHOC_TEST_TOOL_2.id)]['label'], SSHOC_TEST_TOOL_2.label)
        self.assertGreater(tools['tools'][str(SSHOC_TEST_TOOL_2.id)]['research_data']['count'], 0)

    def test_get_tool_mappings(self):
        self.openaire_adapter.get_tool.return_value = [OPENAIRE_TOOL_1]
        self.openaire_adapter.publications.return_value = [OPENAIRE_TOOL_2]
        self.openaire_adapter.others.return_value = [OPENAIRE_TOOL_1]
        self.openaire_adapter.projects.return_value = [OPENAIRE_TOOL_2]
        self.openaire_adapter.datasets.return_value = [OPENAIRE_TOOL_1]
        tool_mapper = OpenaireToolExtractor(self.sshoc_adapter, self.openaire_adapter)

        tools = tool_mapper.get_openaire_tools()

        #self.sshoc_adapter.get_tools.assert_called_once()
        #self.openaire_adapter.get_tool.assert_has_calls([call(SSHOC_TEST_TOOL_1), call(SSHOC_TEST_TOOL_2)])
        self.assertGreater(tools['software'][str(SSHOC_TEST_TOOL_1.id)]['count'], 0)
        self.assertGreater(tools['publication'][str(SSHOC_TEST_TOOL_1.id)]['count'], 0)

    def test_openaire_tool_not_exists(self):
        self.openaire_adapter.get_tool.return_value = []
        self.openaire_adapter.publications.return_value = []
        self.openaire_adapter.others.return_value = []
        self.openaire_adapter.projects.return_value = []
        self.openaire_adapter.datasets.return_value = []
        tool_mapper = OpenaireToolExtractor(self.sshoc_adapter, self.openaire_adapter)

        tools = tool_mapper.get_openaire_tools()

        #self.sshoc_adapter.get_tools.assert_called_once()
        #self.openaire_adapter.get_tool.assert_has_calls([call(SSHOC_TEST_TOOL_1), call(SSHOC_TEST_TOOL_2)])
        self.assertIsInstance(tools['software'], dict)
        self.assertFalse(tools['software'])


class OpenaireToolExtractorIT(unittest.TestCase):
    def test_get_tool_mappings_IT(self):
        rest_client = Mock()
        sshoc_adapter = SshocAdapter(rest_client)
        openaire_adapter = OpenaireAdapter(rest_client)
        tool_mapper = OpenaireToolExtractor(sshoc_adapter, openaire_adapter)
        rest_client.get.side_effect = mock_content

        tools = tool_mapper.get_openaire_tools()

        self.assertEqual(len(tools['software']), 2)

    def test_get_tool_count_IT(self):
        rest_client = Mock()
        sshoc_adapter = SshocAdapter(rest_client)
        openaire_adapter = OpenaireAdapter(rest_client)
        tool_mapper = OpenaireToolCounter(sshoc_adapter, openaire_adapter)
        rest_client.get.side_effect = mock_content

        tools = tool_mapper.get_openaire_tool_counts()

        self.assertTrue(tools['tools']['1330'])


if __name__ == '__main__':
    unittest.main()
