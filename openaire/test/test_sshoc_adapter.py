import json
from datetime import datetime, timezone

import requests.exceptions
import responses
import sys
import unittest
from unittest.mock import Mock, call, patch, ANY
from xml.etree.ElementTree import Element

from openapi_client import ItemRelationId
from openapi_client.models.property_core import PropertyCore
from openapi_client.models.property_type_id import PropertyTypeId
from openapi_client.models.publication_core import PublicationCore
from openapi_client.models.search_item import SearchItem
from openapi_client.models.tool_core import ToolCore

from rest.sshoc_adapter import SshocVocabulary, SshocAdapter, RelatedItemCore
from test.fixture.responses.sshoc_add_relation_response import ADD_RELATION_RESPONSE
from test.fixture.responses.sshoc_response_item_search import SSHOC_RESPONSE_ITEM_SEARCH
from test.fixture.responses.sshoc_response_update_publication import UPDATE_PUBLICATION_RESPONSE

sys.path.append('../../mp_client')

from requests import Response, HTTPError
from model.sshoc_tool import SshocItem, Triple, SshocPublication
from rest.openaire_adapter import OpenaireEndpoint
from rest.rest_client import RestClient
from fixture.fixture import SSHOC_ENDPOINT, mock_multi_tools, get_openaire_test_response, \
    get_sshoc_test_response, mock_rest_client_json_response, SSHOC_TEST_TOOL_1, SSHOC_TEST_TOOL_2, \
    mock_multi_publications, SSHOC_TEST_PUBLICATION_1, SSHOC_TEST_PUBLICATION_2, SSHOC_PROPERTY_TYPE, \
    mock_property_types, TOOL_LABEL_1, TOOL_DESCRIPTION_1
from fixture.responses.sshoc_create_tool_response import CREATE_TOOL_RESPONSE

DESCRIPTION = """140kit provides a management layer for tweet collection and analysis.

Raw data cannot be passed through to the users, but any analytical process can be run across your dataset, and the data is held for as long as the user wants. When new analytical processes are created, they can be run on existing sets of data. 140kit does not claim any control of the analysis, however it retains ownership of the data collected.

Current limits:

User: 20,000 Tweets per Dataset or until Dataset time elapses

Academic User: 500,000 Tweets per Dataset or until Dataset time elapses
"""

DESCRIPTION_2 = """A software tool for the creation of 3D interactive environments. It may be used to model and interact with physical objects that currently or have previously existed in the real world, or create virtual environments that have not previously encountered. VirTool player software is available for Microsoft Windows, Apple MacOS X, Nintendo Wii and Microsoft Xbox 360.
"""
SSHOC_TOOL_1 = SshocItem("140kit", description=DESCRIPTION, id_=1330,
                         category=SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
                         accessible_at=['https://github.com/WebEcologyProject/140kit'], persistent_id="PERSISTENT_ID_1")
SSHOC_TOOL_2 = SshocItem("3DVIA Virtools", DESCRIPTION_2, id_=279,
                         category=SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
                         persistent_id="PERSISTENT_ID_2")


class RestClientTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.rest_client = RestClient()
        self.requests = Mock()

    @patch('rest.rest_client.requests.get')
    def test_get(self, mock_get):
        mock_get.return_value = get_openaire_test_response(0)

        response = self.rest_client.get(SSHOC_ENDPOINT, params={'page': 1})

        self.assertTrue(response)

    @patch('rest.rest_client.requests.get')
    def test_xml_response(self, mock_get):
        mock_get.return_value = get_openaire_test_response(0)

        response = self.rest_client.get(OpenaireEndpoint.SOFTWARE.value, params={'page': 1, 'title': 'test'})

        self.assertIsInstance(response, Element)
        self.assertTrue(response)

    @patch('rest.rest_client.requests.get')
    def test_json_response(self, mock_get):
        mock_get.return_value = get_sshoc_test_response(0)

        response = self.rest_client.get(SSHOC_ENDPOINT, params={'page': 1})

        self.assertIsInstance(response, dict)
        self.assertTrue(response)

    @patch('rest.rest_client.requests.get')
    def test_http_code_error(self, mock_get):
        with self.assertRaises(HTTPError) as ctx:
            mock_get.side_effect = requests.exceptions.HTTPError
            self.rest_client.get(SSHOC_ENDPOINT, params={'page': 1})


class SshocAdapterTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.rest_client = Mock()
        self.sshoc_adapter = SshocAdapter(self.rest_client)

    def test_sign_in_to_api(self):
        headers = {
            "Authorization": "TEST_TOKEN"
        }

        mock_response = responses.Response(method='POST',
                                           url=self.sshoc_adapter.SSHOC_MP_ENDPOINT + self.sshoc_adapter.SIGN_IN_RESOURCE,
                                           headers=headers)
        self.rest_client.post.return_value = mock_response

        result = self.sshoc_adapter.sign_in("TEST", "TEST")

        self.assertIsNotNone(result)
        self.assertEqual(headers['Authorization'], result)

    def test_get_property_types(self):
        self.rest_client.get.return_value = mock_property_types

        result = self.sshoc_adapter.get_property_types()

        self.assertIsNotNone(result)
        self.assertIn(SSHOC_PROPERTY_TYPE, result)

    def test_get_publications(self):
        self.rest_client.get.return_value = mock_multi_publications

        result = self.sshoc_adapter.get_publications()

        self.rest_client.get.assert_called_with(
            self.sshoc_adapter.SSHOC_MP_ENDPOINT + self.sshoc_adapter.PUBLICATIONS_RESOURCE, params=ANY)
        self.assertGreaterEqual(len(result), 2)
        for pub in result:
            self.assertIsInstance(pub, PublicationCore)
        self.assertIn(SSHOC_TEST_PUBLICATION_1, result)
        # self.assertIn(SSHOC_TEST_PUBLICATION_2, result)

    def test_update_publications(self):
        response = Response()
        setattr(response, 'status_code', 200)
        setattr(response, '_content', UPDATE_PUBLICATION_RESPONSE)
        self.rest_client.put.return_value = response
        now = datetime.now(timezone.utc)
        now_iso = now.strftime("%Y-%m-%dT%H:%M:%S%z")

        property_type_id = PropertyTypeId(code='processed_at')
        _property = PropertyCore(type=property_type_id, value=now_iso)
        publication = PublicationCore(label=TOOL_LABEL_1, properties=[_property])

        result: SshocPublication = self.sshoc_adapter.update_publication('TEST_PERSISTENT_ID', publication)

        self.assertIsNotNone(result)
        self.assertIsInstance(result, SshocPublication)
        self.assertEqual(result.persistent_id, 'TEST_PERSISTENT_ID')
        self.assertGreater(len(result.properties), 0)

    def test_get_multiple_tools(self):
        self.rest_client.get.return_value = mock_multi_tools

        result = self.sshoc_adapter.get_tools()

        self.rest_client.get.assert_called()
        self.assertGreaterEqual(len(result), 2)
        self.assertIn(SSHOC_TEST_TOOL_1, result)
        self.assertIn(SSHOC_TEST_TOOL_2, result)

    def test_get_tools_with_pagination(self):
        self.rest_client.get.side_effect = mock_rest_client_json_response

        result = self.sshoc_adapter.get_tools()

        self.rest_client.get.assert_has_calls(
            [call(SSHOC_ENDPOINT, params={'page': 1}), call(SSHOC_ENDPOINT, params={'page': 2})])
        self.assertGreaterEqual(len(result), 2)
        self.assertIn(SSHOC_TOOL_1, result)
        self.assertIn(SSHOC_TOOL_2, result)

    def test_get_tools_of_page(self):
        self.rest_client.get.side_effect = mock_rest_client_json_response

        result = self.sshoc_adapter.get_items_of_page(self.sshoc_adapter.TOOLS_ENDPOINT,
                                                      SshocVocabulary.TOOLS_OR_SERVICE, 2)

        self.rest_client.get.assert_called_with(ANY, params={'page': 2})
        self.assertGreaterEqual(len(result), 2)
        self.assertIn(SSHOC_TOOL_1, result)
        self.assertIn(SSHOC_TOOL_2, result)

    def test_search_tool(self):
        self.rest_client.get.return_value = json.loads(SSHOC_RESPONSE_ITEM_SEARCH)

        result = self.sshoc_adapter.search_item('test', SshocVocabulary.ITEM_SEARCH, 'score')

        self.assertIsNotNone(result)
        self.assertGreater(len(result), 0)
        assert all(isinstance(el, SearchItem) for el in result)

    def test_get_tools(self):
        self.rest_client.get.side_effect = mock_rest_client_json_response

        result = self.sshoc_adapter.get_tools()

        self.rest_client.get.assert_called()
        self.assertGreaterEqual(len(result), 1)
        self.assertIn(SSHOC_TOOL_1, result)

    def test_create_tools(self):
        response = Response()
        setattr(response, 'status_code', 200)
        setattr(response, '_content', CREATE_TOOL_RESPONSE)

        self.rest_client.post.return_value = response
        tool = ToolCore(label=TOOL_LABEL_1, description=TOOL_DESCRIPTION_1)
        self.sshoc_adapter._auth_token = "TEST_TOKEN"

        result = self.sshoc_adapter.create_item(tool, SshocVocabulary.TOOLS_OR_SERVICE)

        self.rest_client.post.assert_called_with(ANY, ANY, ANY)
        self.assertIsInstance(result, SshocItem)

    def test_add_item_relation(self):
        response = Response()
        setattr(response, 'status_code', 200)
        setattr(response, '_content', ADD_RELATION_RESPONSE)

        self.rest_client.post.return_value = response
        self.sshoc_adapter._auth_token = "TEST_TOKEN"

        result = self.sshoc_adapter.add_item_relation("ckBYXe", "mentions", "NF1hKw")

        self.rest_client.post.assert_called_with(ANY, ANY, ANY)
        self.assertIsInstance(result, Triple)

    def test_adapter_calls_rest(self):
        rest_client = RestClient()
        tool_service = SshocAdapter(rest_client)

        with patch('rest.rest_client.requests.get') as mock_get:
            mock_get.return_value = get_sshoc_test_response(0)
            result = tool_service.get_tools()

        self.assertIsInstance(result, list)
        self.assertTrue(list)

    def test_model_to_json(self):
        related_item = [RelatedItemCore(persistent_id='TEST', relation=ItemRelationId(code="mentions"))]
        publication = PublicationCore(
            label="1947Partition On The Margins - The Untold Testimonies Of Sikh, Bahawalpur And Marwari Communities",
            description="No description provided.",
            accessible_at=["https://dh2017.adho.org/abstracts/081/081.pdf"],
            related_items=related_item
        )

        json = self.sshoc_adapter.model_to_json(publication)

        self.assertIn('label', json.keys())
        self.assertIn('accessibleAt', json.keys())
        self.assertIn('externalIds', json.keys())
        self.assertIn('relatedItems', json.keys())
        self.assertNotIn('persistent_id', json['relatedItems'][0].keys())


if __name__ == '__main__':
    unittest.main()
