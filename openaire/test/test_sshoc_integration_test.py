import configparser
import os
import unittest
from datetime import datetime, timezone

from openapi_client import PublicationCore, PropertyCore, PropertyTypeId

from rest.rest_client import RestClient

from model.sshoc_tool import SshocPublication
from rest.sshoc_adapter import SshocAdapter, SshocVocabulary
from util.config_helper import load_openaire_config


class SshocPact(unittest.TestCase):
    def setUp(self) -> None:
        self.rest_client = RestClient()
        self.sshoc_adapter = SshocAdapter(self.rest_client)
        self.sshoc_adapter.auth_token = "TEST_TOKEN"

    @unittest.skip
    def test_update_publication_IT(self):
        publication = PublicationCore(label="test", description="test")
        config = load_openaire_config(os.environ['CONFIG_FILE'])
        self.sshoc_adapter.sign_in(config['sshoc']['username'], config['sshoc']['password'])
        added: SshocPublication = self.sshoc_adapter.create_item(publication, SshocVocabulary.PUBLICATION)

        retrieved: [SshocPublication] = self.sshoc_adapter.get_publications()
        now = datetime.now(timezone.utc)
        now_iso = now.strftime("%Y-%m-%dT%H:%M:%S%z")
        property_type_id = PropertyTypeId(code='processed-at')
        prop = PropertyCore(type=property_type_id, value=now_iso)
        retrieved[0].properties.append(prop)

        updated = self.sshoc_adapter.update_publication(retrieved[0].persistent_id, retrieved[0])

        self.assertIn(prop, updated.properties,)