import json
from xml.etree import ElementTree

from openapi_client.models.actor_dto import ActorDto
from openapi_client.models.actor_id import ActorId
from openapi_client.models.actor_role_dto import ActorRoleDto
from openapi_client.models.actor_role_id import ActorRoleId
from openapi_client.models.item_contributor_dto import ItemContributorDto
from openapi_client.models.item_contributor_id import ItemContributorId
from openapi_client.models.property_core import PropertyCore
from openapi_client.models.property_type_id import PropertyTypeId
from openapi_client.models.publication_dto import PublicationDto
from openapi_client.models.source_id import SourceId
from openapi_client.models.user_dto import UserDto
from requests import Response

from model.openaire_tool import OpenaireTool
from model.sshoc_tool import SshocItem, SshocPropertyType, SshocPublication
from rest.openaire_adapter import OpenaireEndpoint
from rest.sshoc_adapter import SshocVocabulary
from test.fixture.responses.openaire_multi_response_1 import OPENAIRE_MULTI_RESPONSE_1
from test.fixture.responses.openaire_multi_response_1000 import OPENAIRE_MULTI_RESPONSE_1000
from test.fixture.responses.openaire_multi_response_1001 import OPENAIRE_MULTI_RESPONSE_1001
from test.fixture.responses.openaire_multi_response_2 import OPENAIRE_MULTI_RESPONSE_2
from test.fixture.responses.openaire_multi_response_3 import OPENAIRE_MULTI_RESPONSE_3
from test.fixture.responses.openaire_no_result import OPENAIRE_NO_RESULT
from test.fixture.responses.openaire_response import OPENAIRE_RESPONSE
from test.fixture.responses.sshoc_response_body_1 import SSHOC_RESPONSE_BODY_1
from test.fixture.responses.sshoc_response_body_2 import SSHOC_RESPONSE_BODY_2
from test.fixture.responses.sshoc_response_body_default import SSHOC_RESPONSE_BODY_DEFAULT

TEST_PERSISTENT_ID_1 = "TEST_PERSISTENT_ID_1"
TEST_PERSISTENT_ID_2 = "TEST_PERSISTENT_ID_2"

TOOL_ID_2 = 123

TOOL_ID_1 = 456

TOOL_LABEL_2 = "TEST_2"

TOOL_LABEL_1 = "TEST_1"

TOOL_DESCRIPTION_2 = "TEST_DESCRIPTION_2"

TOOL_DESCRIPTION_1 = "TEST_DESCRIPTION_1"

OPENAIRE_TOOL_1 = OpenaireTool("1", "test1", "http://test.test")
OPENAIRE_TOOL_2 = OpenaireTool('openaire____::f5f1466effd6d2171f048013db6b46da', "140kit software on GitHub",
                               'https://github.com/WebEcologyProject/140kit')

SSHOC_ENDPOINT = "http://localhost:8080/api/tools-services"

ACCESSIBLE_AT = ['https://TEST']

item_1 = {
    'id': TOOL_ID_1,
    'category': SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
    'label': TOOL_LABEL_1,
    'description': TOOL_DESCRIPTION_1,
    'accessibleAt': ACCESSIBLE_AT,
    'persistentId': TEST_PERSISTENT_ID_1
}
item_2 = {
    'id': TOOL_ID_2,
    'category': SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
    'label': TOOL_LABEL_2,
    'description': TOOL_DESCRIPTION_2,
    'accessibleAt': ACCESSIBLE_AT,
    'persistentId': TEST_PERSISTENT_ID_2

}

publication_1 = {
                    "id": 24071,
                    "category": "publication",
                    "label": "11th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2016, Krakow, Poland, July 11-16, 2016, Conference Abstracts",
                    "version": None,
                    "persistentId": "5gmHmw",
                    "description": "No description provided.",
                    "licenses": [],
                    "contributors": [
                        {
                            "actor": {
                                "id": 692,
                                "name": "Maciej Eder",
                                "externalIds": [],
                                "website": None,
                                "email": None,
                                "affiliations": []
                            },
                            "role": {
                                "code": "author",
                                "label": "Author"
                            }
                        },
                        {
                            "actor": {
                                "id": 2956,
                                "name": "Jan Rybicki",
                                "externalIds": [],
                                "website": None,
                                "email": None,
                                "affiliations": []
                            },
                            "role": {
                                "code": "author",
                                "label": "Author"
                            }
                        }
                    ],
                    "properties": [
                        {
                            "id": 173171,
                            "type": {
                                "code": "year",
                                "label": "Year",
                                "type": "int",
                                "groupName": None,
                                "hidden": False,
                                "ord": 25,
                                "allowedVocabularies": []
                            },
                            "value": "2016",
                            "concept": None
                        }
                    ],
                    "externalIds": [],
                    "accessibleAt": [
                        "http://dh2016.adho.org/abstracts/"
                    ],
                    "source": {
                        "id": 453,
                        "label": "DBLP",
                        "url": "https://dblp.org",
                        "urlTemplate": "https://dblp.org/rec/{source-item-id}"
                    },
                    "sourceItemId": "conf/dihu/2016",
                    "relatedItems": [],
                    "media": [],
                    "thumbnail": None,
                    "informationContributor": {
                        "id": 2,
                        "username": "Moderator",
                        "displayName": "Moderator",
                        "status": "enabled",
                        "registrationDate": "2020-07-27T15:13:06+0000",
                        "role": "moderator",
                        "email": "moderator@example.com"
                    },
                    "lastInfoUpdate": "2020-12-30T14:00:36+0000",
                    "status": "approved",
                    "olderVersions": [
                        {
                            "id": 19580,
                            "category": "publication",
                            "label": "11th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2016, Krakow, Poland, July 11-16, 2016, Conference Abstracts",
                            "version": None,
                            "persistentId": "5gmHmw"
                        },
                        {
                            "id": 13412,
                            "category": "publication",
                            "label": "11th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2016, Krakow, Poland, July 11-16, 2016, Conference Abstracts",
                            "version": None,
                            "persistentId": "5gmHmw"
                        },
                        {
                            "id": 3696,
                            "category": "publication",
                            "label": "11th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2016, Krakow, Poland, July 11-16, 2016, Conference Abstracts",
                            "version": None,
                            "persistentId": "5gmHmw"
                        }
                    ],
                    "newerVersions": [],
                    "dateCreated": None,
                    "dateLastUpdated": None
                }
publication_2 = {
    "id": 30956,
    "category": "publication",
    "label": "12th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2017, Montréal, Canada, August 8-11, 2017, Conference Abstracts",
    "version": None,
    "persistentId": "YP755x",
    "description": "No description provided.",
    "licenses": [],
    "contributors": [
        {
            "actor": {
                "id": 1647,
                "name": "Rhian Lewis",
                "externalIds": [],
                "website": None,
                "email": None,
                "affiliations": []
            },
            "role": {
                "code": "author",
                "label": "Author"
            }
        },
        {
            "actor": {
                "id": 2535,
                "name": "Cecily Raynor",
                "externalIds": [],
                "website": None,
                "email": None,
                "affiliations": []
            },
            "role": {
                "code": "author",
                "label": "Author"
            }
        },
        {
            "actor": {
                "id": 3357,
                "name": "Dominic Forest",
                "externalIds": [],
                "website": None,
                "email": None,
                "affiliations": []
            },
            "role": {
                "code": "author",
                "label": "Author"
            }
        },
        {
            "actor": {
                "id": 293,
                "name": "Stéfan Sinclair",
                "externalIds": [],
                "website": "http://stefansinclair.name/",
                "email": "stefan.sinclair@mcgill.ca",
                "affiliations": []
            },
            "role": {
                "code": "author",
                "label": "Author"
            }
        },
        {
            "actor": {
                "id": 3089,
                "name": "Michael Sinatra",
                "externalIds": [],
                "website": None,
                "email": None,
                "affiliations": []
            },
            "role": {
                "code": "author",
                "label": "Author"
            }
        }
    ],
    "properties": [
        {
            "id": 185492,
            "type": {
                "code": "year",
                "label": "Year",
                "type": "int",
                "groupName": None,
                "hidden": False,
                "ord": 25,
                "allowedVocabularies": []
            },
            "value": "2017",
            "concept": None
        },
        {
            "id": 185493,
            "type": {
                "code": "HTTP-status-code",
                "label": "HTTP Status Code",
                "type": "string",
                "groupName": None,
                "hidden": False,
                "ord": 40,
                "allowedVocabularies": []
            },
            "value": "404",
            "concept": None
        }
    ],
    "externalIds": [],
    "accessibleAt": [
        "https://dh2017.adho.org/program/abstracts/"
    ],
    "source": {
        "id": 453,
        "label": "DBLP",
        "url": "https://dblp.org",
        "urlTemplate": "https://dblp.org/rec/{source-item-id}"
    },
    "sourceItemId": "conf/dihu/2017",
    "relatedItems": [],
    "media": [],
    "thumbnail": None,
    "informationContributor": {
        "id": 1,
        "username": "Administrator",
        "displayName": "Administrator",
        "status": "enabled",
        "registrationDate": "2020-07-27T15:13:06+0000",
        "role": "administrator",
        "email": "administrator@example.com"
    },
    "lastInfoUpdate": "2021-03-12T15:22:28+0000",
    "status": "approved",
    "olderVersions": [
        {
            "id": 23626,
            "category": "publication",
            "label": "12th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2017, Montréal, Canada, August 8-11, 2017, Conference Abstracts",
            "version": None,
            "persistentId": "YP755x"
        },
        {
            "id": 19135,
            "category": "publication",
            "label": "12th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2017, Montréal, Canada, August 8-11, 2017, Conference Abstracts",
            "version": None,
            "persistentId": "YP755x"
        },
        {
            "id": 12967,
            "category": "publication",
            "label": "12th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2017, Montréal, Canada, August 8-11, 2017, Conference Abstracts",
            "version": None,
            "persistentId": "YP755x"
        },
        {
            "id": 3225,
            "category": "publication",
            "label": "12th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2017, Montréal, Canada, August 8-11, 2017, Conference Abstracts",
            "version": None,
            "persistentId": "YP755x"
        }
    ],
    "newerVersions": [],
    "dateCreated": None,
    "dateLastUpdated": None
}

mock_property_types = {"hits": 1, "count": 1, "page": 1, "perpage": 1, "pages": 1, "propertyTypes": [
    {"code": "test", "label": "test", "type": "date", "ord": 1, "allowedVocabularies": []}]}

mock_multi_publications = {
    "hits": 1,
    "count": 2,
    "page": 1,
    "perpage": 2,
    "pages": 2,
    "publications": [
        publication_1,
        publication_2
    ]
}

mock_multi_tools = {
    'hits': 1,
    'count': 2,
    'page': 1,
    'perpage': 1,
    'pages': 1,
    'tools': [
        item_1,
        item_2
    ]
}

mock_data_1 = {
    'hits': 1,
    'count': 1,
    'page': 1,
    'perpage': 1,
    'pages': 2,
    'tools': [
        item_1
    ]
}

mock_data_2 = {
    'hits': 1,
    'count': 1,
    'page': 2,
    'perpage': 1,
    'pages': 2,
    'tools': [
        item_2
    ]
}


def mock_content(endpoint, params):
    response = mock_pagination(endpoint, params=params)
    if response.headers['Content-Type'] == 'application/xml;charset=UTF-8':
        return ElementTree.XML(response.content)
    else:
        return json.loads(response.content)


def mock_rest_client_xml_response(endpoint, params=None):
    return ElementTree.XML(mock_pagination(endpoint, params).content)


def mock_rest_client_json_response(endpoint, params=None):
    return json.loads(mock_pagination(endpoint, params).content)


def mock_pagination(endpoint, params=None):
    if endpoint in (
            OpenaireEndpoint.SOFTWARE.value,
            OpenaireEndpoint.PUBLICATION.value,
            OpenaireEndpoint.DATASET.value,
            OpenaireEndpoint.PROJECT.value,
            OpenaireEndpoint.OTHER.value
    ):
        return get_openaire_test_response(params['page'])
    else:
        return get_sshoc_test_response(params['page'])


def get_sshoc_test_response(page):
    response_ = Response()
    response_.code = "OK"
    response_.status_code = 200
    response_.headers = {'Content-Type': 'application/json'}
    if page == 1:
        response_._content = SSHOC_RESPONSE_BODY_1
    elif page == 2:
        response_._content = SSHOC_RESPONSE_BODY_2
    else:
        response_._content = SSHOC_RESPONSE_BODY_DEFAULT
    return response_


def get_openaire_test_response(page):
    response_ = Response()
    response_.code = "OK"
    response_.status_code = 200
    response_.headers = {'Content-Type': 'application/xml;charset=UTF-8'}
    # TODO change this in dict
    if page == -1:
        response_._content = OPENAIRE_NO_RESULT
    elif page == 1:
        response_._content = OPENAIRE_MULTI_RESPONSE_1
    elif page == 2:
        response_._content = OPENAIRE_MULTI_RESPONSE_2
    elif page == 3:
        response_._content = OPENAIRE_MULTI_RESPONSE_3
    elif page == 1000:
        response_._content = OPENAIRE_MULTI_RESPONSE_1000
    elif page == 1001:
        response_._content = OPENAIRE_MULTI_RESPONSE_1001
    else:
        response_._content = OPENAIRE_RESPONSE

    return response_


SSHOC_TEST_TOOL_1 = SshocItem(TOOL_LABEL_1, TOOL_DESCRIPTION_1, id_=TOOL_ID_1,
                              category=SshocVocabulary.TOOLS_OR_SERVICE[
                                  SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
                              accessible_at=ACCESSIBLE_AT,
                              persistent_id=TEST_PERSISTENT_ID_1)
SSHOC_TEST_TOOL_2 = SshocItem(TOOL_LABEL_2, TOOL_DESCRIPTION_2, id_=TOOL_ID_2,
                              category=SshocVocabulary.TOOLS_OR_SERVICE[
                                  SshocVocabulary.VocabularyType.ENTITY_TYPE.value], accessible_at=ACCESSIBLE_AT,
                              persistent_id=TEST_PERSISTENT_ID_2)

CONTRIBUTOR_1 = ItemContributorDto(ActorDto(id=692, name='Maciej Eder'), ActorRoleDto(code='author', label='Author'))
CONTRIBUTOR_2 = ItemContributorDto(ActorDto(id=2956, name='Jan Rybicki'), ActorRoleDto(code='author', label='Author'))
SSHOC_TEST_PUBLICATION_1 = SshocPublication(persistent_id='5gmHmw',
                                            label= "11th Annual International Conference of the Alliance of Digital Humanities Organizations, DH 2016, Krakow, Poland, July 11-16, 2016, Conference Abstracts",
                                            description= "No description provided.",
                                            contributors= [ItemContributorId(ActorId(id=692), ActorRoleId(code="author")),ItemContributorId(ActorId(id=2956), ActorRoleId(code="author"))],
                                            properties= [PropertyCore(PropertyTypeId(code="year"), "2016")],
                                            accessible_at= ["http://dh2016.adho.org/abstracts/"],
                                            source= SourceId(id=453),
                                            source_item_id= "conf/dihu/2016",
                                            external_ids=[],
                                            licenses=[],
                                            media=[],
                                            related_items=[])
SSHOC_TEST_PUBLICATION_2 = SshocItem(TOOL_LABEL_2, TOOL_DESCRIPTION_2, TOOL_ID_2, TEST_PERSISTENT_ID_2, SshocVocabulary.PUBLICATION[SshocVocabulary.VocabularyType.ENTITY_TYPE.value],
                                     ACCESSIBLE_AT)

SSHOC_PROPERTY_TYPE = SshocPropertyType("test", "test", "date", 1, [])

SSHOC_PHRASE_TEST_TOOL = SshocItem('3', 'Analyze-It', SshocVocabulary.TOOLS_OR_SERVICE[SshocVocabulary.VocabularyType.ENTITY_TYPE.value], accessible_at='')
