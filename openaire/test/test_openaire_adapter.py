from unittest import TestCase
from unittest.mock import Mock, call, patch, ANY
from xml.etree import ElementTree

from model.openaire_tool import OpenaireTool
from model.sshoc_tool import SshocItem
from rest.openaire_adapter import OpenaireAdapter, OpenaireEndpoint
from fixture.fixture import get_openaire_test_response, \
    mock_rest_client_xml_response, SSHOC_TEST_TOOL_1
from fixture.responses.openaire_phrase import OPENAIRE_PHRASE

SSHOC_TOOL_FOR_NON_EXISTING_TEST = SshocItem('0', 'TEST_NO_RESULT', category='No')

OPENAIRE_TOOL_1 = OpenaireTool('openaire____::f5f1466effd6d2171f048013db6b46da', "140kit software on GitHub",
                               alternative_title='140kit software on GitHub in support of \'Searching Anthropology and Twitter Do "new" fields require new tools?\'',
                               repository_url='https://github.com/WebEcologyProject/140kit',
                               description='Is a pretty cool website').to_dict()


class TestOpenaireAdapter(TestCase):
    def setUp(self) -> None:
        self.rest_client = Mock()
        self.rest_client.get.return_value = ElementTree.XML(get_openaire_test_response(0).content)
        self.adapter = OpenaireAdapter(self.rest_client)

    def test_get_count_endpoint_mapping(self):
        result = self.adapter.get_tool_count(OpenaireEndpoint.SOFTWARE, SSHOC_TEST_TOOL_1)

        self.rest_client.get.assert_has_calls(
            [call(OpenaireEndpoint.SOFTWARE.value, params={'page': 1, 'keywords': 'TEST_1', 'size': 200})])
        self.assertEqual(result, 1)

    def test_get_counts(self):
        result = self.adapter.get_tool_count(OpenaireEndpoint.SOFTWARE, SSHOC_TEST_TOOL_1)

        self.assertEqual(result, 1)

    def test_calls_rest_client_IT(self):
        with patch('rest.rest_client.requests.get') as mock_get:
            mock_get.return_value = get_openaire_test_response(0)
            result = self.adapter.get_tool(SSHOC_TEST_TOOL_1)

        self.assertEqual(len(result), 1)
        self.assertEqual(result, [OPENAIRE_TOOL_1])

    def test_get_tools_with_pagination(self):
        self.rest_client.get.side_effect = mock_rest_client_xml_response

        result = self.adapter.get_tool(SSHOC_TEST_TOOL_1)

        self.rest_client.get.assert_has_calls(
            [call(OpenaireEndpoint.SOFTWARE.value, params={'page': 1, 'keywords': ANY, 'size': ANY}),
             call(OpenaireEndpoint.SOFTWARE.value, params={'page': 2, 'keywords': ANY, 'size': ANY})])
        self.assertGreaterEqual(len(result), 27)
        for entry in result:
            self.assertIsNotNone(entry['_main_title'])
            self.assertIsNotNone(entry['_id'])

    def test_phrase_match(self):
        adapter = OpenaireAdapter(self.rest_client, phrase_match=True)
        self.rest_client.get.return_value = OPENAIRE_PHRASE

        result = adapter.get_tool(SSHOC_TEST_TOOL_1)

        self.assertEqual(0, len(result))

    def test_get_tools_with_higher_limit(self):
        self.rest_client.get.side_effect = mock_rest_client_xml_response
        adapter = OpenaireAdapter(self.rest_client, page_size=10, max_result=20)
        result = adapter.get_tool(SSHOC_TEST_TOOL_1)

        self.rest_client.get.assert_has_calls(
            [call(OpenaireEndpoint.SOFTWARE.value, params={'page': 1, 'keywords': 'TEST_1', 'size': 10}),
             call(OpenaireEndpoint.SOFTWARE.value, params={'page': 2, 'keywords': 'TEST_1', 'size': 10})])
        self.assertEqual(len(result), 20)

    def test_total_pages_should_not_exceed_1000(self):
        self.adapter = OpenaireAdapter(self.rest_client, page_limit=1)
        self.rest_client.get.side_effect = mock_rest_client_xml_response

        result = self.adapter.get_tool(SSHOC_TEST_TOOL_1)

        self.rest_client.get.assert_called_once()
        self.assertGreater(len(result), 0)
        for entry in result:
            self.assertIsNotNone(entry['_main_title'])
            self.assertIsNotNone(entry['_id'])

    def test_empty_list_if_no_result(self):
        self.rest_client.get.return_value = ElementTree.XML(get_openaire_test_response(-1).content)

        result = self.adapter.get_tool(SSHOC_TOOL_FOR_NON_EXISTING_TEST)

        self.assertIsInstance(result, list)
        self.assertFalse(result)
