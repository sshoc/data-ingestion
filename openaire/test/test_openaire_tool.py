from unittest import TestCase
from xml.etree import ElementTree

from model.openaire_tool import OpenaireTool
from rest.openaire_adapter import OpenaireConstants
from fixture.fixture import OPENAIRE_RESPONSE, get_openaire_test_response

KEYWORDS = ['kw1']

DESCRIPTION = "TEST_DESCRIPTION"

REPO_URL = "test://test"

ALTERNATIVE_TITLE = 'TEST_NAME_2'

ID = 'test_id'
NAME = 'testname'

CREATOR = 'MAX MUSTERMANN'


class TestOpenaireTool(TestCase):

    def test_two_equal_tools(self):
        tool_1 = OpenaireTool(ID, NAME, alternative_title=ALTERNATIVE_TITLE, repository_url=REPO_URL, description=DESCRIPTION, keywords=KEYWORDS, creator=CREATOR)
        tool_2 = OpenaireTool(ID, NAME, alternative_title=ALTERNATIVE_TITLE, repository_url=REPO_URL, description=DESCRIPTION, keywords=KEYWORDS, creator=CREATOR)

        self.assertEqual(tool_1, tool_2)

    def test_field_to_dict(self):
        tool_1 = OpenaireTool(ID, NAME, REPO_URL, creator=CREATOR)

        obj = tool_1.to_dict()

        self.assertIsInstance(obj, dict)
        self.assertEqual(len(obj.keys()), 15)

    def test_phrase_match(self):
        tool_1 = OpenaireTool(ID, NAME, description=DESCRIPTION, repository_url=REPO_URL, creator=CREATOR)

        obj = tool_1.phrase_match('TEST_DESCRIPTION')

        self.assertTrue(obj)

    def test_phrase_match_case_insensitive(self):
        tool_1 = OpenaireTool(ID, NAME, description=DESCRIPTION, repository_url=REPO_URL)

        obj = tool_1.phrase_match('test_description')

        self.assertTrue(obj)

    def test_instantiation_from_entity(self):
        openaire_xml_response = ElementTree.XML(get_openaire_test_response(0).content)
        result = openaire_xml_response.findall(OpenaireConstants.RESULTS)[0]
        entity = result.find(OpenaireConstants.OPENAIRE_EU_OAF_RESULT)

        openaire_tool = OpenaireTool.from_entity(entity, result)

        self.assertIsNotNone(openaire_tool)


