# ActorCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** |  | [optional] 
**external_ids** | [**list[ActorExternalIdCore]**](ActorExternalIdCore.md) |  | [optional] 
**website** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**affiliations** | [**list[ActorId]**](ActorId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

