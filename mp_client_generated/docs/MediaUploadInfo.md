# MediaUploadInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media_id** | **str** |  | [optional] 
**filename** | **str** |  | [optional] 
**mime_type** | **str** |  | [optional] 
**next_chunk_no** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

