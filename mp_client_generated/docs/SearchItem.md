# SearchItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**persistent_id** | **str** |  | [optional] 
**label** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**contributors** | [**list[ItemContributorDto]**](ItemContributorDto.md) |  | [optional] 
**properties** | [**list[PropertyDto]**](PropertyDto.md) |  | [optional] 
**category** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**owner** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

