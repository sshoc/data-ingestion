# PropertyDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**type** | [**PropertyTypeDto**](PropertyTypeDto.md) |  | [optional] 
**value** | **str** |  | [optional] 
**concept** | [**ConceptBasicDto**](ConceptBasicDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

