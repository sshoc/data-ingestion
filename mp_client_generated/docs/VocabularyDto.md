# VocabularyDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**label** | **str** |  | [optional] 
**accessible_at** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**concept_results** | [**PaginatedConcepts**](PaginatedConcepts.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

