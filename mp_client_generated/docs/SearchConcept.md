# SearchConcept

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**vocabulary** | [**VocabularyId**](VocabularyId.md) |  | [optional] 
**label** | **str** |  | [optional] 
**notation** | **str** |  | [optional] 
**definition** | **str** |  | [optional] 
**uri** | **str** |  | [optional] 
**types** | [**list[PropertyTypeId]**](PropertyTypeId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

