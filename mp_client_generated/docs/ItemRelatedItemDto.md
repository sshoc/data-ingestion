# ItemRelatedItemDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subject** | [**ItemBasicDto**](ItemBasicDto.md) |  | [optional] 
**object** | [**ItemBasicDto**](ItemBasicDto.md) |  | [optional] 
**relation** | [**ItemRelationDto**](ItemRelationDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

