# ItemContributorDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actor** | [**ActorDto**](ActorDto.md) |  | [optional] 
**role** | [**ActorRoleDto**](ActorRoleDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

