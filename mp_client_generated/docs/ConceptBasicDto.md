# ConceptBasicDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** |  | [optional] 
**vocabulary** | [**VocabularyBasicDto**](VocabularyBasicDto.md) |  | [optional] 
**label** | **str** |  | [optional] 
**notation** | **str** |  | [optional] 
**definition** | **str** |  | [optional] 
**uri** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

