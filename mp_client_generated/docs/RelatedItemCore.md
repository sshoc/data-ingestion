# RelatedItemCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**persistent_id** | **str** |  | [optional] 
**relation** | [**ItemRelationId**](ItemRelationId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

