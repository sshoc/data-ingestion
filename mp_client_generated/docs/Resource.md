# Resource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**readable** | **bool** |  | [optional] 
**url** | **str** |  | [optional] 
**filename** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**uri** | **str** |  | [optional] 
**open** | **bool** |  | [optional] 
**file** | **str** |  | [optional] 
**input_stream** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

