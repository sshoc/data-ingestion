# ItemCommentDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**body** | **str** |  | [optional] 
**creator** | [**UserDto**](UserDto.md) |  | [optional] 
**date_created** | **str** |  | [optional] 
**date_last_updated** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

