# PaginatedSearchItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hits** | **int** |  | [optional] 
**count** | **int** |  | [optional] 
**page** | **int** |  | [optional] 
**perpage** | **int** |  | [optional] 
**pages** | **int** |  | [optional] 
**q** | **str** |  | [optional] 
**order** | **list[str]** |  | [optional] 
**items** | [**list[SearchItem]**](SearchItem.md) |  | [optional] 
**categories** | [**dict(str, LabeledCheckedCount)**](LabeledCheckedCount.md) |  | [optional] 
**facets** | **dict(str, dict(str, CheckedCount))** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

