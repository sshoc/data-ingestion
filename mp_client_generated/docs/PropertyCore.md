# PropertyCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**PropertyTypeId**](PropertyTypeId.md) |  | [optional] 
**value** | **str** |  | [optional] 
**concept** | [**ConceptId**](ConceptId.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

