# ItemExternalIdCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_identifier** | [**ItemExternalIdId**](ItemExternalIdId.md) |  | 
**identifier** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

