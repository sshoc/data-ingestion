# PaginatedDatasets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hits** | **int** |  | [optional] 
**count** | **int** |  | [optional] 
**page** | **int** |  | [optional] 
**perpage** | **int** |  | [optional] 
**pages** | **int** |  | [optional] 
**datasets** | [**list[DatasetDto]**](DatasetDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

