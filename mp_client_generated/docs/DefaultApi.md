# openapi_client.DefaultApi

All URIs are relative to *https://sshoc-marketplace-api.acdh-dev.oeaw.ac.at*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete_items1**](DefaultApi.md#autocomplete_items1) | **GET** /api/item-search/autocomplete | 
[**complete_media_upload1**](DefaultApi.md#complete_media_upload1) | **POST** /api/media/upload/complete/{mediaId} | 
[**create_actor1**](DefaultApi.md#create_actor1) | **POST** /api/actors | 
[**create_actor_role1**](DefaultApi.md#create_actor_role1) | **POST** /api/actor-roles | 
[**create_actor_source1**](DefaultApi.md#create_actor_source1) | **POST** /api/actor-sources | 
[**create_dataset**](DefaultApi.md#create_dataset) | **POST** /api/datasets | 
[**create_item_comment**](DefaultApi.md#create_item_comment) | **POST** /api/items/{itemId}/comments | 
[**create_item_related_item**](DefaultApi.md#create_item_related_item) | **POST** /api/items-relations/{subjectId}/{objectId} | 
[**create_item_source1**](DefaultApi.md#create_item_source1) | **POST** /api/item-sources | 
[**create_media_source1**](DefaultApi.md#create_media_source1) | **POST** /api/media-sources | 
[**create_property_type**](DefaultApi.md#create_property_type) | **POST** /api/property-types | 
[**create_publication**](DefaultApi.md#create_publication) | **POST** /api/publications | 
[**create_source1**](DefaultApi.md#create_source1) | **POST** /api/sources | 
[**create_step1**](DefaultApi.md#create_step1) | **POST** /api/workflows/{workflowId}/steps | 
[**create_substep1**](DefaultApi.md#create_substep1) | **POST** /api/workflows/{workflowId}/steps/{stepId}/steps | 
[**create_tool**](DefaultApi.md#create_tool) | **POST** /api/tools-services | 
[**create_training_material**](DefaultApi.md#create_training_material) | **POST** /api/training-materials | 
[**create_vocabulary1**](DefaultApi.md#create_vocabulary1) | **POST** /api/vocabularies | 
[**create_workflow1**](DefaultApi.md#create_workflow1) | **POST** /api/workflows | 
[**delete_actor1**](DefaultApi.md#delete_actor1) | **DELETE** /api/actors/{id} | 
[**delete_actor_role1**](DefaultApi.md#delete_actor_role1) | **DELETE** /api/actor-roles/{roleCode} | 
[**delete_actor_source1**](DefaultApi.md#delete_actor_source1) | **DELETE** /api/actor-sources/{sourceCode} | 
[**delete_dataset1**](DefaultApi.md#delete_dataset1) | **DELETE** /api/datasets/{id} | 
[**delete_item_comment**](DefaultApi.md#delete_item_comment) | **DELETE** /api/items/{itemId}/comments/{id} | 
[**delete_item_related_item**](DefaultApi.md#delete_item_related_item) | **DELETE** /api/items-relations/{subjectId}/{objectId} | 
[**delete_item_source1**](DefaultApi.md#delete_item_source1) | **DELETE** /api/item-sources/{sourceCode} | 
[**delete_media_source1**](DefaultApi.md#delete_media_source1) | **DELETE** /api/media-sources/{code} | 
[**delete_property_type**](DefaultApi.md#delete_property_type) | **DELETE** /api/property-types/{code} | 
[**delete_publication**](DefaultApi.md#delete_publication) | **DELETE** /api/publications/{id} | 
[**delete_source1**](DefaultApi.md#delete_source1) | **DELETE** /api/sources/{id} | 
[**delete_step1**](DefaultApi.md#delete_step1) | **DELETE** /api/workflows/{workflowId}/steps/{stepId} | 
[**delete_tool**](DefaultApi.md#delete_tool) | **DELETE** /api/tools-services/{id} | 
[**delete_training_material1**](DefaultApi.md#delete_training_material1) | **DELETE** /api/training-materials/{id} | 
[**delete_vocabulary1**](DefaultApi.md#delete_vocabulary1) | **DELETE** /api/vocabularies/{code} | 
[**delete_workflow1**](DefaultApi.md#delete_workflow1) | **DELETE** /api/workflows/{workflowId} | 
[**get_actor1**](DefaultApi.md#get_actor1) | **GET** /api/actors/{id} | 
[**get_actor_role1**](DefaultApi.md#get_actor_role1) | **GET** /api/actor-roles/{roleCode} | 
[**get_actor_source1**](DefaultApi.md#get_actor_source1) | **GET** /api/actor-sources/{sourceCode} | 
[**get_actors1**](DefaultApi.md#get_actors1) | **GET** /api/actors | 
[**get_all_actor_roles1**](DefaultApi.md#get_all_actor_roles1) | **GET** /api/actor-roles | 
[**get_all_actor_sources1**](DefaultApi.md#get_all_actor_sources1) | **GET** /api/actor-sources | 
[**get_all_concept_relations1**](DefaultApi.md#get_all_concept_relations1) | **GET** /api/concept-relations | 
[**get_all_item_relations**](DefaultApi.md#get_all_item_relations) | **GET** /api/items-relations | 
[**get_all_item_sources1**](DefaultApi.md#get_all_item_sources1) | **GET** /api/item-sources | 
[**get_all_license_types**](DefaultApi.md#get_all_license_types) | **GET** /api/license-types | 
[**get_all_media_sources1**](DefaultApi.md#get_all_media_sources1) | **GET** /api/media-sources | 
[**get_comments**](DefaultApi.md#get_comments) | **GET** /api/items/{itemId}/comments | 
[**get_dataset**](DefaultApi.md#get_dataset) | **GET** /api/datasets/{id} | 
[**get_dataset_version**](DefaultApi.md#get_dataset_version) | **GET** /api/datasets/{id}/versions/{versionId} | 
[**get_datasets**](DefaultApi.md#get_datasets) | **GET** /api/datasets | 
[**get_item_categories**](DefaultApi.md#get_item_categories) | **GET** /api/items-categories | 
[**get_item_source1**](DefaultApi.md#get_item_source1) | **GET** /api/item-sources/{sourceCode} | 
[**get_items1**](DefaultApi.md#get_items1) | **GET** /api/items | 
[**get_last_comments**](DefaultApi.md#get_last_comments) | **GET** /api/items/{itemId}/last-comments | 
[**get_licenses1**](DefaultApi.md#get_licenses1) | **GET** /api/licenses | 
[**get_logged_in_user1**](DefaultApi.md#get_logged_in_user1) | **GET** /api/auth/me | 
[**get_media_file1**](DefaultApi.md#get_media_file1) | **GET** /api/media/download/{mediaId} | 
[**get_media_info1**](DefaultApi.md#get_media_info1) | **GET** /api/media/info/{mediaId} | 
[**get_media_source1**](DefaultApi.md#get_media_source1) | **GET** /api/media-sources/{code} | 
[**get_media_thumbnail1**](DefaultApi.md#get_media_thumbnail1) | **GET** /api/media/thumbnail/{mediaId} | 
[**get_property_type**](DefaultApi.md#get_property_type) | **GET** /api/property-types/{code} | 
[**get_property_types**](DefaultApi.md#get_property_types) | **GET** /api/property-types | 
[**get_publication**](DefaultApi.md#get_publication) | **GET** /api/publications/{id} | 
[**get_publication_version**](DefaultApi.md#get_publication_version) | **GET** /api/publications/{id}/versions/{versionId} | 
[**get_publications**](DefaultApi.md#get_publications) | **GET** /api/publications | 
[**get_source1**](DefaultApi.md#get_source1) | **GET** /api/sources/{id} | 
[**get_sources1**](DefaultApi.md#get_sources1) | **GET** /api/sources | 
[**get_step1**](DefaultApi.md#get_step1) | **GET** /api/workflows/{workflowId}/steps/{stepId} | 
[**get_step_version1**](DefaultApi.md#get_step_version1) | **GET** /api/workflows/{workflowId}/steps/{stepId}/versions/{versionId} | 
[**get_tool**](DefaultApi.md#get_tool) | **GET** /api/tools-services/{id} | 
[**get_tool_version**](DefaultApi.md#get_tool_version) | **GET** /api/tools-services/{id}/versions/{versionId} | 
[**get_tools**](DefaultApi.md#get_tools) | **GET** /api/tools-services | 
[**get_training_material1**](DefaultApi.md#get_training_material1) | **GET** /api/training-materials/{id} | 
[**get_training_material_version**](DefaultApi.md#get_training_material_version) | **GET** /api/training-materials/{id}/versions/{versionId} | 
[**get_training_materials**](DefaultApi.md#get_training_materials) | **GET** /api/training-materials | 
[**get_user1**](DefaultApi.md#get_user1) | **GET** /api/users/{id} | 
[**get_users1**](DefaultApi.md#get_users1) | **GET** /api/users | 
[**get_vocabularies1**](DefaultApi.md#get_vocabularies1) | **GET** /api/vocabularies | 
[**get_vocabulary1**](DefaultApi.md#get_vocabulary1) | **GET** /api/vocabularies/{code} | 
[**get_workflow1**](DefaultApi.md#get_workflow1) | **GET** /api/workflows/{workflowId} | 
[**get_workflow_version1**](DefaultApi.md#get_workflow_version1) | **GET** /api/workflows/{workflowId}/versions/{versionId} | 
[**get_workflows1**](DefaultApi.md#get_workflows1) | **GET** /api/workflows | 
[**import_media1**](DefaultApi.md#import_media1) | **POST** /api/media/upload/import | 
[**oauth21**](DefaultApi.md#oauth21) | **GET** /oauth2/authorize/eosc | Sign into the system using oauth2
[**publish_dataset**](DefaultApi.md#publish_dataset) | **POST** /api/datasets/{datasetId}/commit | 
[**publish_publication**](DefaultApi.md#publish_publication) | **POST** /api/publications/{publicationId}/commit | 
[**publish_tool**](DefaultApi.md#publish_tool) | **POST** /api/tools-services/{toolId}/commit | 
[**publish_training_material1**](DefaultApi.md#publish_training_material1) | **POST** /api/training-materials/{trainingMaterialId}/commit | 
[**publish_workflow1**](DefaultApi.md#publish_workflow1) | **POST** /api/workflows/{workflowId}/commit | 
[**rebuild_autocomplete_index1**](DefaultApi.md#rebuild_autocomplete_index1) | **PUT** /api/item-autocomplete-rebuild | 
[**register_o_auth2_user1**](DefaultApi.md#register_o_auth2_user1) | **PUT** /api/oauth/sign-up | 
[**reindex_concepts1**](DefaultApi.md#reindex_concepts1) | **PUT** /api/concept-reindex | 
[**reindex_items1**](DefaultApi.md#reindex_items1) | **PUT** /api/item-reindex | 
[**reorder_property_types**](DefaultApi.md#reorder_property_types) | **POST** /api/property-types/reorder | 
[**revert_dataset**](DefaultApi.md#revert_dataset) | **PUT** /api/datasets/{id}/versions/{versionId}/revert | 
[**revert_publication**](DefaultApi.md#revert_publication) | **PUT** /api/publications/{id}/versions/{versionId}/revert | 
[**revert_step1**](DefaultApi.md#revert_step1) | **PUT** /api/workflows/{workflowId}/steps/{stepId}/versions/{versionId}/revert | 
[**revert_tool**](DefaultApi.md#revert_tool) | **PUT** /api/tools-services/{id}/versions/{versionId}/revert | 
[**revert_training_material**](DefaultApi.md#revert_training_material) | **PUT** /api/training-materials/{id}/versions/{versionId}/revert | 
[**revert_workflow1**](DefaultApi.md#revert_workflow1) | **PUT** /api/workflows/{id}/versions/{versionId}/revert | 
[**search_concepts1**](DefaultApi.md#search_concepts1) | **GET** /api/concept-search | 
[**search_items1**](DefaultApi.md#search_items1) | **GET** /api/item-search | 
[**sign_in1**](DefaultApi.md#sign_in1) | **POST** /api/auth/sign-in | Sign into the system
[**update_actor1**](DefaultApi.md#update_actor1) | **PUT** /api/actors/{id} | 
[**update_actor_role1**](DefaultApi.md#update_actor_role1) | **PUT** /api/actor-roles/{roleCode} | 
[**update_actor_source1**](DefaultApi.md#update_actor_source1) | **PUT** /api/actor-sources/{sourceCode} | 
[**update_dataset**](DefaultApi.md#update_dataset) | **PUT** /api/datasets/{id} | 
[**update_item_comment**](DefaultApi.md#update_item_comment) | **PUT** /api/items/{itemId}/comments/{id} | 
[**update_item_source1**](DefaultApi.md#update_item_source1) | **PUT** /api/item-sources/{sourceCode} | 
[**update_media_source1**](DefaultApi.md#update_media_source1) | **PUT** /api/media-sources/{code} | 
[**update_property_type**](DefaultApi.md#update_property_type) | **PUT** /api/property-types/{code} | 
[**update_publication**](DefaultApi.md#update_publication) | **PUT** /api/publications/{id} | 
[**update_source1**](DefaultApi.md#update_source1) | **PUT** /api/sources/{id} | 
[**update_step1**](DefaultApi.md#update_step1) | **PUT** /api/workflows/{workflowId}/steps/{stepId} | 
[**update_tool**](DefaultApi.md#update_tool) | **PUT** /api/tools-services/{id} | 
[**update_training_material**](DefaultApi.md#update_training_material) | **PUT** /api/training-materials/{id} | 
[**update_user_role1**](DefaultApi.md#update_user_role1) | **PUT** /api/users/{id}/role/{userRole} | 
[**update_user_status1**](DefaultApi.md#update_user_status1) | **PUT** /api/users/{id}/status/{userStatus} | 
[**update_vocabulary1**](DefaultApi.md#update_vocabulary1) | **PUT** /api/vocabularies/{code} | 
[**update_workflow1**](DefaultApi.md#update_workflow1) | **PUT** /api/workflows/{workflowId} | 
[**upload_media1**](DefaultApi.md#upload_media1) | **POST** /api/media/upload/full | 
[**upload_media_chunk1**](DefaultApi.md#upload_media_chunk1) | **POST** /api/media/upload/chunk | 
[**validate_implicit_grant_token1**](DefaultApi.md#validate_implicit_grant_token1) | **PUT** /api/oauth/token | 

# **autocomplete_items1**
> SuggestedSearchPhrases autocomplete_items1(q)



Autocomplete for items search.

### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str | 

try:
    api_response = api_instance.autocomplete_items1(q)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->autocomplete_items1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | 

### Return type

[**SuggestedSearchPhrases**](SuggestedSearchPhrases.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **complete_media_upload1**
> MediaDetails complete_media_upload1(media_id, filename=filename)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
media_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | 
filename = 'filename_example' # str |  (optional)

try:
    api_response = api_instance.complete_media_upload1(media_id, filename=filename)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->complete_media_upload1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **media_id** | [**str**](.md)|  | 
 **filename** | **str**|  | [optional] 

### Return type

[**MediaDetails**](MediaDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_actor1**
> ActorDto create_actor1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ActorCore() # ActorCore |  (optional)

try:
    api_response = api_instance.create_actor1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_actor1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ActorCore**](ActorCore.md)|  | [optional] 

### Return type

[**ActorDto**](ActorDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_actor_role1**
> ActorRoleDto create_actor_role1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ActorRoleCore() # ActorRoleCore |  (optional)

try:
    api_response = api_instance.create_actor_role1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_actor_role1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ActorRoleCore**](ActorRoleCore.md)|  | [optional] 

### Return type

[**ActorRoleDto**](ActorRoleDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_actor_source1**
> ActorSourceDto create_actor_source1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ActorSourceCore() # ActorSourceCore |  (optional)

try:
    api_response = api_instance.create_actor_source1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_actor_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ActorSourceCore**](ActorSourceCore.md)|  | [optional] 

### Return type

[**ActorSourceDto**](ActorSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_dataset**
> DatasetDto create_dataset(body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.DatasetCore() # DatasetCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_dataset(body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DatasetCore**](DatasetCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_item_comment**
> ItemCommentDto create_item_comment(item_id, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
item_id = 'item_id_example' # str | 
body = openapi_client.ItemCommentCore() # ItemCommentCore |  (optional)

try:
    api_response = api_instance.create_item_comment(item_id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_item_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **str**|  | 
 **body** | [**ItemCommentCore**](ItemCommentCore.md)|  | [optional] 

### Return type

[**ItemCommentDto**](ItemCommentDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_item_related_item**
> ItemRelatedItemDto create_item_related_item(subject_id, object_id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
subject_id = 'subject_id_example' # str | 
object_id = 'object_id_example' # str | 
body = openapi_client.ItemRelationId() # ItemRelationId |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_item_related_item(subject_id, object_id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_item_related_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subject_id** | **str**|  | 
 **object_id** | **str**|  | 
 **body** | [**ItemRelationId**](ItemRelationId.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**ItemRelatedItemDto**](ItemRelatedItemDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_item_source1**
> ItemSourceDto create_item_source1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ItemSourceCore() # ItemSourceCore |  (optional)

try:
    api_response = api_instance.create_item_source1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_item_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ItemSourceCore**](ItemSourceCore.md)|  | [optional] 

### Return type

[**ItemSourceDto**](ItemSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_media_source1**
> MediaSourceDto create_media_source1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.MediaSourceCore() # MediaSourceCore |  (optional)

try:
    api_response = api_instance.create_media_source1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_media_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MediaSourceCore**](MediaSourceCore.md)|  | [optional] 

### Return type

[**MediaSourceDto**](MediaSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_property_type**
> PropertyTypeDto create_property_type(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.PropertyTypeCore() # PropertyTypeCore |  (optional)

try:
    api_response = api_instance.create_property_type(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_property_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PropertyTypeCore**](PropertyTypeCore.md)|  | [optional] 

### Return type

[**PropertyTypeDto**](PropertyTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_publication**
> PublicationDto create_publication(body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.PublicationCore() # PublicationCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_publication(body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PublicationCore**](PublicationCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_source1**
> SourceDto create_source1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.SourceCore() # SourceCore |  (optional)

try:
    api_response = api_instance.create_source1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**SourceCore**](SourceCore.md)|  | [optional] 

### Return type

[**SourceDto**](SourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_step1**
> StepDto create_step1(workflow_id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
body = openapi_client.StepCore() # StepCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_step1(workflow_id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_step1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **body** | [**StepCore**](StepCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_substep1**
> StepDto create_substep1(workflow_id, step_id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
body = openapi_client.StepCore() # StepCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_substep1(workflow_id, step_id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_substep1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **body** | [**StepCore**](StepCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_tool**
> ToolDto create_tool(body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ToolCore() # ToolCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_tool(body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ToolCore**](ToolCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_training_material**
> TrainingMaterialDto create_training_material(body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.TrainingMaterialCore() # TrainingMaterialCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_training_material(body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_training_material: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TrainingMaterialCore**](TrainingMaterialCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_vocabulary1**
> VocabularyBasicDto create_vocabulary1(ttl)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
ttl = openapi_client.Ttl1() # Ttl1 | 

try:
    api_response = api_instance.create_vocabulary1(ttl)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_vocabulary1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ttl** | [**Ttl1**](.md)|  | 

### Return type

[**VocabularyBasicDto**](VocabularyBasicDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_workflow1**
> WorkflowDto create_workflow1(body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.WorkflowCore() # WorkflowCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.create_workflow1(body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->create_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**WorkflowCore**](WorkflowCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_actor1**
> delete_actor1(id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 

try:
    api_instance.delete_actor1(id)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_actor1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_actor_role1**
> delete_actor_role1(role_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
role_code = 'role_code_example' # str | 

try:
    api_instance.delete_actor_role1(role_code)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_actor_role1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_code** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_actor_source1**
> delete_actor_source1(source_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 

try:
    api_instance.delete_actor_source1(source_code)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_actor_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_dataset1**
> delete_dataset1(id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_dataset1(id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_dataset1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_item_comment**
> delete_item_comment(item_id, id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
item_id = 'item_id_example' # str | 
id = 789 # int | 

try:
    api_instance.delete_item_comment(item_id, id)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_item_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **str**|  | 
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_item_related_item**
> delete_item_related_item(subject_id, object_id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
subject_id = 'subject_id_example' # str | 
object_id = 'object_id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_item_related_item(subject_id, object_id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_item_related_item: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subject_id** | **str**|  | 
 **object_id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_item_source1**
> delete_item_source1(source_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 

try:
    api_instance.delete_item_source1(source_code)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_item_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_media_source1**
> delete_media_source1(code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 

try:
    api_instance.delete_media_source1(code)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_media_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_property_type**
> PropertyTypeDto delete_property_type(code, force=force)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
force = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.delete_property_type(code, force=force)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_property_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **force** | **bool**|  | [optional] [default to false]

### Return type

[**PropertyTypeDto**](PropertyTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_publication**
> delete_publication(id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_publication(id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_source1**
> delete_source1(id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 

try:
    api_instance.delete_source1(id)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_step1**
> delete_step1(workflow_id, step_id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_step1(workflow_id, step_id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_step1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_tool**
> delete_tool(id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_tool(id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_training_material1**
> delete_training_material1(id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_training_material1(id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_training_material1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_vocabulary1**
> delete_vocabulary1(code, force=force)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
force = false # bool |  (optional) (default to false)

try:
    api_instance.delete_vocabulary1(code, force=force)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_vocabulary1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **force** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_workflow1**
> delete_workflow1(workflow_id, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
draft = false # bool |  (optional) (default to false)

try:
    api_instance.delete_workflow1(workflow_id, draft=draft)
except ApiException as e:
    print("Exception when calling DefaultApi->delete_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_actor1**
> ActorDto get_actor1(id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 

try:
    api_response = api_instance.get_actor1(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_actor1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**ActorDto**](ActorDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_actor_role1**
> ActorRoleDto get_actor_role1(role_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
role_code = 'role_code_example' # str | 

try:
    api_response = api_instance.get_actor_role1(role_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_actor_role1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_code** | **str**|  | 

### Return type

[**ActorRoleDto**](ActorRoleDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_actor_source1**
> ActorSourceDto get_actor_source1(source_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 

try:
    api_response = api_instance.get_actor_source1(source_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_actor_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 

### Return type

[**ActorSourceDto**](ActorSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_actors1**
> PaginatedActors get_actors1(q=q, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_actors1(q=q, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_actors1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedActors**](PaginatedActors.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_actor_roles1**
> list[ActorRoleDto] get_all_actor_roles1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_actor_roles1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_actor_roles1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ActorRoleDto]**](ActorRoleDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_actor_sources1**
> list[ActorSourceDto] get_all_actor_sources1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_actor_sources1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_actor_sources1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ActorSourceDto]**](ActorSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_concept_relations1**
> list[ConceptRelationDto] get_all_concept_relations1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_concept_relations1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_concept_relations1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ConceptRelationDto]**](ConceptRelationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_item_relations**
> list[ItemRelationDto] get_all_item_relations()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_item_relations()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_item_relations: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ItemRelationDto]**](ItemRelationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_item_sources1**
> list[ItemSourceDto] get_all_item_sources1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_item_sources1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_item_sources1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ItemSourceDto]**](ItemSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_license_types**
> list[LicenseTypeDto] get_all_license_types()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_license_types()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_license_types: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[LicenseTypeDto]**](LicenseTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_all_media_sources1**
> list[MediaSourceDto] get_all_media_sources1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_all_media_sources1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_all_media_sources1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[MediaSourceDto]**](MediaSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_comments**
> list[ItemCommentDto] get_comments(item_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
item_id = 'item_id_example' # str | 

try:
    api_response = api_instance.get_comments(item_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **str**|  | 

### Return type

[**list[ItemCommentDto]**](ItemCommentDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset**
> DatasetDto get_dataset(id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_dataset(id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_dataset_version**
> DatasetDto get_dataset_version(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_dataset_version(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_dataset_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_datasets**
> PaginatedDatasets get_datasets(page=page, perpage=perpage, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_datasets(page=page, perpage=perpage, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_datasets: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PaginatedDatasets**](PaginatedDatasets.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_item_categories**
> dict(str, str) get_item_categories()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_item_categories()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_item_categories: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**dict(str, str)**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_item_source1**
> ItemSourceDto get_item_source1(source_code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 

try:
    api_response = api_instance.get_item_source1(source_code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_item_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 

### Return type

[**ItemSourceDto**](ItemSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_items1**
> list[ItemBasicDto] get_items1(source_id, source_item_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_id = 789 # int | 
source_item_id = 'source_item_id_example' # str | 

try:
    api_response = api_instance.get_items1(source_id, source_item_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_items1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_id** | **int**|  | 
 **source_item_id** | **str**|  | 

### Return type

[**list[ItemBasicDto]**](ItemBasicDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_last_comments**
> list[ItemCommentDto] get_last_comments(item_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
item_id = 'item_id_example' # str | 

try:
    api_response = api_instance.get_last_comments(item_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_last_comments: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **str**|  | 

### Return type

[**list[ItemCommentDto]**](ItemCommentDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_licenses1**
> PaginatedLicenses get_licenses1(q=q, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_licenses1(q=q, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_licenses1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedLicenses**](PaginatedLicenses.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_logged_in_user1**
> UserDto get_logged_in_user1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_response = api_instance.get_logged_in_user1()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_logged_in_user1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDto**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_media_file1**
> Resource get_media_file1(media_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
media_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | 

try:
    api_response = api_instance.get_media_file1(media_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_media_file1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **media_id** | [**str**](.md)|  | 

### Return type

[**Resource**](Resource.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_media_info1**
> MediaDetails get_media_info1(media_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
media_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | 

try:
    api_response = api_instance.get_media_info1(media_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_media_info1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **media_id** | [**str**](.md)|  | 

### Return type

[**MediaDetails**](MediaDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_media_source1**
> MediaSourceDto get_media_source1(code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 

try:
    api_response = api_instance.get_media_source1(code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_media_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**MediaSourceDto**](MediaSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_media_thumbnail1**
> Resource get_media_thumbnail1(media_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
media_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str | 

try:
    api_response = api_instance.get_media_thumbnail1(media_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_media_thumbnail1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **media_id** | [**str**](.md)|  | 

### Return type

[**Resource**](Resource.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_property_type**
> PropertyTypeDto get_property_type(code)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 

try:
    api_response = api_instance.get_property_type(code)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_property_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 

### Return type

[**PropertyTypeDto**](PropertyTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_property_types**
> PaginatedPropertyTypes get_property_types(q=q, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_property_types(q=q, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_property_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedPropertyTypes**](PaginatedPropertyTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_publication**
> PublicationDto get_publication(id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_publication(id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_publication_version**
> PublicationDto get_publication_version(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_publication_version(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_publication_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_publications**
> PaginatedPublications get_publications(page=page, perpage=perpage, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_publications(page=page, perpage=perpage, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_publications: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PaginatedPublications**](PaginatedPublications.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_source1**
> SourceDto get_source1(id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 

try:
    api_response = api_instance.get_source1(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**SourceDto**](SourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_sources1**
> PaginatedSources get_sources1(q=q, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_sources1(q=q, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_sources1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedSources**](PaginatedSources.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_step1**
> StepDto get_step1(workflow_id, step_id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_step1(workflow_id, step_id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_step1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_step_version1**
> StepDto get_step_version1(workflow_id, step_id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_step_version1(workflow_id, step_id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_step_version1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tool**
> ToolDto get_tool(id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_tool(id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tool_version**
> ToolDto get_tool_version(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_tool_version(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_tool_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tools**
> PaginatedTools get_tools(page=page, perpage=perpage, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_tools(page=page, perpage=perpage, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_tools: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PaginatedTools**](PaginatedTools.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_training_material1**
> TrainingMaterialDto get_training_material1(id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_training_material1(id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_training_material1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_training_material_version**
> TrainingMaterialDto get_training_material_version(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_training_material_version(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_training_material_version: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_training_materials**
> PaginatedTrainingMaterials get_training_materials(page=page, perpage=perpage, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_training_materials(page=page, perpage=perpage, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_training_materials: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PaginatedTrainingMaterials**](PaginatedTrainingMaterials.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_user1**
> UserDto get_user1(id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 

try:
    api_response = api_instance.get_user1(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_user1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**UserDto**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_users1**
> PaginatedUsers get_users1(q=q, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_users1(q=q, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_users1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedUsers**](PaginatedUsers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_vocabularies1**
> PaginatedVocabularies get_vocabularies1(page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_vocabularies1(page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_vocabularies1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**PaginatedVocabularies**](PaginatedVocabularies.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_vocabulary1**
> VocabularyDto get_vocabulary1(code, page=page, perpage=perpage)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)

try:
    api_response = api_instance.get_vocabulary1(code, page=page, perpage=perpage)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_vocabulary1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 

### Return type

[**VocabularyDto**](VocabularyDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workflow1**
> WorkflowDto get_workflow1(workflow_id, draft=draft, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
draft = false # bool |  (optional) (default to false)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_workflow1(workflow_id, draft=draft, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **draft** | **bool**|  | [optional] [default to false]
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workflow_version1**
> WorkflowDto get_workflow_version1(workflow_id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.get_workflow_version1(workflow_id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_workflow_version1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_workflows1**
> PaginatedWorkflows get_workflows1(page=page, perpage=perpage, approved=approved)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
approved = true # bool |  (optional) (default to true)

try:
    api_response = api_instance.get_workflows1(page=page, perpage=perpage, approved=approved)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_workflows1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **approved** | **bool**|  | [optional] [default to true]

### Return type

[**PaginatedWorkflows**](PaginatedWorkflows.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **import_media1**
> MediaDetails import_media1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.MediaLocation() # MediaLocation |  (optional)

try:
    api_response = api_instance.import_media1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->import_media1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MediaLocation**](MediaLocation.md)|  | [optional] 

### Return type

[**MediaDetails**](MediaDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **oauth21**
> oauth21(success_redirect_url, failure_redirect_url, registration_redirect_url)

Sign into the system using oauth2

### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
success_redirect_url = 'success_redirect_url_example' # str | 
failure_redirect_url = 'failure_redirect_url_example' # str | 
registration_redirect_url = 'registration_redirect_url_example' # str | 

try:
    # Sign into the system using oauth2
    api_instance.oauth21(success_redirect_url, failure_redirect_url, registration_redirect_url)
except ApiException as e:
    print("Exception when calling DefaultApi->oauth21: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **success_redirect_url** | **str**|  | 
 **failure_redirect_url** | **str**|  | 
 **registration_redirect_url** | **str**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_dataset**
> DatasetDto publish_dataset(dataset_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
dataset_id = 'dataset_id_example' # str | 

try:
    api_response = api_instance.publish_dataset(dataset_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->publish_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataset_id** | **str**|  | 

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_publication**
> PublicationDto publish_publication(publication_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
publication_id = 'publication_id_example' # str | 

try:
    api_response = api_instance.publish_publication(publication_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->publish_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **publication_id** | **str**|  | 

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_tool**
> ToolDto publish_tool(tool_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
tool_id = 'tool_id_example' # str | 

try:
    api_response = api_instance.publish_tool(tool_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->publish_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tool_id** | **str**|  | 

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_training_material1**
> TrainingMaterialDto publish_training_material1(training_material_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
training_material_id = 'training_material_id_example' # str | 

try:
    api_response = api_instance.publish_training_material1(training_material_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->publish_training_material1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **training_material_id** | **str**|  | 

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **publish_workflow1**
> WorkflowDto publish_workflow1(workflow_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 

try:
    api_response = api_instance.publish_workflow1(workflow_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->publish_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rebuild_autocomplete_index1**
> rebuild_autocomplete_index1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_instance.rebuild_autocomplete_index1()
except ApiException as e:
    print("Exception when calling DefaultApi->rebuild_autocomplete_index1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register_o_auth2_user1**
> UserDto register_o_auth2_user1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.OAuthRegistrationData() # OAuthRegistrationData |  (optional)

try:
    api_response = api_instance.register_o_auth2_user1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->register_o_auth2_user1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**OAuthRegistrationData**](OAuthRegistrationData.md)|  | [optional] 

### Return type

[**UserDto**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reindex_concepts1**
> reindex_concepts1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_instance.reindex_concepts1()
except ApiException as e:
    print("Exception when calling DefaultApi->reindex_concepts1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reindex_items1**
> reindex_items1()



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()

try:
    api_instance.reindex_items1()
except ApiException as e:
    print("Exception when calling DefaultApi->reindex_items1: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reorder_property_types**
> reorder_property_types(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.PropertyTypesReordering() # PropertyTypesReordering |  (optional)

try:
    api_instance.reorder_property_types(body=body)
except ApiException as e:
    print("Exception when calling DefaultApi->reorder_property_types: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**PropertyTypesReordering**](PropertyTypesReordering.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_dataset**
> DatasetDto revert_dataset(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_dataset(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_publication**
> PublicationDto revert_publication(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_publication(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_step1**
> StepDto revert_step1(workflow_id, step_id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_step1(workflow_id, step_id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_step1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_tool**
> ToolDto revert_tool(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_tool(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_training_material**
> TrainingMaterialDto revert_training_material(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_training_material(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_training_material: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **revert_workflow1**
> WorkflowDto revert_workflow1(id, version_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
version_id = 789 # int | 

try:
    api_response = api_instance.revert_workflow1(id, version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->revert_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **version_id** | **int**|  | 

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_concepts1**
> PaginatedSearchConcepts search_concepts1(q=q, types=types, page=page, perpage=perpage, advanced=advanced)



Search among concepts.

### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
types = ['types_example'] # list[str] |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
advanced = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.search_concepts1(q=q, types=types, page=page, perpage=perpage, advanced=advanced)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->search_concepts1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **types** | [**list[str]**](str.md)|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **advanced** | **bool**|  | [optional] [default to false]

### Return type

[**PaginatedSearchConcepts**](PaginatedSearchConcepts.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_items1**
> PaginatedSearchItems search_items1(q=q, categories=categories, order=order, page=page, perpage=perpage, advanced=advanced, f=f)



Search among items.

### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
q = 'q_example' # str |  (optional)
categories = ['categories_example'] # list[str] |  (optional)
order = ['order_example'] # list[str] |  (optional)
page = 56 # int |  (optional)
perpage = 56 # int |  (optional)
advanced = false # bool |  (optional) (default to false)
f = 'f_example' # str | Facets parameters should be provided with putting multiple f.{filter-name}={value} as request parameters. Allowed filter names: activity, source, keyword. (optional)

try:
    api_response = api_instance.search_items1(q=q, categories=categories, order=order, page=page, perpage=perpage, advanced=advanced, f=f)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->search_items1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **str**|  | [optional] 
 **categories** | [**list[str]**](str.md)|  | [optional] 
 **order** | [**list[str]**](str.md)|  | [optional] 
 **page** | **int**|  | [optional] 
 **perpage** | **int**|  | [optional] 
 **advanced** | **bool**|  | [optional] [default to false]
 **f** | **str**| Facets parameters should be provided with putting multiple f.{filter-name}&#x3D;{value} as request parameters. Allowed filter names: activity, source, keyword. | [optional] 

### Return type

[**PaginatedSearchItems**](PaginatedSearchItems.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sign_in1**
> sign_in1(body=body)

Sign into the system

### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.LoginData() # LoginData |  (optional)

try:
    # Sign into the system
    api_instance.sign_in1(body=body)
except ApiException as e:
    print("Exception when calling DefaultApi->sign_in1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LoginData**](LoginData.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_actor1**
> ActorDto update_actor1(id, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 
body = openapi_client.ActorCore() # ActorCore |  (optional)

try:
    api_response = api_instance.update_actor1(id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_actor1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **body** | [**ActorCore**](ActorCore.md)|  | [optional] 

### Return type

[**ActorDto**](ActorDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_actor_role1**
> ActorRoleDto update_actor_role1(role_code, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
role_code = 'role_code_example' # str | 
body = openapi_client.ActorRoleCore() # ActorRoleCore |  (optional)

try:
    api_response = api_instance.update_actor_role1(role_code, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_actor_role1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_code** | **str**|  | 
 **body** | [**ActorRoleCore**](ActorRoleCore.md)|  | [optional] 

### Return type

[**ActorRoleDto**](ActorRoleDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_actor_source1**
> ActorSourceDto update_actor_source1(source_code, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 
body = openapi_client.ActorSourceCore() # ActorSourceCore |  (optional)

try:
    api_response = api_instance.update_actor_source1(source_code, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_actor_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 
 **body** | [**ActorSourceCore**](ActorSourceCore.md)|  | [optional] 

### Return type

[**ActorSourceDto**](ActorSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_dataset**
> DatasetDto update_dataset(id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
body = openapi_client.DatasetCore() # DatasetCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_dataset(id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_dataset: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **body** | [**DatasetCore**](DatasetCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**DatasetDto**](DatasetDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_item_comment**
> ItemCommentDto update_item_comment(item_id, id, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
item_id = 'item_id_example' # str | 
id = 789 # int | 
body = openapi_client.ItemCommentCore() # ItemCommentCore |  (optional)

try:
    api_response = api_instance.update_item_comment(item_id, id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_item_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **str**|  | 
 **id** | **int**|  | 
 **body** | [**ItemCommentCore**](ItemCommentCore.md)|  | [optional] 

### Return type

[**ItemCommentDto**](ItemCommentDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_item_source1**
> ItemSourceDto update_item_source1(source_code, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
source_code = 'source_code_example' # str | 
body = openapi_client.ItemSourceCore() # ItemSourceCore |  (optional)

try:
    api_response = api_instance.update_item_source1(source_code, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_item_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source_code** | **str**|  | 
 **body** | [**ItemSourceCore**](ItemSourceCore.md)|  | [optional] 

### Return type

[**ItemSourceDto**](ItemSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_media_source1**
> MediaSourceDto update_media_source1(code, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
body = openapi_client.MediaSourceCore() # MediaSourceCore |  (optional)

try:
    api_response = api_instance.update_media_source1(code, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_media_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **body** | [**MediaSourceCore**](MediaSourceCore.md)|  | [optional] 

### Return type

[**MediaSourceDto**](MediaSourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_property_type**
> PropertyTypeDto update_property_type(code, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
body = openapi_client.PropertyTypeCore() # PropertyTypeCore |  (optional)

try:
    api_response = api_instance.update_property_type(code, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_property_type: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **body** | [**PropertyTypeCore**](PropertyTypeCore.md)|  | [optional] 

### Return type

[**PropertyTypeDto**](PropertyTypeDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_publication**
> PublicationDto update_publication(id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
body = openapi_client.PublicationCore() # PublicationCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_publication(id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_publication: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **body** | [**PublicationCore**](PublicationCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**PublicationDto**](PublicationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_source1**
> SourceDto update_source1(id, body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 
body = openapi_client.SourceCore() # SourceCore |  (optional)

try:
    api_response = api_instance.update_source1(id, body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_source1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **body** | [**SourceCore**](SourceCore.md)|  | [optional] 

### Return type

[**SourceDto**](SourceDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_step1**
> StepDto update_step1(workflow_id, step_id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
step_id = 'step_id_example' # str | 
body = openapi_client.StepCore() # StepCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_step1(workflow_id, step_id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_step1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **step_id** | **str**|  | 
 **body** | [**StepCore**](StepCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**StepDto**](StepDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_tool**
> ToolDto update_tool(id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
body = openapi_client.ToolCore() # ToolCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_tool(id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_tool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **body** | [**ToolCore**](ToolCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**ToolDto**](ToolDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_training_material**
> TrainingMaterialDto update_training_material(id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 'id_example' # str | 
body = openapi_client.TrainingMaterialCore() # TrainingMaterialCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_training_material(id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_training_material: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **body** | [**TrainingMaterialCore**](TrainingMaterialCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**TrainingMaterialDto**](TrainingMaterialDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_role1**
> UserDto update_user_role1(id, user_role)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 
user_role = 'user_role_example' # str | 

try:
    api_response = api_instance.update_user_role1(id, user_role)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_user_role1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **user_role** | **str**|  | 

### Return type

[**UserDto**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_user_status1**
> UserDto update_user_status1(id, user_status)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
id = 789 # int | 
user_status = 'user_status_example' # str | 

try:
    api_response = api_instance.update_user_status1(id, user_status)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_user_status1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **user_status** | **str**|  | 

### Return type

[**UserDto**](UserDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_vocabulary1**
> VocabularyBasicDto update_vocabulary1(code, ttl, force=force)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
code = 'code_example' # str | 
ttl = openapi_client.Ttl() # Ttl | 
force = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_vocabulary1(code, ttl, force=force)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_vocabulary1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | 
 **ttl** | [**Ttl**](.md)|  | 
 **force** | **bool**|  | [optional] [default to false]

### Return type

[**VocabularyBasicDto**](VocabularyBasicDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_workflow1**
> WorkflowDto update_workflow1(workflow_id, body=body, draft=draft)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
workflow_id = 'workflow_id_example' # str | 
body = openapi_client.WorkflowCore() # WorkflowCore |  (optional)
draft = false # bool |  (optional) (default to false)

try:
    api_response = api_instance.update_workflow1(workflow_id, body=body, draft=draft)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->update_workflow1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **workflow_id** | **str**|  | 
 **body** | [**WorkflowCore**](WorkflowCore.md)|  | [optional] 
 **draft** | **bool**|  | [optional] [default to false]

### Return type

[**WorkflowDto**](WorkflowDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_media1**
> MediaDetails upload_media1(file)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
file = '/path/to/file' # file | 

try:
    api_response = api_instance.upload_media1(file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->upload_media1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | [**file**](.md)|  | 

### Return type

[**MediaDetails**](MediaDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload_media_chunk1**
> MediaUploadInfo upload_media_chunk1(no, chunk, media_id=media_id)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
no = 56 # int | 
chunk = openapi_client.Chunk() # Chunk | 
media_id = '38400000-8cf0-11bd-b23e-10b96e4ef00d' # str |  (optional)

try:
    api_response = api_instance.upload_media_chunk1(no, chunk, media_id=media_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->upload_media_chunk1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **no** | **int**|  | 
 **chunk** | [**Chunk**](.md)|  | 
 **media_id** | [**str**](.md)|  | [optional] 

### Return type

[**MediaUploadInfo**](MediaUploadInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validate_implicit_grant_token1**
> OAuthRegistrationDto validate_implicit_grant_token1(body=body)



### Example
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = openapi_client.DefaultApi()
body = openapi_client.ImplicitGrantTokenData() # ImplicitGrantTokenData |  (optional)

try:
    api_response = api_instance.validate_implicit_grant_token1(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->validate_implicit_grant_token1: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ImplicitGrantTokenData**](ImplicitGrantTokenData.md)|  | [optional] 

### Return type

[**OAuthRegistrationDto**](OAuthRegistrationDto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

