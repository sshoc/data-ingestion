# ActorExternalIdDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier_service** | [**ActorSourceDto**](ActorSourceDto.md) |  | [optional] 
**identifier** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

