# ActorDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**external_ids** | [**list[ActorExternalIdDto]**](ActorExternalIdDto.md) |  | [optional] 
**website** | **str** |  | [optional] 
**email** | **str** |  | [optional] 
**affiliations** | [**list[ActorDto]**](ActorDto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

