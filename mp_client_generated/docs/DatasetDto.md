# DatasetDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**category** | **str** |  | [optional] 
**label** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**persistent_id** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**licenses** | [**list[LicenseDto]**](LicenseDto.md) |  | [optional] 
**contributors** | [**list[ItemContributorDto]**](ItemContributorDto.md) |  | [optional] 
**properties** | [**list[PropertyDto]**](PropertyDto.md) |  | [optional] 
**external_ids** | [**list[ItemExternalIdDto]**](ItemExternalIdDto.md) |  | [optional] 
**accessible_at** | **list[str]** |  | [optional] 
**source** | [**SourceBasicDto**](SourceBasicDto.md) |  | [optional] 
**source_item_id** | **str** |  | [optional] 
**related_items** | [**list[RelatedItemDto]**](RelatedItemDto.md) |  | [optional] 
**media** | [**list[ItemMediaDto]**](ItemMediaDto.md) |  | [optional] 
**thumbnail** | [**ItemThumbnailId**](ItemThumbnailId.md) |  | [optional] 
**information_contributor** | [**UserDto**](UserDto.md) |  | [optional] 
**last_info_update** | **str** |  | [optional] 
**status** | **str** |  | [optional] 
**older_versions** | [**list[ItemBasicDto]**](ItemBasicDto.md) |  | [optional] 
**newer_versions** | [**list[ItemBasicDto]**](ItemBasicDto.md) |  | [optional] 
**date_created** | **str** |  | [optional] 
**date_last_updated** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

