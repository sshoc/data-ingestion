# MediaDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**media_id** | **str** |  | [optional] 
**category** | **str** |  | [optional] 
**location** | [**MediaLocation**](MediaLocation.md) |  | [optional] 
**filename** | **str** |  | [optional] 
**mime_type** | **str** |  | [optional] 
**has_thumbnail** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

