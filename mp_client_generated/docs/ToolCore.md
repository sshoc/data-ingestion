# ToolCore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**licenses** | [**list[LicenseId]**](LicenseId.md) |  | [optional] 
**contributors** | [**list[ItemContributorId]**](ItemContributorId.md) |  | [optional] 
**external_ids** | [**list[ItemExternalIdCore]**](ItemExternalIdCore.md) |  | [optional] 
**properties** | [**list[PropertyCore]**](PropertyCore.md) |  | [optional] 
**related_items** | [**list[RelatedItemCore]**](RelatedItemCore.md) |  | [optional] 
**media** | [**list[ItemMediaCore]**](ItemMediaCore.md) |  | [optional] 
**thumbnail** | [**ItemThumbnailId**](ItemThumbnailId.md) |  | [optional] 
**accessible_at** | **list[str]** |  | [optional] 
**source** | [**SourceId**](SourceId.md) |  | [optional] 
**source_item_id** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

