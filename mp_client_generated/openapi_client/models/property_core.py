# coding: utf-8

"""
    SSHOC Marketplace API

    Social Sciences and Humanities Open Cloud Marketplace  # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PropertyCore(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'type': 'PropertyTypeId',
        'value': 'str',
        'concept': 'ConceptId'
    }

    attribute_map = {
        'type': 'type',
        'value': 'value',
        'concept': 'concept'
    }

    def __init__(self, type=None, value=None, concept=None):  # noqa: E501
        """PropertyCore - a model defined in Swagger"""  # noqa: E501
        self._type = None
        self._value = None
        self._concept = None
        self.discriminator = None
        if type is not None:
            self.type = type
        if value is not None:
            self.value = value
        if concept is not None:
            self.concept = concept

    @property
    def type(self):
        """Gets the type of this PropertyCore.  # noqa: E501


        :return: The type of this PropertyCore.  # noqa: E501
        :rtype: PropertyTypeId
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this PropertyCore.


        :param type: The type of this PropertyCore.  # noqa: E501
        :type: PropertyTypeId
        """

        self._type = type

    @property
    def value(self):
        """Gets the value of this PropertyCore.  # noqa: E501


        :return: The value of this PropertyCore.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this PropertyCore.


        :param value: The value of this PropertyCore.  # noqa: E501
        :type: str
        """

        self._value = value

    @property
    def concept(self):
        """Gets the concept of this PropertyCore.  # noqa: E501


        :return: The concept of this PropertyCore.  # noqa: E501
        :rtype: ConceptId
        """
        return self._concept

    @concept.setter
    def concept(self, concept):
        """Sets the concept of this PropertyCore.


        :param concept: The concept of this PropertyCore.  # noqa: E501
        :type: ConceptId
        """

        self._concept = concept

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PropertyCore, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PropertyCore):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
