# coding: utf-8

"""
    SSHOC Marketplace API

    Social Sciences and Humanities Open Cloud Marketplace  # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class PaginatedConcepts(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'hits': 'int',
        'count': 'int',
        'page': 'int',
        'perpage': 'int',
        'pages': 'int',
        'concepts': 'list[ConceptDto]'
    }

    attribute_map = {
        'hits': 'hits',
        'count': 'count',
        'page': 'page',
        'perpage': 'perpage',
        'pages': 'pages',
        'concepts': 'concepts'
    }

    def __init__(self, hits=None, count=None, page=None, perpage=None, pages=None, concepts=None):  # noqa: E501
        """PaginatedConcepts - a model defined in Swagger"""  # noqa: E501
        self._hits = None
        self._count = None
        self._page = None
        self._perpage = None
        self._pages = None
        self._concepts = None
        self.discriminator = None
        if hits is not None:
            self.hits = hits
        if count is not None:
            self.count = count
        if page is not None:
            self.page = page
        if perpage is not None:
            self.perpage = perpage
        if pages is not None:
            self.pages = pages
        if concepts is not None:
            self.concepts = concepts

    @property
    def hits(self):
        """Gets the hits of this PaginatedConcepts.  # noqa: E501


        :return: The hits of this PaginatedConcepts.  # noqa: E501
        :rtype: int
        """
        return self._hits

    @hits.setter
    def hits(self, hits):
        """Sets the hits of this PaginatedConcepts.


        :param hits: The hits of this PaginatedConcepts.  # noqa: E501
        :type: int
        """

        self._hits = hits

    @property
    def count(self):
        """Gets the count of this PaginatedConcepts.  # noqa: E501


        :return: The count of this PaginatedConcepts.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this PaginatedConcepts.


        :param count: The count of this PaginatedConcepts.  # noqa: E501
        :type: int
        """

        self._count = count

    @property
    def page(self):
        """Gets the page of this PaginatedConcepts.  # noqa: E501


        :return: The page of this PaginatedConcepts.  # noqa: E501
        :rtype: int
        """
        return self._page

    @page.setter
    def page(self, page):
        """Sets the page of this PaginatedConcepts.


        :param page: The page of this PaginatedConcepts.  # noqa: E501
        :type: int
        """

        self._page = page

    @property
    def perpage(self):
        """Gets the perpage of this PaginatedConcepts.  # noqa: E501


        :return: The perpage of this PaginatedConcepts.  # noqa: E501
        :rtype: int
        """
        return self._perpage

    @perpage.setter
    def perpage(self, perpage):
        """Sets the perpage of this PaginatedConcepts.


        :param perpage: The perpage of this PaginatedConcepts.  # noqa: E501
        :type: int
        """

        self._perpage = perpage

    @property
    def pages(self):
        """Gets the pages of this PaginatedConcepts.  # noqa: E501


        :return: The pages of this PaginatedConcepts.  # noqa: E501
        :rtype: int
        """
        return self._pages

    @pages.setter
    def pages(self, pages):
        """Sets the pages of this PaginatedConcepts.


        :param pages: The pages of this PaginatedConcepts.  # noqa: E501
        :type: int
        """

        self._pages = pages

    @property
    def concepts(self):
        """Gets the concepts of this PaginatedConcepts.  # noqa: E501


        :return: The concepts of this PaginatedConcepts.  # noqa: E501
        :rtype: list[ConceptDto]
        """
        return self._concepts

    @concepts.setter
    def concepts(self, concepts):
        """Sets the concepts of this PaginatedConcepts.


        :param concepts: The concepts of this PaginatedConcepts.  # noqa: E501
        :type: list[ConceptDto]
        """

        self._concepts = concepts

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(PaginatedConcepts, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, PaginatedConcepts):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
