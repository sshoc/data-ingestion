# coding: utf-8

"""
    SSHOC Marketplace API

    Social Sciences and Humanities Open Cloud Marketplace  # noqa: E501

    OpenAPI spec version: 0.1.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class LicenseDto(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'code': 'str',
        'label': 'str',
        'types': 'list[LicenseTypeDto]',
        'accessible_at': 'str'
    }

    attribute_map = {
        'code': 'code',
        'label': 'label',
        'types': 'types',
        'accessible_at': 'accessibleAt'
    }

    def __init__(self, code=None, label=None, types=None, accessible_at=None):  # noqa: E501
        """LicenseDto - a model defined in Swagger"""  # noqa: E501
        self._code = None
        self._label = None
        self._types = None
        self._accessible_at = None
        self.discriminator = None
        if code is not None:
            self.code = code
        if label is not None:
            self.label = label
        if types is not None:
            self.types = types
        if accessible_at is not None:
            self.accessible_at = accessible_at

    @property
    def code(self):
        """Gets the code of this LicenseDto.  # noqa: E501


        :return: The code of this LicenseDto.  # noqa: E501
        :rtype: str
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this LicenseDto.


        :param code: The code of this LicenseDto.  # noqa: E501
        :type: str
        """

        self._code = code

    @property
    def label(self):
        """Gets the label of this LicenseDto.  # noqa: E501


        :return: The label of this LicenseDto.  # noqa: E501
        :rtype: str
        """
        return self._label

    @label.setter
    def label(self, label):
        """Sets the label of this LicenseDto.


        :param label: The label of this LicenseDto.  # noqa: E501
        :type: str
        """

        self._label = label

    @property
    def types(self):
        """Gets the types of this LicenseDto.  # noqa: E501


        :return: The types of this LicenseDto.  # noqa: E501
        :rtype: list[LicenseTypeDto]
        """
        return self._types

    @types.setter
    def types(self, types):
        """Sets the types of this LicenseDto.


        :param types: The types of this LicenseDto.  # noqa: E501
        :type: list[LicenseTypeDto]
        """

        self._types = types

    @property
    def accessible_at(self):
        """Gets the accessible_at of this LicenseDto.  # noqa: E501


        :return: The accessible_at of this LicenseDto.  # noqa: E501
        :rtype: str
        """
        return self._accessible_at

    @accessible_at.setter
    def accessible_at(self, accessible_at):
        """Sets the accessible_at of this LicenseDto.


        :param accessible_at: The accessible_at of this LicenseDto.  # noqa: E501
        :type: str
        """

        self._accessible_at = accessible_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(LicenseDto, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, LicenseDto):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
