# Data Ingestion repo for the SSHOC Marketplace

Hosts RML mappings, UV pipelines and JSONLD context for the ingestion of data into the SSHOC marketplace backend

## [Relations](./relations/README.md)
Ingest relations between items within the MP. The data used to ingest these relations come from other related tools
 (e.g. [ToolXtractor](https://github.com/lehkost/ToolXtractor/)).

# Also hosts scripts to check OpenAIRE data (api.openaire.eu)
## How to use (get counts and CSV table)
1. pip install -r requirements.txt
1. python toolcount_application.py
1. cd scripts/
1. python json_to_csv.py

File `result/epenaire_count.csv` is created

## How to use (retrieve files from OpenAIRE)
1. pip install -r requirements.txt
1. python tool_extraction_application.py

Files are created under `result/xml`

## Retrieved XML Files
the retrieved xml files are too big to be managed with git, google drive and dvc(https://dvc.org/doc/install) are used to manage them.
run 
<code> dvc pull </code> to download the data.
1. openaire.zip.dvc - stopwords filtered
1. openaire_500.zip.dvc - XML files with max_result limit of 500 with stopwords filter

