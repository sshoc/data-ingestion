import json
import requests
import sys

verbose = False
api_url_base = 'https://sshoc-marketplace-api.acdh-dev.oeaw.ac.at/'
openaire_api_url_base = 'http://api.openaire.eu/'
# https://sshoc-marketplace-api.acdh-dev.oeaw.ac.at/api/item-search?categories=publication&order=label&f.source=SSK%20Zotero%20library
# https://sshoc-marketplace-api.acdh-dev.oeaw.ac.at/api/item-search?categories=publication&order=label&f.source=DBLP

publication_titles = []
titles_existing_in_both = []


def get_info(source, category, page):
    if verbose:
        print("Get '" + category + "' info from its source: '" + source + "' - page: " + str(page))
    api_url = '{0}api/item-search'.format(api_url_base)
    response = requests.get(api_url, params={'page': page, 'f.source': source, 'categories': category, 'perpage': 100},
                            headers={'Content-Type': 'application/json'}).json()

    for item in response['items']:
        publication_titles.append(item['label'])

    num_pages = response['pages']
    if page == 1:
        for page in range(2, num_pages + 1):
            get_info(source, category, page)


# http://api.openaire.eu/search/publications?title=github&format=json
# http://api.openaire.eu/search/datasets?title=github&format=json
def find_in_openaire(category):
    api_url = '{0}search/{1}'.format(openaire_api_url_base, category)
    for title in publication_titles:
        print("\n" + title)
        try:
            response = requests.get(api_url, params={'title': title.replace('"', ''), 'format': 'json'},
                                    headers={'Content-Type': 'application/json'}).json()
            if response['response']['results'] is not None:
                for result in response['response']['results']['result']:
                    if isinstance(result['metadata']['oaf:entity']['oaf:result']['title'], list):
                        for openaire_title in result['metadata']['oaf:entity']['oaf:result']['title']:
                            if openaire_title['@classid'] == 'main title':
                                if verbose:
                                    print("--> " + openaire_title['$'])
                                if openaire_title['$'] == title:
                                    titles_existing_in_both.append(title)
                                    print("EXACT MATCH --> " + openaire_title['$'])
                    else:
                        if verbose:
                            print("--> " + result['metadata']['oaf:entity']['oaf:result']['title']['$'])
                        if result['metadata']['oaf:entity']['oaf:result']['title']['$'] == title:
                            titles_existing_in_both.append(title)
                            print("EXACT MATCH --> " + result['metadata']['oaf:entity']['oaf:result']['title']['$'])
        except:
            print('ERROR!!')


if __name__ == "__main__":
    # For DBLP (publications)
    get_info('DBLP', 'publication', 1)
    find_in_openaire('publications')

    # For SSK Zotero library (publications)
    # get_info('SSK Zotero library', 'publication', 1)
    # find_in_openaire('publications')

    # For Humanities Data (datasets)
    # get_info("Humanities Data", 'dataset', 1)
    # find_in_openaire("datasets")

    print("\n\n")
    print(len(titles_existing_in_both))
    print(titles_existing_in_both)
