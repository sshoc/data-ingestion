import json
import requests
import sys

verbose = True
dry_run = True

def get_training_info(name):
    return get_info(name, 'training-material')

def get_tool_info(name):
    return get_info(name, 'tool-or-service')

def get_publication_info(name):
    return get_info(name, 'publication')

def get_info(query, category):
    if verbose:
        print("Get '" + category + "' info from its name: " + query)
    api_url = '{0}api/item-search/'.format(api_url_base)
    response = requests.get(api_url, params={'q': query, 'categories': category}, headers={'Content-Type': 'application/json'})
    if response.status_code == 200:
        return json.loads(response.content.decode('utf-8'))
    else:
        return None

def get_id(info):
    if len(info['items']) > 0:
        return str(info['items'][0]['id'])
    print('THERE IS NO ID FOUND FOR THIS ITEM !!!!!!')
    return None

def login(username, password):
    load = '"username": "{0}", "password": "{1}"'.format(username, password)
    data_raw = json.loads('{' + load + '}')
    api_url = '{0}api/auth/sign-in'.format(api_url_base)
    r = requests.post(api_url, data=json.dumps(data_raw), headers={'Content-Type': 'application/json'})
    if r.ok:
        authToken = r.headers['Authorization']
        return authToken
    else:
        print("HTTP %i - %s, Message %s" % (r.status_code, r.reason, r.text))

def send_api_req(token_key, item_id, related_id):
    api_url = '{0}api/items-relations/{1}/{2}'.format(api_url_base, item_id, related_id)
    data_raw = json.loads('{"code": "relates-to"}') #for publications: use mentions or something else than relates-to
    headers_token={'Content-Type':'application/json', 'Authorization': '{}'.format(token_key)}
    if dry_run:
        print("DRY RUN only for item_id=" + str(item_id) + " and related_id=" + str(related_id))
    if not dry_run:
        r = requests.post(api_url, data=json.dumps(data_raw), headers=headers_token)
        if r.ok:
            print("Connected items together, item_id=" + str(item_id) + ", related_id=" + str(related_id))
        else:
            print("HTTP %i - %s, Message %s" % (r.status_code, r.reason, r.text))

def delete_api_req(token, item_id, related_id):
    api_url = '{0}api/items-relations/{1}/{2}'.format(api_url_base, item_id, related_id)
    headers_token={'Content-Type':'application/json', 'Authorization': '{}'.format(token)}
    if not dry_run:
        r = requests.delete(api_url, headers=headers_token)
        if r.ok:
            print("Deleted relation, item_id=" + str(item_id) + ", related_id=" + str(related_id))
        else:
            print("HTTP %i - %s, Message %s" % (r.status_code, r.reason, r.text))


api_url_base = 'https://sshoc-marketplace-api.acdh-dev.oeaw.ac.at/'

#part is 1 or 2 (e.g. PH_TAPoR: part 1 is PH so training_material, part 2 is TAPoR so tool-or-service)
def select_type_item(type_relation, part, key):
    if type_relation == "PH_TAPoR":
        if part == 1:
            return get_training_info(key)
        elif part == 2:
            return get_tool_info(key)
    elif type_relation == "TAPoR_DH2020" or type_relation == "TAPoR_DH2019" or type_relation == "TAPoR_DH2018" or type_relation == "TAPoR_DH2017" or type_relation == "TAPoR_DH2016":
        if part == 1:
            return get_tool_info(key)
        elif part == 2:
            return get_publication_info(key)
    else:
        return None

def read_all_relation_file(token_key, type_relation):
    file_name = "data/" + type_relation + ".json"

    with open(file_name) as json_file:
        data = json.load(json_file)
        for list in data:
            for key in list.keys():
                item_info = select_type_item(type_relation, 1, key)
                if item_info is not None:
                    item_id = get_id(item_info)
                    if item_id is not None:
                        if str(item_info['items'][0]['label']) != key:
                            print(item_info['items'][0]['label'])
                            print(key)
                        print("Item_id: " + str(item_id))
                        for related_item in list[key]:
                            related_info = select_type_item(type_relation, 2, related_item)
                            if related_info is not None:
                                related_id = get_id(related_info)
                                if related_id is not None:
                                    if str(related_info['items'][0]['label']) != related_item:
                                        print(related_info['items'][0]['label'])
                                        print(related_item)
                                    print("Related_id: " + str(related_id))
                                    send_api_req(token_key, item_id, related_id)

if __name__ == "__main__":
    user = sys.argv[1]
    pwd = sys.argv[2]
    token = login(user, pwd)

    #Ok: verified and launched
    #read_all_relation_file(token, "PH_TAPoR")
    #read_all_relation_file(token, "TAPoR_DH2020")
    #read_all_relation_file(token, "TAPoR_DH2018")
    #read_all_relation_file(token, "TAPoR_DH2017")
    #read_all_relation_file(token, "TAPoR_DH2016")


    #not launched yet (not found the publications)
    #read_all_relation_file(token, "TAPoR_DH2019")


