# ingest_relations_SSHOCMP.py
## To install
```
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```
## To launch
`[username]` and `[password]` are to connect to the SSHOC MP API
```
python3 ingest_relations_SSHOCMP.py [username] [password]
```

## Explanation: where do the .json files come from
From ToolXtractor modified version where it outputs a JSON instead of a list - to describe better

### TAPoR_DH2020
From https://weltliteratur.net/tools-mentioned-in-dh2020-abstracts/

### PH_TAPoR
From ...

# find_related_publications.py

Find related publications between SSH MP and OpenAIRE

## To change sources to check

Edit the file and comment/uncomment the lines at the end of the file

## To launch
```
python3 find_related_publications.py
```